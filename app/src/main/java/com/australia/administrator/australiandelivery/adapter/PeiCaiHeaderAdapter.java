package com.australia.administrator.australiandelivery.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.australia.administrator.australiandelivery.R;
import com.australia.administrator.australiandelivery.bean.CaidanBean;
import com.australia.administrator.australiandelivery.bean.GoodsBean;
import com.australia.administrator.australiandelivery.bean.ShopDetailsBean;
import com.australia.library.StickyHeadersAdapter;

import java.util.List;


public class PeiCaiHeaderAdapter implements StickyHeadersAdapter<PeiCaiHeaderAdapter.ViewHolder> {

    private  Context mContext;
    private List<CaidanBean.PeicaiBean> dataList;
    private List<CaidanBean> goodscatrgoryEntities;
    public PeiCaiHeaderAdapter(Context context, List<CaidanBean.PeicaiBean> items, List<CaidanBean> goodscatrgoryEntities) {
        this.mContext = context;
        this.dataList = items;
        this.goodscatrgoryEntities = goodscatrgoryEntities;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_goods_list, parent, false);
        return new PeiCaiHeaderAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PeiCaiHeaderAdapter.ViewHolder headerViewHolder, int position) {
        headerViewHolder.tvGoodsItemTitle.setText(goodscatrgoryEntities.get(dataList.get(position).getId()).getPcaidan());
//        headerViewHolder.tvGoodsItemTitle.setText("wolegequ");
    }

    @Override
    public long getHeaderId(int position) {
        return dataList.get(position).getId();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvGoodsItemTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            tvGoodsItemTitle = (TextView) itemView.findViewById(R.id.tvGoodsItemTitle);
        }
    }
}
