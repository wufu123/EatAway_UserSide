package com.australia.administrator.australiandelivery.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.australia.administrator.australiandelivery.R;
import com.australia.administrator.australiandelivery.bean.CaidanBean;
import com.australia.administrator.australiandelivery.bean.GoodsBean;
import com.australia.administrator.australiandelivery.utils.NumberFormatUtil;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2017/7/8.
 */

public class OrdersAdapterSecond extends RecyclerView.Adapter<OrdersAdapterSecond.ViewHolder> {
    private Context context;
    private List<GoodsBean> goods;

    public OrdersAdapterSecond(Context context, List<GoodsBean> goods) {
        this.context = context;
        this.goods = goods;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.orders_item_second, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (goods.get(position).getSelectGui()!=null) {
            holder.tvOrdersName.setText(goods.get(position).getGoodsname() + "(" + goods.get(position).getSelectGui().getGuige()
            + ")");
        }else {
            holder.tvOrdersName.setText(goods.get(position).getGoodsname());
        }
        holder.tvOrdersNum.setText("×" + goods.get(position).getNumber());
        holder.tvOrdersMoney.setText("$" + NumberFormatUtil.round(goods.get(position).getNumber() * goods.get(position).getRealPrice(), 2));
        holder.llPeicai.removeAllViews();
        for (int i = 0; goods.get(position).getSelectPei()!= null && i < goods.get(position).getSelectPei().size(); i++) {
            TextView textView = new TextView(context);
            textView.setText("+"+goods.get(position).getSelectPei().get(i).getPcname());
            holder.llPeicai.addView(textView);
        }
    }

    @Override
    public int getItemCount() {
        if (goods != null)
            return goods.size();
        return 0;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_orders_name)
        TextView tvOrdersName;
        @Bind(R.id.tv_orders_num)
        TextView tvOrdersNum;
        @Bind(R.id.tv_orders_money)
        TextView tvOrdersMoney;
        @Bind(R.id.ll_peicai)
        LinearLayout llPeicai;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
