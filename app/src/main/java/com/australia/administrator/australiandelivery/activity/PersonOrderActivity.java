package com.australia.administrator.australiandelivery.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.australia.administrator.australiandelivery.MyApplication;
import com.australia.administrator.australiandelivery.R;
import com.australia.administrator.australiandelivery.adapter.PersonOrderAdapter;
import com.australia.administrator.australiandelivery.bean.MyOrderBean;
import com.australia.administrator.australiandelivery.comm.BaseActivity;
import com.australia.administrator.australiandelivery.loadmore.LoadMoreWrapper;
import com.australia.administrator.australiandelivery.utils.Contants;
import com.australia.administrator.australiandelivery.utils.HttpUtils;
import com.australia.administrator.australiandelivery.view.TopBar;
import com.braintreepayments.api.Json;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.Call;

import static com.australia.administrator.australiandelivery.MyApplication.isNeedAutoLocate;

public class PersonOrderActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener{
    private final int ORDER_REQUEST = 1200;
    private final int ORDER_RESULT = 1201;
    @Bind(R.id.tp_person_order)
    TopBar tpPersonOrder;
    @Bind(R.id.rv_activity_person_order)
    RecyclerView rvActivityPersonOrder;
    @Bind(R.id.sr_current_order)
    SwipeRefreshLayout srCurrentOrder;

    private MyOrderBean bean;
    private PersonOrderAdapter adapter;
    private HttpUtils utils;
    private int page = 1;
    private LoadMoreWrapper mLoadMoreWrapper;
    private final int REQUEST_CODE = 10086;
    private static final int REFRESH_COMPLETE = 0X110;
    private boolean iSDestroy = false;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case REFRESH_COMPLETE:
                    if (iSDestroy) {
                        return;
                    }
                    page = 1;
                    loadData();
                    break;

            }
        }
    };

    @Override
    protected int getLayoutId() {
        return R.layout.activity_person_order;
    }

    @Override
    protected void initDate() {
    }

    @Override
    public void onRefresh() {
        mHandler.sendEmptyMessageDelayed(REFRESH_COMPLETE, 2000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        InitTopBar();
        initRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        page = 1;
        showDialog();
        loadData();
    }

    @Override
    protected void onDestroy() {
        iSDestroy = true;
        super.onDestroy();
    }

    private void initRecyclerView() {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        rvActivityPersonOrder.setLayoutManager(manager);
        adapter = new PersonOrderAdapter(PersonOrderActivity.this);

        srCurrentOrder.setOnRefreshListener(this);
        srCurrentOrder.setColorScheme(android.R.color.holo_green_light);
//        srCurrentOrder.setRefreshing(true);
        //上拉加载把你用的adapter传进去
        mLoadMoreWrapper = new LoadMoreWrapper(this, adapter);
        mLoadMoreWrapper.setOnLoadListener(new LoadMoreWrapper.OnLoadListener() {
            @Override
            public void onRetry() {
                    loadData();
            }

            @Override
            public void onLoadMore() {
                if (bean.getMsg() == null || bean.getMsg().size() < 15) {
                    mLoadMoreWrapper.showLoadComplete();
                } else {
                    //加载更多
                    loadData();
                }
            }
        });
        rvActivityPersonOrder.setAdapter(mLoadMoreWrapper);
    }


    private void InitTopBar() {
        tpPersonOrder.setBackGround(R.color.actionBar);
        tpPersonOrder.setTbCenterTv(R.string.person_order_title, R.color.white);
        tpPersonOrder.setTbLeftIv(R.drawable.img_back_icon_white, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * 辅助函数
     */
    private void loadData() {
        utils = new HttpUtils(Contants.URL_ORDER_ORDERLIST) {
            @Override
            public void onError(Call call, Exception e, int id) {
                hidDialog();
                mLoadMoreWrapper.showLoadError();
                srCurrentOrder.setRefreshing(false);
                Toast.makeText(PersonOrderActivity.this, R.string.please_check_your_network_connection, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(String response, int id) {
                hidDialog();
                mLoadMoreWrapper.showLoadComplete();
                srCurrentOrder.setRefreshing(false);
                try {
                    JSONObject o = new JSONObject(response);
                    int status = o.getInt("status");
                    if(status == 1) {
                        bean = new Gson().fromJson(response, MyOrderBean.class);
                        if (page ==1) {
                            adapter.setBean(bean);
                        }else {
                            adapter.addBean(bean);
                        }
                        mLoadMoreWrapper.notifyDataSetChanged();
                        page++;
                    }else if (status == 0) {
                        Toast.makeText(PersonOrderActivity.this, R.string.please_check_your_network_connection, Toast.LENGTH_SHORT).show();
                    }else if (status == 9) {
                        Toast.makeText(PersonOrderActivity.this, R.string.validation_fails, Toast.LENGTH_SHORT).show();
                        MyApplication.saveLogin(null);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        utils.addParam("page", page + "");
        utils.addParam("userid", MyApplication.getLogin().getUserId());
        utils.addParam("token", MyApplication.getLogin().getToken());
        utils.clicent();
    }
}
