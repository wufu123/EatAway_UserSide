package com.australia.administrator.australiandelivery.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by local123 on 2017/11/28.
 */

public class GoodsBean implements Serializable {
    /**
     * goodsid : 1986
     * goodsname : 好运姐 厄运小姐
     * goodsphoto : http://wm.sawfree.com/uploads/9c95ffa9d28e58641f5d428a6e9fea09.jpg
     * goodsprice : 188.00
     * goodscontent : lolo
     * cid : 366
     * shopid : 45
     * flag : 1
     * zhekou : 0
     * statts : null
     * starttime : null
     * stoptime : null
     * addtime : 2017-11-08 13:58:02
     * wzhekou : 0
     * gzhekou : 0
     * goodsstate : 3
     * guige : [{"gid":"7","goodsid":"1986","guige":"泳池派对","primoney":"1800","addtime":"2017-11-08
     * 13:58:02"},{"gid":"8","goodsid":"1986","guige":"s6赛季专属","primoney":"200","addtime":"2017-11-08 13:58:02"},
     * {"gid":"9","goodsid":"1986","guige":"西部牛仔","primoney":"98","addtime":"2017-11-08 13:58:02"},
     * {"gid":"12","goodsid":"1986","guige":"电玩女神","primoney":"188","addtime":"0000-00-00 00:00:00"}]
     * caidan : [{"pid":"6","goodsid":"1986","pcaidan":"红叉","addtime":"2017-11-08 13:58:02","peicai":
     * [{"pcid":"1","pcname":"sad","pcprice":"0.99","fid":"6","addtime":"2017-11-09 16:22:44"},{"pcid":"2","pcname":"提3阿
     * 发","pcprice":"0.99","fid":"6","addtime":"2017-11-09 16:23:12"},{"pcid":"3","pcname":"撒发
     * 达","pcprice":"200.00","fid":"6","addtime":"2017-11-09 16:23:35"},{"pcid":"4","pcname":"什
     * 么","pcprice":"200.00","fid":"6","addtime":"0000-00-00 00:00:00"},
     * {"pcid":"5","pcname":"asdfas","pcprice":"12.00","fid":"6","addtime":"0000-00-00 00:00:00"}]},
     * {"pid":"7","goodsid":"1986","pcaidan":"电刀","addtime":"2017-11-08 13:58:02","peicai":[]},
     * {"pid":"8","goodsid":"1986","pcaidan":"无尽之力","addtime":"2017-11-08 13:58:02","peicai":[]},
     * {"pid":"11","goodsid":"1986","pcaidan":"饮血剑","addtime":"2017-11-09 14:57:29","peicai":[]},
     * {"pid":"12","goodsid":"1986","pcaidan":"破败之刃","addtime":"2017-11-09 15:00:00","peicai":
     * [{"pcid":"9","pcname":"aaa","pcprice":"0.00","fid":"12","addtime":"0000-00-00 00:00:00"}]}]
     */
    private String goodsid;
    private String goodsname;
    private String goodsphoto;
    private String goodsprice;
    private String goodscontent;
    private String cid;
    private String shopid;
    private String flag;
    private String zhekou;
    private String statts;
    private String starttime;
    private String stoptime;
    private String addtime;
    private String wzhekou;
    private String gzhekou;
    private int goodsstate;
    private List<GuigeBean> guige;
    private List<CaidanBean> caidan;
    private int number;
    private int id;
    private DishInfo dishInfos;
    private List<CaidanBean.PeicaiBean> selectPei;
    private GuigeBean selectGui;
    private double realPrice;   //真实价格，用于维护购物车总价
    private double disPrice;    //折扣计算完后折扣了多少

    public double getDisPrice() {
        return disPrice;
    }

    public void setDisPrice(double disPrice) {
        this.disPrice = disPrice;
    }

    public double getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(double realPrice) {
        this.realPrice = realPrice;
    }

    public List<CaidanBean.PeicaiBean> getSelectPei() {
        return selectPei;
    }

    public void setSelectPei(List<CaidanBean.PeicaiBean> selectPei) {
        this.selectPei = selectPei;
    }

    public GuigeBean getSelectGui() {
        return selectGui;
    }

    public void setSelectGui(GuigeBean selectGui) {
        this.selectGui = selectGui;
    }

    public DishInfo getDishInfos() {
        return dishInfos;
    }

    public void setDishInfos(DishInfo dishInfos) {
        this.dishInfos = dishInfos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getGoodsid() {
        return goodsid;
    }

    public void setGoodsid(String goodsid) {
        this.goodsid = goodsid;
    }

    public String getGoodsname() {
        return goodsname;
    }

    public void setGoodsname(String goodsname) {
        this.goodsname = goodsname;
    }

    public String getGoodsphoto() {
        return goodsphoto;
    }

    public void setGoodsphoto(String goodsphoto) {
        this.goodsphoto = goodsphoto;
    }

    public String getGoodsprice() {
        return goodsprice;
    }

    public void setGoodsprice(String goodsprice) {
        this.goodsprice = goodsprice;
    }

    public String getGoodscontent() {
        return goodscontent;
    }

    public void setGoodscontent(String goodscontent) {
        this.goodscontent = goodscontent;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getShopid() {
        return shopid;
    }

    public void setShopid(String shopid) {
        this.shopid = shopid;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getZhekou() {
        return zhekou;
    }

    public void setZhekou(String zhekou) {
        this.zhekou = zhekou;
    }

    public Object getStatts() {
        return statts;
    }

    public void setStatts(String statts) {
        this.statts = statts;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getStoptime() {
        return stoptime;
    }

    public void setStoptime(String stoptime) {
        this.stoptime = stoptime;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public String getWzhekou() {
        return wzhekou;
    }

    public void setWzhekou(String wzhekou) {
        this.wzhekou = wzhekou;
    }

    public String getGzhekou() {
        return gzhekou;
    }

    public void setGzhekou(String gzhekou) {
        this.gzhekou = gzhekou;
    }

    public int getGoodsstate() {
        return goodsstate;
    }

    public void setGoodsstate(int goodsstate) {
        this.goodsstate = goodsstate;
    }

    public List<GuigeBean> getGuige() {
        return guige;
    }

    public void setGuige(List<GuigeBean> guige) {
        this.guige = guige;
    }

    public List<CaidanBean> getCaidan() {
        return caidan;
    }

    public void setCaidan(List<CaidanBean> caidan) {
        this.caidan = caidan;
    }

    public static class DishInfo {
        private String guiid;
        private List<String> peicaiId;
        private int num;
        private double price;

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public String getGuiid() {
            return guiid;
        }

        public void setGuiid(String guiid) {
            this.guiid = guiid;
        }

        public List<String> getPeicaiId() {
            return peicaiId;
        }

        public void setPeicaiId(List<String> peicaiId) {
            this.peicaiId = peicaiId;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }
    }
}
