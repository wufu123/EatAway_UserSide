package com.australia.administrator.australiandelivery.bean;

import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by local123 on 2017/11/28.
 */

public class GuigeBean implements Serializable{
    /**
     * gid : 7
     * goodsid : 1986
     * guige : 泳池派对
     * primoney : 1800
     * addtime : 2017-11-08 13:58:02
     */

    private String gid;
    private String goodsid;
    private String guige;
    private String primoney;
    private String addtime;

    private boolean isSelect;

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getGoodsid() {
        return goodsid;
    }

    public void setGoodsid(String goodsid) {
        this.goodsid = goodsid;
    }

    public String getGuige() {
        return guige;
    }

    public void setGuige(String guige) {
        this.guige = guige;
    }

    public String getPrimoney() {
        return primoney;
    }

    public void setPrimoney(String primoney) {
        this.primoney = primoney;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }
}
