package com.australia.administrator.australiandelivery.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.australia.administrator.australiandelivery.MyApplication;
import com.australia.administrator.australiandelivery.R;
import com.australia.administrator.australiandelivery.adapter.GuiGeAdapter;
import com.australia.administrator.australiandelivery.adapter.PeiCaiAdapter;
import com.australia.administrator.australiandelivery.bean.CaidanBean;
import com.australia.administrator.australiandelivery.bean.GoodsBean;
import com.australia.administrator.australiandelivery.bean.GuigeBean;
import com.australia.administrator.australiandelivery.comm.*;
import com.australia.administrator.australiandelivery.utils.AnimationUtil;
import com.australia.administrator.australiandelivery.utils.GlideUtils;
import com.australia.administrator.australiandelivery.utils.NumberFormatUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

import static com.australia.administrator.australiandelivery.MyApplication.shopcar;

public class DishDetailActivity extends com.australia.administrator.australiandelivery.comm.BaseActivity
        implements GuiGeAdapter.onGuiGeSelectListener, PeiCaiAdapter.onSelectedListener {
    @Bind(R.id.iv_dish_detail)
    ImageView ivDishDetail;
    @Bind(R.id.tv_dish_detail_dish_price)
    TextView tvDishDetailDishPrice;
    @Bind(R.id.tv_dish_detail_dish_content)
    TextView tvDishDetailDishContent;
    @Bind(R.id.tb_activity_goods_detail)
    Toolbar tbActivityGoodsDetail;
    @Bind(R.id.collapsing)
    CollapsingToolbarLayout collapsing;
    @Bind(R.id.appbar)
    AppBarLayout appbar;
    @Bind(R.id.dish_detail_list_line)
    View dishDetailListLine;
    @Bind(R.id.totalPrice)
    TextView totalPrice;
    @Bind(R.id.minPrice)
    TextView minPrice;
    @Bind(R.id.settlement)
    TextView settlement;
    @Bind(R.id.shopping_cart)
    ImageView shoppingCart;
    @Bind(R.id.shopCartNum)
    TextView shopCartNum;
    @Bind(R.id.shopCartMain)
    RelativeLayout shopCartMain;
    @Bind(R.id.toolBar)
    RelativeLayout toolBar;
    @Bind(R.id.bg_layout)
    View bgLayout;
    @Bind(R.id.defaultText)
    TextView defaultText;
    @Bind(R.id.shopproductListView)
    ListView shopproductListView;
    @Bind(R.id.cardShopLayout)
    LinearLayout cardShopLayout;
    @Bind(R.id.noShop)
    FrameLayout noShop;
    @Bind(R.id.iv_activity_goods_back)
    ImageView ivActivityGoodsBack;
    @Bind(R.id.tb_goods_dish_name)
    TextView tbGoodsDishName;
    @Bind(R.id.tv_dish_detail_dish_name)
    TextView tvDishDetailDishName;
    @Bind(R.id.rv_guige)
    RecyclerView rvGuige;
    @Bind(R.id.ll_guige)
    LinearLayout llGuige;
    @Bind(R.id.rv_peicai)
    RecyclerView rvPeicai;
    @Bind(R.id.ivGoodsMinus)
    ImageView ivGoodsMinus;
    @Bind(R.id.tvGoodsSelectNum)
    TextView tvGoodsSelectNum;
    @Bind(R.id.ivGoodsAdd)
    ImageView ivGoodsAdd;
    @Bind(R.id.ll_add_sub)
    LinearLayout llAddSub;
    @Bind(R.id.tv_guige)
    TextView tvGuige;
    @Bind(R.id.tv_guige_num)
    TextView tvGuigeNum;

    private ViewGroup anim_mask_layout;//动画层

    private Intent i;
    private GuiGeAdapter guiGeAdapter;
    private PeiCaiAdapter peiCaiAdapter;

    private List<GuigeBean> guigeBean;
    private List<CaidanBean> caidanBean;


    private String cid;
    private GoodsBean goodsBean;
    //商品表头列表
    private List<CaidanBean> caidanList = new ArrayList<>();
    //商品列表
    private List<CaidanBean.PeicaiBean> peicaiList = new ArrayList<>();
    //配菜选择列表
    private List<CaidanBean.PeicaiBean> selectList = new ArrayList<>();

    private boolean isChange = false;
    private int currentGuige;
    private int[] currentPeicai;
    //显示的价格
    private double price;
    private ImageView buyImg;

    private void setStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        } else {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        collapsing.setStatusBarScrimColor(getResources().getColor(R.color.touming));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_dish_detail;
    }

    @Override
    protected void initDate() {
        setStatusBar();
//        shopCartMain.startAnimation(AnimationUtil.createInAnimation(this, shopCartMain.getMeasuredHeight()));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_activity_goods_detail);
        setSupportActionBar(toolbar);

        cid = getIntent().getStringExtra("cid");
        goodsBean = (GoodsBean) getIntent().getSerializableExtra("dish");
        GlideUtils.load(this, goodsBean.getGoodsphoto(), ivDishDetail, GlideUtils.Shape.ShopPic);
        tvDishDetailDishName.setText(goodsBean.getGoodsname());
        tvDishDetailDishContent.setText(goodsBean.getGoodsname());
        caidanBean = goodsBean.getCaidan();
        guigeBean = goodsBean.getGuige();

        ivActivityGoodsBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        initRecyclerview();
        initData();
    }

    private void initRecyclerview() {
        rvGuige.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvPeicai.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvPeicai.setNestedScrollingEnabled(false);
        rvGuige.setNestedScrollingEnabled(false);
        rvGuige.setItemAnimator(null);
        rvPeicai.setItemAnimator(null);
        if ((caidanBean != null && !caidanBean.isEmpty()) || (guigeBean != null && !guigeBean.isEmpty())) {
            llAddSub.setVisibility(View.GONE);
            tvGuige.setVisibility(View.VISIBLE);
            tvGuigeNum.setVisibility(View.VISIBLE);
            if (guigeBean != null && !guigeBean.isEmpty()) {
                price = getPrice(guigeBean.get(0).getPrimoney());
                llGuige.setVisibility(View.VISIBLE);
                guiGeAdapter = new GuiGeAdapter(this, guigeBean);
                guiGeAdapter.setListener(this);
                rvGuige.setAdapter(guiGeAdapter);
            } else {
                price = getPrice(getIntent().getStringExtra("dishprice"));
                llGuige.setVisibility(View.GONE);
            }
            if (caidanBean != null && !caidanBean.isEmpty()) {
                rvPeicai.setVisibility(View.VISIBLE);
                //初始化配菜表头
                for (int i = 0; i < caidanBean.size(); i++) {
                    CaidanBean bean = caidanBean.get(i);
                    caidanList.add(bean);
                    for (int j = 0; j < bean.getPeicai().size(); j++) {
                        CaidanBean.PeicaiBean peicaiBean = bean.getPeicai().get(j);
                        peicaiBean.setId(i);
                        peicaiBean.setIsselect(false);
                        peicaiList.add(peicaiBean);
                    }
                }
                peiCaiAdapter = new PeiCaiAdapter(this, peicaiList, caidanList);
                peiCaiAdapter.setListener(this);
                rvPeicai.setAdapter(peiCaiAdapter);
            } else {
                rvPeicai.setVisibility(View.GONE);
            }
        } else {
            llAddSub.setVisibility(View.VISIBLE);
            tvGuige.setVisibility(View.GONE);
            tvGuigeNum.setVisibility(View.GONE);
            llGuige.setVisibility(View.GONE);
            rvPeicai.setVisibility(View.GONE);
            price = getPrice(getIntent().getStringExtra("dishprice"));
        }
        tvDishDetailDishPrice.setText("$" + price + "/" + getString(R.string.fen));
    }

    private void initData() {
    }


    private void minitData() {
        caidanBean = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            List<CaidanBean.PeicaiBean> beanList = new ArrayList<>();
            CaidanBean bean = new CaidanBean();
            bean.setPcaidan("配菜" + i);
            for (int j = 0; j < 2 - i; j++) {
                CaidanBean.PeicaiBean peicaiBean = new CaidanBean.PeicaiBean();
                peicaiBean.setPcname("名称" + j);
                beanList.add(peicaiBean);
            }
            bean.setPeicai(beanList);
            caidanBean.add(bean);
        }
    }

    @Override
    public void onGuiGeSelected(int select, int preSelet) {
        currentGuige = select;
        price = price - getPrice(guigeBean.get(preSelet).getPrimoney()) + getPrice(guigeBean.get(select).getPrimoney());
        price = NumberFormatUtil.round(price, 2);
        tvDishDetailDishPrice.setText("$" + price + "/" + getString(R.string.fen));
    }

    @Override
    public void onPeiCaiSelected(int pmenu, int pdish, boolean isSelect) {
        if (isSelect) {
            //选中了这个配菜
            price += getPrice(peicaiList.get(pdish).getPcprice());
            peicaiList.get(pdish).setIsselect(true);
        } else {
            price -= getPrice(peicaiList.get(pdish).getPcprice());
            peicaiList.get(pdish).setIsselect(false);
        }
        price = NumberFormatUtil.round(price, 2);
        tvDishDetailDishPrice.setText("$" + price + "/" + getString(R.string.fen));
    }

    //更新价格
    private void refreshPrice() {

    }

    //得到价格,保留两位小数
    private double getPrice(String price) {
        return TextUtils.isEmpty(price) ? 0 : NumberFormatUtil.round(Double.valueOf(price), 2);
    }

    @OnClick({R.id.ivGoodsMinus, R.id.ivGoodsAdd, R.id.tv_guige})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivGoodsMinus:
                //删除商品--无规格
                if (goodsBean.getNumber() > 0) {
                    for (GoodsBean data : shopcar) {
                        if (TextUtils.equals(data.getCid(), goodsBean.getCid())) {
                            int number = data.getNumber() - 1;
                            data.setNumber(number);
                            if (number == 0) {
                                shopcar.remove(data);
                            }
                        }
                    }
                }
                break;
            case R.id.ivGoodsAdd:
                startAnim(view);
                //添加商品--无规格
                if (shopcar == null) { shopcar = new ArrayList<>(); }
                for (GoodsBean data : shopcar) {
                    if (TextUtils.equals(data.getCid(), goodsBean.getCid())) {
                        int number = data.getNumber() + 1;
                        data.setNumber(number);
                        return;
                    }
                }
                //如果没有在列表中找到的话，新增一个商品信息，并且没有规格和配菜，直接添加当前goodsBean
                shopcar.add(goodsBean);
                break;
            case R.id.tv_guige:
                startAnim(view);
                //添加商品--有规格

                break;
        }
    }

    /**
     * 修改购物车状态
     */
    private void changeShopCart() {

    }

    /**
     * 开始动画
     *
     * @param view
     */
    private void startAnim(View view) {
        buyImg = new ImageView(this);
        buyImg.setBackgroundResource(R.mipmap.icon_goods_add_item);// 设置buyImg的图片
        int[] loc = new int[2];
        view.getLocationInWindow(loc);
        int[] startLocation = new int[2];// 一个整型数组，用来存储按钮的在屏幕的X、Y坐标
        view.getLocationInWindow(startLocation);// 这是获取购买按钮的在屏幕的X、Y坐标（这也是动画开始的坐标）
        setAnim(buyImg, startLocation);// 开始执行动画
    }

    /**
     * 设置动画（点击添加商品）
     *
     * @param v
     * @param startLocation
     */
    public void setAnim(final View v, int[] startLocation) {
        anim_mask_layout = null;
        anim_mask_layout = createAnimLayout();
        anim_mask_layout.addView(v);//把动画小球添加到动画层
        final View view = addViewToAnimLayout(anim_mask_layout, v, startLocation);
        int[] endLocation = new int[2];// 存储动画结束位置的X、Y坐标
        shopCartNum.getLocationInWindow(endLocation);
        // 计算位移
        int endX = 0 - startLocation[0] + 40;// 动画位移的X坐标
        int endY = endLocation[1] - startLocation[1];// 动画位移的y坐标

        TranslateAnimation translateAnimationX = new TranslateAnimation(0, endX, 0, 0);
        translateAnimationX.setInterpolator(new LinearInterpolator());
        translateAnimationX.setRepeatCount(0);// 动画重复执行的次数
        translateAnimationX.setFillAfter(true);

        TranslateAnimation translateAnimationY = new TranslateAnimation(0, 0, 0, endY);
        translateAnimationY.setInterpolator(new AccelerateInterpolator());
        translateAnimationY.setRepeatCount(0);// 动画重复执行的次数
        translateAnimationY.setFillAfter(true);

        AnimationSet set = new AnimationSet(false);
        set.setFillAfter(false);
        set.addAnimation(translateAnimationY);
        set.addAnimation(translateAnimationX);
        set.setDuration(400);// 动画的执行时间
        view.startAnimation(set);
        // 动画监听事件
        set.setAnimationListener(new Animation.AnimationListener() {
            // 动画的开始
            @Override
            public void onAnimationStart(Animation animation) {
                v.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            // 动画的结束
            @Override
            public void onAnimationEnd(Animation animation) {
                v.setVisibility(View.GONE);
            }
        });

    }

    /**
     * 初始化动画图层
     *
     * @return
     */
    private ViewGroup createAnimLayout() {
        ViewGroup rootView = (ViewGroup) this.getWindow().getDecorView();
        LinearLayout animLayout = new LinearLayout(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        animLayout.setLayoutParams(lp);
        animLayout.setId(Integer.MAX_VALUE - 1);
        animLayout.setBackgroundResource(android.R.color.transparent);
        rootView.addView(animLayout);
        return animLayout;
    }

    /**
     * 将View添加到动画图层
     *
     * @param parent
     * @param view
     * @param location
     * @return
     */
    private View addViewToAnimLayout(final ViewGroup parent, final View view, int[] location) {
        int x = location[0];
        int y = location[1];
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.leftMargin = x;
        lp.topMargin = y;
        view.setLayoutParams(lp);
        return view;
    }
}
