package com.australia.administrator.australiandelivery.comm;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.australia.administrator.australiandelivery.MyApplication;
import com.australia.administrator.australiandelivery.R;
import com.australia.administrator.australiandelivery.adapter.GuiGeAdapter;
import com.australia.administrator.australiandelivery.adapter.PeiCaiAdapter;
import com.australia.administrator.australiandelivery.adapter.ShopAdapter;
import com.australia.administrator.australiandelivery.bean.CaidanBean;
import com.australia.administrator.australiandelivery.bean.GoodsBean;
import com.australia.administrator.australiandelivery.bean.GuigeBean;
import com.australia.administrator.australiandelivery.utils.GlideUtils;
import com.australia.administrator.australiandelivery.utils.NumberFormatUtil;
import com.baidu.mapapi.map.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 菜品详情
 * Created by local123 on 2017/11/22.
 */

public class DishDetailDialog extends DialogFragment implements GuiGeAdapter.onGuiGeSelectListener, PeiCaiAdapter.onSelectedListener
, Dialog.OnKeyListener{
    public static final String BUNDLE_DATA = "bean";
    public static final String BUNDLE_ID = "cid";
    public static final String BUNDLE_BUYNUM = "buynum";
    public static final String BUNDLE_TOTALPRICE = "totalprice";
    @Bind(R.id.iv_dish_detail)
    public ImageView ivDishDetail;
    @Bind(R.id.tb_goods_dish_name)
    public TextView tbGoodsDishName;
    @Bind(R.id.tb_activity_goods_detail)
    public Toolbar tbActivityGoodsDetail;
    @Bind(R.id.collapsing)
    public CollapsingToolbarLayout collapsing;
    @Bind(R.id.appbar)
    public AppBarLayout appbar;
    @Bind(R.id.tv_dish_detail_dish_name)
    public TextView tvDishDetailDishName;
    @Bind(R.id.tv_dish_detail_dish_price)
    public TextView tvDishDetailDishPrice;
    @Bind(R.id.ivGoodsMinus)
    public ImageView ivGoodsMinus;
    @Bind(R.id.tvGoodsSelectNum)
    public TextView tvGoodsSelectNum;
    @Bind(R.id.ivGoodsAdd)
    public ImageView ivGoodsAdd;
    @Bind(R.id.ll_add_sub)
    public LinearLayout llAddSub;
    @Bind(R.id.tv_guige)
    public TextView tvGuige;
    @Bind(R.id.tv_guige_num)
    public TextView tvGuigeNum;
    @Bind(R.id.dish_detail_list_line)
    public View dishDetailListLine;
    @Bind(R.id.tv_dish_detail_dish_content)
    public TextView tvDishDetailDishContent;
    @Bind(R.id.rv_guige)
    public RecyclerView rvGuige;
    @Bind(R.id.ll_guige)
    public LinearLayout llGuige;
    @Bind(R.id.rv_peicai)
    public RecyclerView rvPeicai;
    @Bind(R.id.totalPrice)
    public TextView totalPrice;
    @Bind(R.id.minPrice)
    public TextView tvPsmoney;
    @Bind(R.id.settlement)
    public TextView settlement;
    @Bind(R.id.shopping_cart)
    public ImageView shoppingCart;
    @Bind(R.id.shopCartNum)
    public TextView shopCartNum;
    @Bind(R.id.shopCartMain)
    public RelativeLayout shopCartMain;
    @Bind(R.id.toolBar)
    public RelativeLayout toolBar;
    @Bind(R.id.bg_layout)
    public View bgLayout;
    @Bind(R.id.defaultText)
    public TextView defaultText;
    @Bind(R.id.shopproductListView)
    public ListView shopproductListView;
    @Bind(R.id.cardShopLayout)
    public LinearLayout cardShopLayout;
    @Bind(R.id.noShop)
    public FrameLayout noShop;
    @Bind(R.id.iv_activity_goods_back)
    public ImageView ivActivityGoodsBack;

    public ShopAdapter shopAdapter;
    @Bind(R.id.tv_pre_price)
    TextView tvPrePrice;
    @Bind(R.id.rl_buttons)
    RelativeLayout rlButtons;
    private Dialog dialog;
    private View.OnClickListener onClickListener;
    private changeMenuListener changeMenuListener;
    private ViewGroup anim_mask_layout;//动画层

    private GuiGeAdapter guiGeAdapter;
    private PeiCaiAdapter peiCaiAdapter;

    private List<GuigeBean> guigeBean;
    private List<CaidanBean> caidanBean;

    private String cid;
    private GoodsBean goodsBean;
    //商品表头列表
    private List<CaidanBean> caidanList = new ArrayList<>();
    //商品列表
    private List<CaidanBean.PeicaiBean> peicaiList = new ArrayList<>();
    //配菜选择列表
    private List<String> selectList = new ArrayList<>();

    private boolean isChange = false;
    private int currentGuige;
    private int[] currentPeicai;
    //显示的价格
    private double price;
    private ImageView buyImg;
    private int type = 0;

    public int buyNum;
    public double allPrice;
    private String wzhekou;

    private double orignal; //不算折扣的价格
    double freeMaxmoney, km, kmmoney, lkmoney, psmoney, lmoney, longx, juli;
    private int shop_type = 1;   //1:外卖  2：自取

    public DishDetailDialog.changeMenuListener getChangeMenuListener() {
        return changeMenuListener;
    }

    public void setChangeMenuListener(DishDetailDialog.changeMenuListener changeMenuListener) {
        this.changeMenuListener = changeMenuListener;
    }

    private void setStatusBar(Window window) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        } else {
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        collapsing.setStatusBarScrimColor(getResources().getColor(R.color.touming));
    }

    public DishDetailDialog() {
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.activity_dish_detail, null);
        ButterKnife.bind(this, view);
        dialog = new Dialog(getActivity(), R.style.style_dialog);
        dialog.setContentView(view);
        dialog.show();

        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM); //可设置dialog的位置
        window.getDecorView().setPadding(0, 0, 0, 0); //消除边距

        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;   //设置宽度充满屏幕
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
        shoppingCart.setOnClickListener(onClickListener);
        settlement.setOnClickListener(onClickListener);
        return dialog;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        setStatusBar(dialog.getWindow());
        initData();
        initRecyclerview();
        bgLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noShop.setVisibility(View.GONE);
                bgLayout.setVisibility(View.GONE);
                cardShopLayout.setVisibility(View.GONE);
            }
        });
        this.getDialog().setOnKeyListener(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public static DishDetailDialog newInstance(String id, GoodsBean data, int buyNum, double allPrice, Map<String, Double> shopInfo, int type) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(BUNDLE_DATA, data);
        bundle.putString(BUNDLE_ID, id);
        bundle.putInt(BUNDLE_BUYNUM, buyNum);
        bundle.putDouble(BUNDLE_TOTALPRICE, allPrice);
        bundle.putDouble("freeMaxmoney", shopInfo.get("freeMaxmoney"));
        bundle.putDouble("longx", shopInfo.get("longx"));
        bundle.putDouble("juli", shopInfo.get("juli"));
        bundle.putDouble("km", shopInfo.get("km"));
        bundle.putDouble("kmmoney", shopInfo.get("kmmoney"));
        bundle.putDouble("lkmoney", shopInfo.get("lkmoney"));
        bundle.putDouble("lmoney", shopInfo.get("lmoney"));
        bundle.putInt("type", type);
        DishDetailDialog fragment = new DishDetailDialog();
        fragment.setArguments(bundle);
        return fragment;
    }

    private void initData() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            cid = arguments.getString(BUNDLE_ID);
            goodsBean = (GoodsBean) arguments.getSerializable(BUNDLE_DATA);
            buyNum = arguments.getInt(BUNDLE_BUYNUM);
            allPrice = arguments.getDouble(BUNDLE_TOTALPRICE);
            GlideUtils.load(getContext(), goodsBean.getGoodsphoto(), ivDishDetail, GlideUtils.Shape.ShopPic);
            tvDishDetailDishName.setText(goodsBean.getGoodsname());
            tvDishDetailDishContent.setText(goodsBean.getGoodsname());
            caidanBean = goodsBean.getCaidan();
            guigeBean = goodsBean.getGuige();
            wzhekou = goodsBean.getWzhekou();
            freeMaxmoney = arguments.getDouble("freeMaxmoney", -1);
            longx = arguments.getDouble("longx", 0);
            juli = arguments.getDouble("juli", 0);
            km = arguments.getDouble("km", -1);
            kmmoney = arguments.getDouble("kmmoney", 0);
            lkmoney = arguments.getDouble("lkmoney", 0);
            lmoney = arguments.getDouble("lmoney", 0);
            shop_type = arguments.getInt("type", 1);
            if (shop_type == 1) {
                tvPsmoney.setVisibility(View.VISIBLE);
            }else {
                tvPsmoney.setVisibility(View.GONE);
            }
            Log.i("peisongfei", freeMaxmoney + " " + longx + " " + juli + " "+ km + " "+kmmoney
            +" " +lkmoney + " " +lmoney);
            changeShopcar();
            ivActivityGoodsBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
        }
    }

    private void initRecyclerview() {
        rvGuige.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvPeicai.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvPeicai.setNestedScrollingEnabled(false);
        rvGuige.setNestedScrollingEnabled(false);
        rvGuige.setItemAnimator(null);
        rvPeicai.setItemAnimator(null);
        if ((caidanBean != null && !caidanBean.isEmpty()) || (guigeBean != null && !guigeBean.isEmpty())) {
            type = 2;
            llAddSub.setVisibility(View.GONE);
            tvGuige.setVisibility(View.VISIBLE);
            if (goodsBean.getNumber() > 0) {
                tvGuigeNum.setVisibility(View.VISIBLE);
                tvGuigeNum.setText(goodsBean.getNumber() + "");
            } else {
                tvGuigeNum.setVisibility(View.GONE);
            }
            if (guigeBean != null && !guigeBean.isEmpty()) {
                price = getPrice(guigeBean.get(0).getPrimoney());
                llGuige.setVisibility(View.VISIBLE);
                guiGeAdapter = new GuiGeAdapter(getContext(), guigeBean);
                guiGeAdapter.setListener(this);
                rvGuige.setAdapter(guiGeAdapter);
            } else {
                price = getPrice(goodsBean.getGoodsprice());
                llGuige.setVisibility(View.GONE);
            }
            if (caidanBean != null && !caidanBean.isEmpty()) {
                rvPeicai.setVisibility(View.VISIBLE);
                //初始化配菜表头
                for (int i = 0; i < caidanBean.size(); i++) {
                    CaidanBean bean = caidanBean.get(i);
                    caidanList.add(bean);
                    for (int j = 0; j < bean.getPeicai().size(); j++) {
                        CaidanBean.PeicaiBean peicaiBean = bean.getPeicai().get(j);
                        peicaiBean.setId(i);
                        peicaiBean.setPosition(j);
//                        peicaiBean.setIsselect(false);
                        if (peicaiBean.isselect()) {
                            price += getPrice(peicaiBean.getPcprice());
                        }
                        peicaiList.add(peicaiBean);
                    }
                }
                peiCaiAdapter = new PeiCaiAdapter(getContext(), peicaiList, caidanList);
                peiCaiAdapter.setListener(this);
                rvPeicai.setAdapter(peiCaiAdapter);
            } else {
                rvPeicai.setVisibility(View.GONE);
            }
        } else {
            type = 1;
            llAddSub.setVisibility(View.VISIBLE);
            tvGuige.setVisibility(View.GONE);
            tvGuigeNum.setVisibility(View.GONE);
            llGuige.setVisibility(View.GONE);
            rvPeicai.setVisibility(View.GONE);
            if (goodsBean.getNumber() == 0) {
                ivGoodsMinus.setVisibility(View.GONE);
                tvGoodsSelectNum.setVisibility(View.GONE);
            }else {
                ivGoodsMinus.setVisibility(View.VISIBLE);
                tvGoodsSelectNum.setVisibility(View.VISIBLE);
                tvGoodsSelectNum.setText(goodsBean.getNumber() + "");
            }
            price = getPrice(goodsBean.getGoodsprice());
        }
        orignal = price;
        price = getDiscount(price+"");
        if ((!TextUtils.isEmpty(wzhekou) && !TextUtils.equals(wzhekou, "0"))
                || (!TextUtils.isEmpty(goodsBean.getGzhekou()) && !TextUtils.equals(goodsBean.getGzhekou(), "0"))) {
            tvPrePrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            tvPrePrice.setVisibility(View.VISIBLE);
            tvPrePrice.setText("$" + orignal);
        }else {
            tvPrePrice.setVisibility(View.GONE);
        }
        tvDishDetailDishPrice.setText("$" + price + "/" + getString(R.string.fen));
    }

    //得到价格,保留两位小数
    private double getPrice(String price) {
        return TextUtils.isEmpty(price) ? 0 : NumberFormatUtil.round(Double.valueOf(price), 2);
    }

    private double getDiscount(String p) {
        double price = TextUtils.isEmpty(p) ? 0 : NumberFormatUtil.round(Double.valueOf(p), 2);
        if (!TextUtils.isEmpty(goodsBean.getGzhekou()) && !TextUtils.equals(goodsBean.getGzhekou(), "0")) {
            return NumberFormatUtil.round(price * (100 - Double.parseDouble(goodsBean.getGzhekou())) / 100, 2);
        }else {
            if (!TextUtils.isEmpty(wzhekou) && !TextUtils.equals(wzhekou, "0")) {
                return NumberFormatUtil.round(price * (100 - Double.parseDouble(goodsBean.getWzhekou())) / 100, 2);
            }else {
                return NumberFormatUtil.round(price, 2);
            }
        }
    }

    private double getPsmoney(double allPirce,double peisong, double freeMaxmoney, double km, double kmmoney, double lkmoney) {
        if (freeMaxmoney > 0 && allPirce >= freeMaxmoney) {
            return 0;
        } else if (km >= 0 && peisong <= km) {
            return kmmoney;
        } else if (km >= 0 && peisong > km) {
            return NumberFormatUtil.round((peisong - km) * lkmoney + kmmoney, 2);
        } else {
            return NumberFormatUtil.round(peisong * lkmoney, 2);
        }
    }

    @Override
    public void onGuiGeSelected(int select, int preSelet) {
        currentGuige = select;
        orignal = orignal - getPrice(guigeBean.get(preSelet).getPrimoney()) + getPrice(guigeBean.get(select).getPrimoney());
        price = price - getDiscount(guigeBean.get(preSelet).getPrimoney()) + getDiscount(guigeBean.get(select).getPrimoney());
        tvPrePrice.setText("$" + NumberFormatUtil.round(orignal, 2) + "/" + getString(R.string.fen));
        tvDishDetailDishPrice.setText("$" + NumberFormatUtil.round(price, 2) + "/" + getString(R.string.fen));
        if (changeMenuListener != null) {
            changeMenuListener.onGuiGeSelected(cid, select, preSelet);
        }
    }

    @Override
    public void onPeiCaiSelected(int pmenu, int pdish, boolean isSelect) {
        if (isSelect) {
            //选中了这个配菜
            price += getDiscount(caidanList.get(pmenu).getPeicai().get(pdish).getPcprice());
            orignal += getPrice(caidanList.get(pmenu).getPeicai().get(pdish).getPcprice());
            peicaiList.get(pdish).setIsselect(true);
        } else {
            price -= getDiscount(caidanList.get(pmenu).getPeicai().get(pdish).getPcprice());
            orignal -= getPrice(caidanList.get(pmenu).getPeicai().get(pdish).getPcprice());
            peicaiList.get(pdish).setIsselect(false);
        }
        price = NumberFormatUtil.round(price, 2);
        orignal = NumberFormatUtil.round(orignal, 2);
        tvPrePrice.setText("$" + orignal + "/" + getString(R.string.fen));
        tvDishDetailDishPrice.setText("$" + price + "/" + getString(R.string.fen));
        if (changeMenuListener != null) {
            changeMenuListener.onPeiCaiSelected(cid, pmenu, pdish, isSelect);
        }
    }

    @OnClick({R.id.ivGoodsMinus, R.id.ivGoodsAdd, R.id.tv_guige})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivGoodsMinus:
                //删除商品--无规格
                if (goodsBean.getNumber() > 0) {
                    int number = goodsBean.getNumber() - 1;
                    buyNum--;
                    allPrice -= price;
                    changeShopcar();
                    if (number == 0) {
                        ivGoodsMinus.setAnimation(getHiddenAnimation());
                        tvGoodsSelectNum.setAnimation(getHiddenAnimation());
                        ivGoodsMinus.setVisibility(View.GONE);
                        tvGoodsSelectNum.setVisibility(View.GONE);
                    }else {
                        tvGoodsSelectNum.setText(number + "");
                    }
                    goodsBean.setNumber(number);
                    if (changeMenuListener != null) {
                        changeMenuListener.sub(cid, false);
                    }
                }
                break;
            case R.id.ivGoodsAdd:
                startAnim(view);
                //添加商品--无规格
//                if (shopcar == null) { shopcar = new ArrayList<>(); }
//                for (GoodsBean data : shopcar) {
//                    if (TextUtils.equals(data.getCid(), goodsBean.getCid())) {
//                        int number = data.getNumber() + 1;
//                        data.setNumber(number);
//                        return;
//                    }
//                }
//                //如果没有在列表中找到的话，新增一个商品信息，并且没有规格和配菜，直接添加当前goodsBean
//                shopcar.add(goodsBean);
                int number = goodsBean.getNumber() + 1;
                if (number == 1) {
                    ivGoodsMinus.setAnimation(getShowAnimation());
                    tvGoodsSelectNum.setAnimation(getShowAnimation());
                    ivGoodsMinus.setVisibility(View.VISIBLE);
                    tvGoodsSelectNum.setVisibility(View.VISIBLE);
                }
                buyNum++;
                allPrice += price;
                changeShopcar();
                goodsBean.setNumber(number);
                tvGoodsSelectNum.setText(number + "");
                if (changeMenuListener != null) {
                    changeMenuListener.add(cid, "0", false);
                }
                break;
            case R.id.tv_guige:
                startAnim(view);
                //添加商品--有规格
                tvGuigeNum.setVisibility(View.VISIBLE);
                int num = goodsBean.getNumber() + 1;
                goodsBean.setNumber(num);
                tvGuigeNum.setText(num + "");
                buyNum++;
                allPrice += price;
                changeShopcar();
                if (changeMenuListener != null) {
                    changeMenuListener.add(cid, "2", true);
                }
                break;
        }
    }


    private void changeShopcar() {
        if (buyNum > 0) {
            shopCartNum.setVisibility(View.VISIBLE);
            shopCartNum.setText(buyNum + "");
        } else {
            shopCartNum.setVisibility(View.GONE);
        }
        allPrice = NumberFormatUtil.round(allPrice, 2);
        totalPrice.setVisibility(View.VISIBLE);
        totalPrice.setText("$" + allPrice);
        //计算配送费
        if (juli / 1000 > longx) {
            tvPsmoney.setText(R.string.chao_chu_pei_song);
        }else if (MyApplication.mLocation == null || (MyApplication.mLocation.latitude == 30 && MyApplication.mLocation.longitude == 30)) {
            //定位失败
            tvPsmoney.setText(R.string.ding_wei_shi_bai);
        }else {
            psmoney = getPsmoney(allPrice, juli / 1000, freeMaxmoney, km, kmmoney, lkmoney);
            tvPsmoney.setText(getString(R.string.pei_song_fei) + ":$" + psmoney);
        }
        //计算最低起送费
        if (lmoney > 0) {
            double sub = NumberFormatUtil.round(lmoney - allPrice , 2);
            if (sub > 0) {
                if (buyNum > 0) {
                    if (MyApplication.isEnglish) settlement.setText("REMAINING $" + sub);
                    else settlement.setText("还差 $" + sub);
                }else {
                    if (MyApplication.isEnglish) settlement.setText("$" + lmoney +  "MIN");
                    else settlement.setText("$" + lmoney +  "起送");
                }
                settlement.setBackgroundResource(R.color.list_line);
                settlement.setClickable(false);
            }else {
                settlement.setText(R.string.settlement);
                settlement.setClickable(true);
                settlement.setBackgroundResource(R.color.color_orange);
            }
        }else {
            settlement.setText(R.string.settlement);
            settlement.setClickable(true);
            settlement.setBackgroundResource(R.color.color_orange);
        }
    }

    /**
     * 开始动画
     *
     * @param view
     */
    private void startAnim(View view) {
        buyImg = new ImageView(getContext());
        buyImg.setBackgroundResource(R.mipmap.icon_circle);// 设置buyImg的图片
        int[] loc = new int[2];
        view.getLocationInWindow(loc);
        int[] startLocation = new int[2];// 一个整型数组，用来存储按钮的在屏幕的X、Y坐标
        view.getLocationInWindow(startLocation);// 这是获取购买按钮的在屏幕的X、Y坐标（这也是动画开始的坐标）
        setAnim(buyImg, startLocation);// 开始执行动画
    }

    /**
     * 设置动画（点击添加商品）
     *
     * @param v
     * @param startLocation
     */
    public void setAnim(final View v, int[] startLocation) {
        anim_mask_layout = null;
        anim_mask_layout = createAnimLayout();
        anim_mask_layout.addView(v);//把动画小球添加到动画层
        final View view = addViewToAnimLayout(anim_mask_layout, v, startLocation);
        int[] endLocation = new int[2];// 存储动画结束位置的X、Y坐标
        shopCartNum.getLocationInWindow(endLocation);
        // 计算位移
        int endX = 0 - startLocation[0] + 40;// 动画位移的X坐标
        int endY = endLocation[1] - startLocation[1];// 动画位移的y坐标

        TranslateAnimation translateAnimationX = new TranslateAnimation(0, endX, 0, 0);
        translateAnimationX.setInterpolator(new LinearInterpolator());
        translateAnimationX.setRepeatCount(0);// 动画重复执行的次数
        translateAnimationX.setFillAfter(true);

        TranslateAnimation translateAnimationY = new TranslateAnimation(0, 0, 0, endY);
        translateAnimationY.setInterpolator(new AccelerateInterpolator());
        translateAnimationY.setRepeatCount(0);// 动画重复执行的次数
        translateAnimationY.setFillAfter(true);

        AnimationSet set = new AnimationSet(false);
        set.setFillAfter(false);
        set.addAnimation(translateAnimationY);
        set.addAnimation(translateAnimationX);
        set.setDuration(400);// 动画的执行时间
        view.startAnimation(set);
        // 动画监听事件
        set.setAnimationListener(new Animation.AnimationListener() {
            // 动画的开始
            @Override
            public void onAnimationStart(Animation animation) {
                v.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            // 动画的结束
            @Override
            public void onAnimationEnd(Animation animation) {
                v.setVisibility(View.GONE);
            }
        });

    }

    /**
     * 初始化动画图层
     *
     * @return
     */
    private ViewGroup createAnimLayout() {
        ViewGroup rootView = (ViewGroup) dialog.getWindow().getDecorView();
        LinearLayout animLayout = new LinearLayout(getContext());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        animLayout.setLayoutParams(lp);
        animLayout.setId(Integer.MAX_VALUE - 1);
        animLayout.setBackgroundResource(android.R.color.transparent);
        rootView.addView(animLayout);
        return animLayout;
    }

    /**
     * 将View添加到动画图层
     *
     * @param parent
     * @param view
     * @param location
     * @return
     */
    private View addViewToAnimLayout(final ViewGroup parent, final View view, int[] location) {
        int x = location[0];
        int y = location[1];
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.leftMargin = x;
        lp.topMargin = y;
        view.setLayoutParams(lp);
        return view;
    }

    /**
     * 显示减号的动画
     *
     * @return
     */
    private Animation getShowAnimation() {
        AnimationSet set = new AnimationSet(true);
        RotateAnimation rotate = new RotateAnimation(0, 720, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        set.addAnimation(rotate);
        TranslateAnimation translate = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 2f
                , TranslateAnimation.RELATIVE_TO_SELF, 0
                , TranslateAnimation.RELATIVE_TO_SELF, 0
                , TranslateAnimation.RELATIVE_TO_SELF, 0);
        set.addAnimation(translate);
        AlphaAnimation alpha = new AlphaAnimation(0, 1);
        set.addAnimation(alpha);
        set.setDuration(500);
        return set;
    }


    /**
     * 隐藏减号的动画
     *
     * @return
     */
    private Animation getHiddenAnimation() {
        AnimationSet set = new AnimationSet(true);
        RotateAnimation rotate = new RotateAnimation(0, 720, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        set.addAnimation(rotate);
        TranslateAnimation translate = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 0
                , TranslateAnimation.RELATIVE_TO_SELF, 4f
                , TranslateAnimation.RELATIVE_TO_SELF, 0
                , TranslateAnimation.RELATIVE_TO_SELF, 0);
        set.addAnimation(translate);
        AlphaAnimation alpha = new AlphaAnimation(1, 0);
        set.addAnimation(alpha);
        set.setDuration(500);
        return set;
    }

    public interface changeMenuListener {
        void add(String cid, String type, boolean isGuige);

        void sub(String cid, boolean isGuige);

        void onGuiGeSelected(String id, int select, int preSelet);

        void onPeiCaiSelected(String id, int pmenu, int pdish, boolean isSelect);
    }

    public void addNum(String gid, double price) {
        if (TextUtils.equals(cid, gid)) {
//            int number = goodsBean.getNumber()+1;
            int number = goodsBean.getNumber();
//            goodsBean.setNumber(number);
            if (type == 1) {
                tvGoodsSelectNum.setText(number + "");
            } else {
                tvGuigeNum.setText(number + "");
            }
        }
        buyNum++;
        allPrice += price;
        changeShopcar();
    }

    public void subNum(String gid, double price) {
        if (TextUtils.equals(cid, gid)) {
            if (goodsBean.getNumber() >= 0) {
//                int number = goodsBean.getNumber()-1;
                int number = goodsBean.getNumber();
//                goodsBean.setNumber(number);
                if (type == 1) {
                    if (number > 0) {
                        tvGoodsSelectNum.setText(number + "");
                    }else {
                        ivGoodsMinus.setVisibility(View.GONE);
                        tvGoodsSelectNum.setVisibility(View.GONE);
                    }
                } else {
                    if (number > 0) {
                        tvGuigeNum.setText(number + "");
                    }else {
                        tvGuigeNum.setVisibility(View.GONE);
                    }
                }
            }
        }
        buyNum--;
        allPrice -= price;
        changeShopcar();
    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (bgLayout.getVisibility() == View.VISIBLE) {
                bgLayout.setVisibility(View.GONE);
                cardShopLayout.setVisibility(View.GONE);
                return true;
            }else {
                return false;
            }
        }
        return false;
    }
}
