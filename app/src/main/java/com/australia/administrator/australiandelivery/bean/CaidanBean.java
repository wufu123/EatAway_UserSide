package com.australia.administrator.australiandelivery.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by local123 on 2017/11/28.
 */

public class CaidanBean implements Serializable{
    /**
     * pid : 6
     * goodsid : 1986
     * pcaidan : 红叉
     * addtime : 2017-11-08 13:58:02
     * peicai : [{"pcid":"1","pcname":"sad","pcprice":"0.99","fid":"6","addtime":"2017-11-09 16:22:44"},
     {"pcid":"2","pcname":"提3阿发","pcprice":"0.99","fid":"6","addtime":"2017-11-09 16:23:12"},{"pcid":"3","pcname":"撒发
     达","pcprice":"200.00","fid":"6","addtime":"2017-11-09 16:23:35"},{"pcid":"4","pcname":"什
     么","pcprice":"200.00","fid":"6","addtime":"0000-00-00 00:00:00"},
     {"pcid":"5","pcname":"asdfas","pcprice":"12.00","fid":"6","addtime":"0000-00-00 00:00:00"}]
     */

    private String pid;
    private String goodsid;
    private String pcaidan;
    private String addtime;
    private List<PeicaiBean> peicai;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getGoodsid() {
        return goodsid;
    }

    public void setGoodsid(String goodsid) {
        this.goodsid = goodsid;
    }

    public String getPcaidan() {
        return pcaidan;
    }

    public void setPcaidan(String pcaidan) {
        this.pcaidan = pcaidan;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public List<PeicaiBean> getPeicai() {
        return peicai;
    }

    public void setPeicai(List<PeicaiBean> peicai) {
        this.peicai = peicai;
    }

    public static class PeicaiBean implements Serializable{
        /**
         * pcid : 1
         * pcname : sad
         * pcprice : 0.99
         * fid : 6
         * addtime : 2017-11-09 16:22:44
         */

        private String pcid;
        private String pcname;
        private String pcprice;
        private String fid;
        private String addtime;
        private boolean isselect;
        private int number;
        private int id;
        private int position;

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public boolean isselect() {
            return isselect;
        }

        public void setIsselect(boolean isselect) {
            this.isselect = isselect;
        }

        public String getPcid() {
            return pcid;
        }

        public void setPcid(String pcid) {
            this.pcid = pcid;
        }

        public String getPcname() {
            return pcname;
        }

        public void setPcname(String pcname) {
            this.pcname = pcname;
        }

        public String getPcprice() {
            return pcprice;
        }

        public void setPcprice(String pcprice) {
            this.pcprice = pcprice;
        }

        public String getFid() {
            return fid;
        }

        public void setFid(String fid) {
            this.fid = fid;
        }

        public String getAddtime() {
            return addtime;
        }

        public void setAddtime(String addtime) {
            this.addtime = addtime;
        }
    }
}
