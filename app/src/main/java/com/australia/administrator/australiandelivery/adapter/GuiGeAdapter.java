package com.australia.administrator.australiandelivery.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.australia.administrator.australiandelivery.R;
import com.australia.administrator.australiandelivery.bean.GuigeBean;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by local123 on 2017/11/28.
 */

public class GuiGeAdapter extends RecyclerView.Adapter<GuiGeAdapter.ViewHolder> {
    private Context context;
    private List<GuigeBean> mData;
    private int isSelect = 0;
    private onGuiGeSelectListener listener;

    public onGuiGeSelectListener getListener() {
        return listener;
    }

    public void setListener(onGuiGeSelectListener listener) {
        this.listener = listener;
    }

    public GuiGeAdapter(Context context, List<GuigeBean> data) {
        this.context = context;
        mData = data;
    }

    public void setData(List<GuigeBean> data) {
        mData = data;
        notifyDataSetChanged();
    }

    public void addData(List<GuigeBean> data) {
        if (data != null && data.size() > 0) {
            mData.addAll(data);
            notifyDataSetChanged();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recyclerview_guige, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.tvGuige.setText(mData.get(position).getGuige());
        holder.tvGuigePrice.setText("$" + mData.get(position).getPrimoney());
        if (position == isSelect) {
            holder.rbGuige.setChecked(true);
        } else {
            holder.rbGuige.setChecked(false);
        }
        holder.rlGuige.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    holder.rbGuige.setChecked(true);
                    int pre = isSelect;
                    isSelect = position;
                    notifyItemChanged(pre);
                    listener.onGuiGeSelected(position, pre);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mData != null) {
            return mData.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.rb_guige)
        RadioButton rbGuige;
        @Bind(R.id.tv_guige)
        TextView tvGuige;
        @Bind(R.id.rl_guige)
        RelativeLayout rlGuige;
        @Bind(R.id.tv_guige_price)
        TextView tvGuigePrice;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface onGuiGeSelectListener {
        void onGuiGeSelected(int select, int preSelet);
    }
}
