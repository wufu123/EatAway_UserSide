package com.australia.administrator.australiandelivery.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.australia.administrator.australiandelivery.R;
import com.australia.administrator.australiandelivery.activity.DishDetailActivity;
import com.australia.administrator.australiandelivery.activity.GoodsListActivity1;
import com.australia.administrator.australiandelivery.bean.CaidanBean;
import com.australia.administrator.australiandelivery.bean.GoodsBean;
import com.australia.administrator.australiandelivery.bean.GuigeBean;
import com.australia.administrator.australiandelivery.bean.ShopDetailsBean;
import com.australia.administrator.australiandelivery.comm.DishDetailDialog;
import com.australia.administrator.australiandelivery.comm.GoodsListEvent;
import com.australia.administrator.australiandelivery.comm.MessageEvent;
import com.australia.administrator.australiandelivery.utils.GlideUtils;
import com.australia.administrator.australiandelivery.utils.NumberFormatUtil;
import com.australia.administrator.australiandelivery.utils.ObjectCloner;
import com.google.android.gms.wearable.DataApi;

import org.greenrobot.eventbus.EventBus;
import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

public class RightMenuAdapter extends RecyclerView.Adapter<RightMenuAdapter.ViewHolder> {
    private List<GoodsBean> dataList;
    private Context mContext;
    private int[] goodsNum;
    public int buyNum;
    public double totalPrice;
    private int[] mSectionIndices;
    private int[] mGoodsCategoryBuyNums;
    private Activity mActivity;
    private TextView shopCart;
    private ImageView buyImg;
    private List<ShopDetailsBean.MsgBean.ShopmessageBean> goodscatrgoryEntities;
    private String[] mSectionLetters;
    public List<GoodsBean> selectGoods = new ArrayList<>();
    private ViewHolder viewHolder;
    private showDialog listener;

    private boolean isChange = true;

    public showDialog getListener() {
        return listener;
    }

    public void setListener(showDialog listener) {
        this.listener = listener;
    }

    public RightMenuAdapter(Context context, List<GoodsBean> items
            , List<ShopDetailsBean.MsgBean.ShopmessageBean> goodscatrgoryEntities) {
        this.mContext = context;
        this.dataList = items;
        this.goodscatrgoryEntities = goodscatrgoryEntities;
        initGoodsNum();
        mSectionIndices = getSectionIndices();
        mSectionLetters = getSectionLetters();
        mGoodsCategoryBuyNums = getBuyNums();
        setHasStableIds(true);
    }

    public List<GoodsBean> getCart() {
        return selectGoods;
    }

    public void setShopCart(TextView shopCart) {
        this.shopCart = shopCart;
    }

    public void setmActivity(Activity mActivity) {
        this.mActivity = mActivity;
    }

    /**
     * 初始化各个商品的购买数量
     */
    private void initGoodsNum() {
        int leng = dataList.size();
        goodsNum = new int[leng];
        for (int i = 0; i < leng; i++) {
            goodsNum[i] = 0;
        }
    }

    /**
     * 开始动画
     *
     * @param view
     */
    private void startAnim(View view) {
        buyImg = new ImageView(mActivity);
        buyImg.setBackgroundResource(R.mipmap.icon_circle);// 设置buyImg的图片
        int[] loc = new int[2];
        view.getLocationInWindow(loc);
        int[] startLocation = new int[2];// 一个整型数组，用来存储按钮的在屏幕的X、Y坐标
        view.getLocationInWindow(startLocation);// 这是获取购买按钮的在屏幕的X、Y坐标（这也是动画开始的坐标）
        ((GoodsListActivity1) mActivity).setAnim(buyImg, startLocation);// 开始执行动画
    }

    /**
     * 判断商品是否有添加到购物车中
     *
     * @param i  条目下标
     * @param vh ViewHolder
     */
    private void isSelected(int i, ViewHolder vh) {
        if (i == 0) {
            vh.tvGoodsSelectNum.setVisibility(View.GONE);
            vh.ivGoodsMinus.setVisibility(View.GONE);
        } else {
            vh.tvGoodsSelectNum.setVisibility(View.VISIBLE);
            vh.tvGoodsSelectNum.setText(i + "");
            vh.ivGoodsMinus.setVisibility(View.VISIBLE);
        }
    }

    public void setJia(String id, String type, int gp, boolean isGuige) {
        for (int i = 0; i < dataList.size(); i++) {
            if (dataList.get(i).getGoodsid().equals(id)) {
                if (gp == -1) {
                    //不是从购物车点的
                    if (isGuige) {
                        //从有规格的Dialog里点的
                        Log.i("ddddd", "从有规格的Dialog里点的");
                        jia_dialog(i);
                    }else {
                        //从无规格的Dialog里或者正常adapter点的
                        Log.i("ddddd", "从无规格的Dialog里或者正常adapter点的或无规格的购物车");
                        jia(i, viewHolder, type);
                    }
                }else {
                    //从购物车里点的
                    if (isGuige) {
                        Log.i("ddddd", "从有规格的购物车里点的");
                        jia_guige(i, viewHolder, gp);
                    }else {
                        //或无规格的购物车
                        jia(i, viewHolder, type);
                    }
                }
            }
        }
    }

    public void setJian(String id, int gp, boolean isGuige) {
        for (int i = 0; i < dataList.size(); i++) {
            if (dataList.get(i).getGoodsid().equals(id)) {
                if (gp != -1) {
                    //从购物车中点击的
                    if (isGuige) {
                        //有规格
                        Log.i("ddddd", "有规格购物车-");
                        jian_guige(i, viewHolder, gp);
                    }else {
                        //无规格，可以和原来的减复用
                        Log.i("ddddd", "无规格，可以和原来的减复用-");
                        jian(i, viewHolder);
                    }
                }else {
                    //不是从购物车里点的
                    if (isGuige) {
                        Log.i("ddddd", "有规格，Dialog-");
                    }else {
                        Log.i("ddddd", "无规格，可以和原来的减复用-");
                        jian(i, viewHolder);
                    }
                }
            }
        }
    }

    public void setGuige(String id ,int select, int preSelet) {
        for (int i = 0; i < dataList.size(); i++) {
            if (dataList.get(i).getGoodsid().equals(id)) {
//                dataList.get(i).getGuige().get(preSelet).setSelect(false);
//                dataList.get(i).getGuige().get(select).setSelect(true);
                dataList.get(i).setSelectGui(dataList.get(i).getGuige().get(select));
            }
        }
    }

    public void setPeicai(String id, int pmenu, int pdish, boolean isSelect) {
        for (int i = 0; i < dataList.size(); i++) {
            if (dataList.get(i).getGoodsid().equals(id)) {
//                Log.i("peicai", "setPeicai: " + dataList.get(i).getCaidan().get(pmenu).getPeicai().get(pdish).getPcname() + " " + isSelect);
                Log.i("peicai", "setPeicai: " + pmenu + " " + pdish);
                dataList.get(i).getCaidan().get(pmenu).getPeicai().get(pdish).setIsselect(isSelect);
            }
        }
    }

    /**
     * 存放每个组里的添加购物车的数量
     *
     * @return
     */
    public int[] getBuyNums() {
        int[] letters = new int[goodscatrgoryEntities.size()];
        for (int i = 0; i < goodscatrgoryEntities.size(); i++) {
            letters[i] = goodscatrgoryEntities.get(i).getBugNum();
        }
        return letters;
    }

    /**
     * 存放每个分组的第一条的ID
     *
     * @return
     */
    private int[] getSectionIndices() {
        ArrayList<Integer> sectionIndices = new ArrayList<Integer>();
        int lastFirstPoi = -1;
        for (int i = 0; i < dataList.size(); i++) {
            if (dataList.get(i).getId() != lastFirstPoi) {
                lastFirstPoi = dataList.get(i).getId();
                sectionIndices.add(i);
            }
        }
        int[] sections = new int[sectionIndices.size()];
        for (int i = 0; i < sectionIndices.size(); i++) {
            sections[i] = sectionIndices.get(i);
        }
        return sections;
    }

    /**
     * 填充每一个分组要展现的数据
     *
     * @return
     */
    private String[] getSectionLetters() {
        String[] letters = new String[mSectionIndices.length];
        for (int i = 0; i < mSectionIndices.length; i++) {
            letters[i] = goodscatrgoryEntities.get(i).getCname();
        }
        return letters;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_goods_list_second, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public long getItemId(int position) {
        return dataList.get(position).hashCode();
    }

    public void clear() {
        mSectionIndices = new int[0];
        mSectionLetters = new String[0];
        notifyDataSetChanged();
    }

    public void restore() {
        mSectionIndices = getSectionIndices();
        mSectionLetters = getSectionLetters();
        notifyDataSetChanged();
    }

    public void clearAll() {
        if (selectGoods != null && !selectGoods.isEmpty()) {
            for (int i = 0; i < selectGoods.size(); i++) {
                for (int j = 0; j < dataList.size(); j++) {
                    if (TextUtils.equals(selectGoods.get(i).getGoodsid(), dataList.get(j).getGoodsid())) {
                        goodsNum[j] = 0;
                        mGoodsCategoryBuyNums[dataList.get(j).getId()] = 0;
                        dataList.get(j).setNumber(goodsNum[j]);
                    }
                }
            }
            mSectionIndices = new int[0];
            mSectionLetters = new String[0];
            selectGoods.clear();
            buyNum = 0;
            totalPrice = 0;
            changeShopCart();
            notifyDataSetChanged();
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        viewHolder = holder;
        //设置名
        holder.goodsCategoryName.setText(dataList.get(position).getGoodsname());

        //设置价格
        double orignal;
        if (dataList.get(position).getGuige() != null && dataList.get(position).getGuige().size() > 0) {
            orignal = TextUtils.isEmpty(dataList.get(position).getGuige().get(0).getPrimoney()) ? 0 : Double.parseDouble(dataList.get(position).getGuige().get(0).getPrimoney());
        }else {
            orignal = TextUtils.isEmpty(dataList.get(position).getGoodsprice()) ? 0 : Double.parseDouble(dataList.get(position).getGoodsprice());
        }
        if (!TextUtils.isEmpty(dataList.get(position).getGzhekou()) && !TextUtils.equals(dataList.get(position).getGzhekou(),"0")) {
            holder.tvGoodsPricePre.setVisibility(View.VISIBLE);
            double discount = orignal * (100 - Double.parseDouble(dataList.get(position).getGzhekou()))/100;
            holder.tvGoodsPrice.setText("$" + NumberFormatUtil.round(discount, 2));
            holder.tvGoodsPricePre.setText("$" + NumberFormatUtil.round(orignal, 2));
        }else {
            if (!TextUtils.isEmpty(dataList.get(position).getWzhekou()) && !TextUtils.equals(dataList.get(position).getWzhekou(), "0")) {
                holder.tvGoodsPricePre.setVisibility(View.VISIBLE);
                double discount = orignal * (100 - Double.parseDouble(dataList.get(position).getWzhekou()))/100;
                holder.tvGoodsPricePre.setText("$" + NumberFormatUtil.round(orignal, 2));
                holder.tvGoodsPrice.setText("$" + NumberFormatUtil.round(discount, 2));
            }else {
                holder.tvGoodsPricePre.setVisibility(View.GONE);
                holder.tvGoodsPrice.setText("$" + dataList.get(position).getGoodsprice());
            }
        }

        //判断是否上架
        if (dataList.get(position).getFlag().equals("2")) {
            holder.LlAddSub.setVisibility(View.GONE);
            holder.tvGuige.setVisibility(View.GONE);
            holder.tvGuigeNum.setVisibility(View.GONE);
            holder.shape.setVisibility(View.VISIBLE);
        } else {
            //上架,需判断是否为存在规格样式
            if ((dataList.get(position).getGuige() != null && dataList.get(position).getGuige().size() > 0) ||
                    (dataList.get(position).getCaidan() != null && dataList.get(position).getCaidan().size() > 0)) {
                //规格或配菜存在
                holder.tvGuige.setVisibility(View.VISIBLE);
                holder.LlAddSub.setVisibility(View.GONE);
                if (goodsNum[position] == 0) {
                    holder.tvGuigeNum.setVisibility(View.GONE);
                } else {
                    holder.tvGuigeNum.setVisibility(View.VISIBLE);
                    holder.tvGuigeNum.setText(goodsNum[position] + "");
                }
            }else {
                //规格或配菜都不存在
                holder.tvGuige.setVisibility(View.GONE);
                holder.tvGuigeNum.setVisibility(View.GONE);
                holder.LlAddSub.setVisibility(View.VISIBLE);
                //通过判别对应位置的数量是否大于0来显示隐藏数量
                isSelected(goodsNum[position], holder);
                //加号按钮点击
                holder.ivGoodsAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        jia(position, holder, "0");
                    }
                });
                //减号点击按钮点击
                holder.ivGoodsMinus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        jian(position, holder);
                    }
                });
            }
            holder.shape.setVisibility(View.GONE);
            //新增：菜单详情
            holder.all.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        if (dataList.get(position).getGuige()!=null && !dataList.get(position).getGuige().isEmpty()) {
                            dataList.get(position).setSelectGui(dataList.get(position).getGuige().get(0));
                        }
                        listener.showDialog(dataList.get(position).getGoodsid(), dataList.get(position));
                    }
                }
            });
        }
        GlideUtils.load(mContext, dataList.get(position).getGoodsphoto(), holder.ivGoodsImage, GlideUtils.Shape.ShopPic);

    }

    private void jia(int position, ViewHolder holder, String type) {
        goodsNum[position]++;
        dataList.get(position).setNumber(goodsNum[position]);
        double realPrice = TextUtils.isEmpty(dataList.get(position).getGoodsprice()) ? 0 : Double.parseDouble(dataList.get(position).getGoodsprice());
        if (!TextUtils.isEmpty(dataList.get(position).getGzhekou()) && !TextUtils.equals(dataList.get(position).getGzhekou(), "0")) {
            dataList.get(position).setDisPrice(realPrice * Double.parseDouble(dataList.get(position).getGzhekou())/100);
            dataList.get(position).setRealPrice(realPrice * (100 - Double.parseDouble(dataList.get(position).getGzhekou()))/100);
        }else {
            if (!TextUtils.isEmpty(dataList.get(position).getWzhekou()) && !TextUtils.equals(dataList.get(position).getWzhekou(), "0")) {
                dataList.get(position).setDisPrice(realPrice * Double.parseDouble(dataList.get(position).getWzhekou())/100);
                dataList.get(position).setRealPrice(realPrice * (100 - Double.parseDouble(dataList.get(position).getWzhekou()))/100);
            }else {
                dataList.get(position).setDisPrice(0);
                dataList.get(position).setRealPrice(realPrice);
            }
        }
        if (selectGoods == null) {
            selectGoods = new ArrayList<>();
        }
        if (selectGoods.size() == 0) {
            selectGoods.add(dataList.get(position));
        } else {
            boolean isHad = false;
            for (int i = 0; i < selectGoods.size(); i++) {
                if (TextUtils.equals(selectGoods.get(i).getGoodsid(), dataList.get(position).getGoodsid())) {
                    selectGoods.get(i).setNumber(goodsNum[position]);
                    isHad = true;
                    break;
                }
            }
            if (!isHad) {
                selectGoods.add(dataList.get(position));
            }
        }
        mGoodsCategoryBuyNums[dataList.get(position).getId()]++;
        buyNum++;
        totalPrice += dataList.get(position).getRealPrice();
        if (goodsNum[position] <= 1) {
            holder.ivGoodsMinus.setAnimation(getShowAnimation());
            holder.tvGoodsSelectNum.setAnimation(getShowAnimation());
            holder.ivGoodsMinus.setVisibility(View.VISIBLE);
            holder.tvGoodsSelectNum.setVisibility(View.VISIBLE);
        }
        startAnim(holder.ivGoodsAdd);
        changeShopCart();
        if (mOnGoodsNunChangeListener != null)
            mOnGoodsNunChangeListener.onNumChange();
        isSelected(goodsNum[position], holder);
        notifyDataSetChanged();
    }

    /**
     *购物车点击减号使用的维护方法
     * @param position 代表的是datalist里的位置，用于维护datalist数据
     * @param holder
     * @param gp 购物车传来的selectlist里的位置，用于维护selectlist数据
     */
    public void jian_guige(int position, ViewHolder holder, int gp) {
        if (goodsNum[position] > 0) {
            goodsNum[position]--;
            dataList.get(position).setNumber(goodsNum[position]);
            if (goodsNum[position] == 0) {
                holder.tvGuigeNum.setVisibility(View.GONE);
            }
            mGoodsCategoryBuyNums[dataList.get(position).getId()]--;
            isSelected(goodsNum[position], holder);
            buyNum--;
            totalPrice -= selectGoods.get(gp).getRealPrice();
            if (selectGoods.get(gp).getNumber() == 0)
                selectGoods.remove(gp);
            changeShopCart();
            notifyDataSetChanged();
        }
    }

    /**
     *购物车点击加号使用的维护方法
     * @param position 代表的是datalist里的位置，用于维护datalist数据
     * @param holder
     * @param gp 购物车传来的selectlist里的位置，用于维护selectlist数据
     */
    public void jia_guige(int position, ViewHolder holder, int gp) {
        if (goodsNum[position] == 0) {
            holder.tvGuigeNum.setVisibility(View.VISIBLE);
        }
        goodsNum[position]++;
        dataList.get(position).setNumber(goodsNum[position]);
        //维护购物车中的数据
//        int number = selectGoods.get(gp).getNumber() + 1;
//        selectGoods.get(gp).setNumber(number);
        mGoodsCategoryBuyNums[dataList.get(position).getId()]++;
        isSelected(goodsNum[position], holder);
        buyNum++;
        totalPrice += selectGoods.get(gp).getRealPrice();
        if (mOnGoodsNunChangeListener != null)
            mOnGoodsNunChangeListener.onNumChange();
        isSelected(goodsNum[position], holder);
        changeShopCart();
        notifyDataSetChanged();
    }

    /**
     * 菜单详情页有规格的时候调用的加入购物车方法
     * @param position
     */
    public void jia_dialog(int position) {
        goodsNum[position]++;
        dataList.get(position).setNumber(goodsNum[position]);
        mGoodsCategoryBuyNums[dataList.get(position).getId()]++;
        buyNum++;
        if (mOnGoodsNunChangeListener != null) mOnGoodsNunChangeListener.onNumChange();
        GoodsBean bean = ObjectCloner.cloneObj(dataList.get(position));

        ArrayList<CaidanBean.PeicaiBean> peicaiBeen = new ArrayList<>();
        double realPrice = 0;       //商品实际价格
        for (CaidanBean caidanBean : dataList.get(position).getCaidan()) {
            for (CaidanBean.PeicaiBean peicaiBean : caidanBean.getPeicai()) {
                if (peicaiBean.isselect()) {
                    peicaiBeen.add(peicaiBean);
                    realPrice += TextUtils.isEmpty(peicaiBean.getPcprice())?0:Double.parseDouble(peicaiBean.getPcprice());
                }
            }
        }
        if (bean.getSelectGui() != null) {
            realPrice += TextUtils.isEmpty(bean.getSelectGui().getPrimoney()) ? 0 : Double.parseDouble(bean.getSelectGui().getPrimoney());
        }else {
            realPrice += TextUtils.isEmpty(bean.getGoodsprice()) ? 0 : Double.parseDouble(bean.getGoodsprice());
        }
        if (!TextUtils.isEmpty(bean.getGzhekou()) && !TextUtils.equals(bean.getGzhekou(), "0")) {
            bean.setDisPrice(realPrice * Double.parseDouble(bean.getGzhekou())/100);
            bean.setRealPrice(realPrice * (100 - Double.parseDouble(bean.getGzhekou()))/100);
            Log.i("shangpinzhekou", "1 " + bean.getDisPrice() + "--" + bean.getRealPrice());
        }else {
            if (!TextUtils.isEmpty(bean.getWzhekou()) && !TextUtils.equals(bean.getWzhekou(), "0")) {
                bean.setDisPrice(realPrice * Double.parseDouble(bean.getWzhekou())/100);
                bean.setRealPrice(realPrice * (100 - Double.parseDouble(bean.getWzhekou()))/100);
                Log.i("shangpinzhekou", "2 " + bean.getDisPrice() + "--" + bean.getRealPrice());
            }else {
                bean.setDisPrice(0);
                bean.setRealPrice(realPrice);
                Log.i("shangpinzhekou", "3 " + bean.getDisPrice() + "--" + bean.getRealPrice());
            }
        }
        bean.setSelectPei(peicaiBeen);
        totalPrice += bean.getRealPrice();
        if (selectGoods == null) { selectGoods = new ArrayList<>(); }
        if (selectGoods.size() > 0) {
            //如果购物车不为空需要在购物车中寻找是否包含当前规格配菜的菜品，如果找不到就add新的
            for (GoodsBean data : selectGoods) {
                if (data.getSelectGui() != null && bean.getSelectGui()!=null && TextUtils.equals(bean.getSelectGui().getGid(), data.getSelectGui().getGid())) {
                    if (isPeiCaiEquals(bean.getSelectPei(), data.getSelectPei())) {
                        int i = data.getNumber() + 1;
                        data.setNumber(i);
                        changeShopCart();
                        notifyDataSetChanged();
                        return;
                    }
                }else {
                    if (data.getSelectGui() == null && bean.getSelectGui() == null) {
                        if (isPeiCaiEquals(bean.getSelectPei(), data.getSelectPei())) {
                            int i = data.getNumber() + 1;
                            data.setNumber(i);
                            changeShopCart();
                            notifyDataSetChanged();
                            return;
                        }
                    }
                }
            }
            bean.setNumber(1);
        }else {
            //购物车为空
            bean.setNumber(1);
        }
        selectGoods.add(bean);
        changeShopCart();
        notifyDataSetChanged();
    }

    private void jian(int position, ViewHolder holder) {
        if (goodsNum[position] > 0) {
            goodsNum[position]--;
            dataList.get(position).setNumber(goodsNum[position]);
            if (goodsNum[position] == 0) {
                for (int i = 0; i < selectGoods.size(); i++) {
                    if (dataList.get(position).getGoodsid().equals(selectGoods.get(i).getGoodsid())) {
                        selectGoods.remove(i);
                    }
                }
            } else {
                for (int i = 0; i < selectGoods.size(); i++) {
                    if (selectGoods.get(i).getGoodsid().equals(dataList.get(position).getGoodsid())) {
                        selectGoods.get(i).setNumber(goodsNum[position]);
                    }
                }
            }
            mGoodsCategoryBuyNums[dataList.get(position).getId()]--;//Integer.parseInt(dataList.get(position).getGoodsid())
            isSelected(goodsNum[position], holder);
            buyNum--;
            totalPrice -= dataList.get(position).getRealPrice();
            if (goodsNum[position] <= 0) {
                holder.ivGoodsMinus.setAnimation(getHiddenAnimation());
                holder.tvGoodsSelectNum.setAnimation(getHiddenAnimation());
                holder.ivGoodsMinus.setVisibility(View.GONE);
                holder.tvGoodsSelectNum.setVisibility(View.GONE);
            }
            changeShopCart();
            if (mOnGoodsNunChangeListener != null)
                mOnGoodsNunChangeListener.onNumChange();
        } else {

        }
        notifyDataSetChanged();
    }

    private boolean isPeiCaiEquals(List<CaidanBean.PeicaiBean> data, List<CaidanBean.PeicaiBean> data1) {
         if ((data == null && data1 == null)) return true;
        else if (data != null && data1!=null && data.size() != data1.size()) return false;
//        Log.i("peicai", "isPeiCaiEquals: " + data.size() + " " + data1.size());
        if (data != null && data1 != null && data.size() == data1.size()) {
            for (int i = 0; i < data.size(); i++) {
                Log.i("peicai", "is: " + data.get(i).getPcid() + " " + data1.get(i).getPcid());
                if (!TextUtils.equals(data.get(i).getPcid(), data1.get(i).getPcid())) return false;
            }
            return true;
        }
        return false;
    }


//    private int fromInfosGetNum(GoodsBean.DishInfo info) {
//        int num = 0;
//        for (GoodsBean bean : selectGoods) {
//            for (GoodsBean.DishInfo dishInfo : bean.getDishInfos()) {
//                if (TextUtils.equals(dishInfo.getGuiid(), info.getGuiid())) {
//                    for ()
//                }
//            }
//        }
//        return num;
//    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    /**
     * 显示减号的动画
     *
     * @return
     */
    private Animation getShowAnimation() {
        AnimationSet set = new AnimationSet(true);
        RotateAnimation rotate = new RotateAnimation(0, 720, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        set.addAnimation(rotate);
        TranslateAnimation translate = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 2f
                , TranslateAnimation.RELATIVE_TO_SELF, 0
                , TranslateAnimation.RELATIVE_TO_SELF, 0
                , TranslateAnimation.RELATIVE_TO_SELF, 0);
        set.addAnimation(translate);
        AlphaAnimation alpha = new AlphaAnimation(0, 1);
        set.addAnimation(alpha);
        set.setDuration(500);
        return set;
    }


    /**
     * 隐藏减号的动画
     *
     * @return
     */
    private Animation getHiddenAnimation() {
        AnimationSet set = new AnimationSet(true);
        RotateAnimation rotate = new RotateAnimation(0, 720, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        set.addAnimation(rotate);
        TranslateAnimation translate = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 0
                , TranslateAnimation.RELATIVE_TO_SELF, 4f
                , TranslateAnimation.RELATIVE_TO_SELF, 0
                , TranslateAnimation.RELATIVE_TO_SELF, 0);
        set.addAnimation(translate);
        AlphaAnimation alpha = new AlphaAnimation(1, 0);
        set.addAnimation(alpha);
        set.setDuration(500);
        return set;
    }


    /**
     * 修改购物车状态
     */
    private void changeShopCart() {
        Log.i("price_event", "changeShopCart: " + buyNum + totalPrice);
        EventBus.getDefault().post(new MessageEvent(buyNum, totalPrice, selectGoods));
        EventBus.getDefault().post(new GoodsListEvent(mGoodsCategoryBuyNums));
        if (shopCart == null) return;
        if (buyNum > 0) {
            shopCart.setVisibility(View.VISIBLE);
            shopCart.setText(buyNum + "");
        } else {
            shopCart.setVisibility(View.GONE);
        }
    }

    private OnShopCartGoodsChangeListener mOnGoodsNunChangeListener = null;

    public void setOnShopCartGoodsChangeListener(OnShopCartGoodsChangeListener e) {
        mOnGoodsNunChangeListener = e;
    }

    public interface OnShopCartGoodsChangeListener {
        public void onNumChange();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public final ImageView ivGoodsImage;
        public final TextView goodsCategoryName;
        public final TextView tvGoodsPricePre;
        public final LinearLayout goodsInfo;
        public final TextView tvGoodsPrice;
        public final ImageView ivGoodsMinus;
        public final TextView tvGoodsSelectNum;
        public final ImageView ivGoodsAdd;
        public final View root;
        public final RelativeLayout all;
        public final RelativeLayout shape;

        //规格字样，规格数量标签
        public TextView tvGuige;
        public TextView tvGuigeNum;
        public LinearLayout LlAddSub;

        public ViewHolder(View root) {
            super(root);
            LlAddSub = (LinearLayout) root.findViewById(R.id.ll_add_sub);
            tvGuige = (TextView) root.findViewById(R.id.tv_guige);
            tvGuigeNum = (TextView) root.findViewById(R.id.tv_guige_num);
            ivGoodsImage = (ImageView) root.findViewById(R.id.ivGoodsImage);
            goodsCategoryName = (TextView) root.findViewById(R.id.goodsCategoryName);
            tvGoodsPricePre = (TextView) root.findViewById(R.id.tvGoodsPricePre);
            goodsInfo = (LinearLayout) root.findViewById(R.id.goodsInfo);
            tvGoodsPrice = (TextView) root.findViewById(R.id.tvGoodsPrice);
            ivGoodsMinus = (ImageView) root.findViewById(R.id.ivGoodsMinus);
            tvGoodsSelectNum = (TextView) root.findViewById(R.id.tvGoodsSelectNum);
            ivGoodsAdd = (ImageView) root.findViewById(R.id.ivGoodsAdd);
            all = (RelativeLayout) root.findViewById(R.id.rl_goods_list);
            shape = (RelativeLayout) root.findViewById(R.id.rl_shape_item);
            //中划线
            tvGoodsPricePre.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            this.root = root;
        }
    }

    public interface showDialog {
        void showDialog(String id, GoodsBean goodsBean);
    }

}
