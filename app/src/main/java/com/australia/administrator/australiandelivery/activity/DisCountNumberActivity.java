package com.australia.administrator.australiandelivery.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.australia.administrator.australiandelivery.MyApplication;
import com.australia.administrator.australiandelivery.R;
import com.australia.administrator.australiandelivery.bean.DiscountNumberBean;
import com.australia.administrator.australiandelivery.comm.BaseActivity;
import com.australia.administrator.australiandelivery.comm.Login;
import com.australia.administrator.australiandelivery.utils.HttpUtils;
import com.australia.administrator.australiandelivery.utils.ToastUtils;
import com.australia.administrator.australiandelivery.view.TopBar;
import com.braintreepayments.api.Json;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

import static com.australia.administrator.australiandelivery.utils.Contants.URL_GET_YOUHUI;

public class DisCountNumberActivity extends BaseActivity {
    private final String TAG = "DisCountNumberActivity";
    private final int DISCOUNT_RESULT_CODE = 1111;

    @Bind(R.id.tb_activity_discount_number)
    TopBar tbActivityDiscountNumber;
    @Bind(R.id.et_discount_number)
    EditText etDiscountNumber;
    @Bind(R.id.btn_discount_number)
    Button btnDiscountNumber;

    private Login login = MyApplication.getLogin();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_dis_count_number;
    }

    @Override
    protected void initDate() {
        initTopBar();
    }

    private void initTopBar() {
        tbActivityDiscountNumber.setTbCenterTv(R.string.you_hui_ma, R.color.color_white);
        tbActivityDiscountNumber.setTbLeftIv(R.drawable.img_icon_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    @OnClick(R.id.btn_discount_number)
    public void onViewClicked() {
        if (etDiscountNumber.getText().length()>0) {
            if (login != null) {
                showDialog();
                HttpUtils httpUtils = new HttpUtils(URL_GET_YOUHUI) {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        hidDialog();
                        ToastUtils.showToast(R.string.please_check_your_network_connection, DisCountNumberActivity.this);
                        Log.i(TAG, "onError: "+ e.getMessage());
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        hidDialog();
                        Log.i(TAG, "onResponse: "+ response);
                        try {
                            JSONObject o = new JSONObject(response);
                            int status = o.getInt("status");
                            if (status == 3) {
                                //优惠码已停用
                                ToastUtils.showToast(R.string.you_hui_ma_yi_ting_yong, DisCountNumberActivity.this);
                            }else if (status == 4) {
                                //优惠活动未开始
                                ToastUtils.showToast(R.string.you_hui_huo_dong_wei_kai_shi, DisCountNumberActivity.this);
                            }else if (status == 5) {
                                //优惠码已过期
                                ToastUtils.showToast(R.string.you_hui_ma_yi_guo_qi, DisCountNumberActivity.this);
                            }else if (status == 6) {
                                //优惠码超出个人使用上限
                                ToastUtils.showToast(R.string.gai_you_hui_ma_yi_chao_chu_shi_yong_shang_xian, DisCountNumberActivity.this);
                            }else if (status == 2) {
                                //优惠码已失效
                                ToastUtils.showToast(R.string.you_hui_ma_yi_shi_xiao, DisCountNumberActivity.this);
                            }else if (status == 1) {
                                DiscountNumberBean bean = new Gson().fromJson(response, DiscountNumberBean.class);
                                Intent i = new Intent();
                                i.putExtra("yid", bean.getMsg().getYid());
                                if (bean.getMsg().getMoney() != null && !"".equals(bean.getMsg().getMoney())) {
                                    i.putExtra("discount", Double.parseDouble(bean.getMsg().getMoney()));
                                }else {
                                    i.putExtra("discount", 0.0);
                                }
                                if (bean.getMsg().getState() != null && !"".equals(bean.getMsg().getState())) {
                                    i.putExtra("state", Integer.parseInt(bean.getMsg().getState()));
                                }else {
                                    i.putExtra("state", 0);
                                }
                                i.putExtra("ycode", etDiscountNumber.getText().toString());
                                setResult(DISCOUNT_RESULT_CODE, i);
                                ToastUtils.showToast(R.string.dui_huan_cheng_gong, DisCountNumberActivity.this);
                                finish();
                            }else if (status == 9) {
                                MyApplication.saveLogin(null);
                                ToastUtils.showToast(R.string.Please_Log_on_again, DisCountNumberActivity.this);
                                goToActivity(LoginActivity.class);
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
                Log.i(TAG, "onViewClicked: " + login.getUserId());
                httpUtils.addParam("userid", login.getUserId());
                httpUtils.addParam("token", login.getToken());
                httpUtils.addParam("codema", etDiscountNumber.getText().toString());
                httpUtils.clicent();
            }else {
                ToastUtils.showToast(R.string.Please_Log_on_again, this);
                startActivity(new Intent(this, LoginActivity.class));
                finish();
            }
        }else {
            ToastUtils.showToast(R.string.you_hui_ma_xin_xi_bu_neng_wei_kong, this);
        }
    }
}
