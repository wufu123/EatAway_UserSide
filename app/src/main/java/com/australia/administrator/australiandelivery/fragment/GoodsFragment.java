package com.australia.administrator.australiandelivery.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.TextView;
import android.widget.Toast;


import com.australia.administrator.australiandelivery.MyApplication;
import com.australia.administrator.australiandelivery.R;
import com.australia.administrator.australiandelivery.activity.GoodsListActivity1;
import com.australia.administrator.australiandelivery.activity.PickupOrdersActivity;
import com.australia.administrator.australiandelivery.activity.SubmitOrdersActivity;
import com.australia.administrator.australiandelivery.adapter.BigramHeaderAdapter;
import com.australia.administrator.australiandelivery.adapter.RightMenuAdapter;
import com.australia.administrator.australiandelivery.adapter.RecycleGoodsCategoryListAdapter;
import com.australia.administrator.australiandelivery.adapter.ShopAdapter;
import com.australia.administrator.australiandelivery.bean.GoodsBean;
import com.australia.administrator.australiandelivery.bean.ShopDetailsBean;
import com.australia.administrator.australiandelivery.comm.DishDetailDialog;
import com.australia.administrator.australiandelivery.comm.GoodsListEvent;
import com.australia.administrator.australiandelivery.ui.ShopToDetailListener;
import com.australia.administrator.australiandelivery.utils.NoFastClickUtils;
import com.australia.administrator.australiandelivery.utils.NumberFormatUtil;
import com.australia.library.OnHeaderClickListener;
import com.australia.library.StickyHeadersBuilder;
import com.australia.library.StickyHeadersItemDecoration;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 商品
 */
public class GoodsFragment extends BaseFragment1 implements RightMenuAdapter.OnShopCartGoodsChangeListener, OnHeaderClickListener, RightMenuAdapter.showDialog
,DishDetailDialog.changeMenuListener{
    public static final int ORDER_REQUEST = 1000;
    public static final int ORDER_RESULT = 1001;

    private RecyclerView mGoodsCateGoryList;
    private RecycleGoodsCategoryListAdapter mGoodsCategoryListAdapter;
    //商品类别列表
    private List<ShopDetailsBean.MsgBean.ShopmessageBean> goodscatrgoryEntities=new ArrayList<>();
    //商品列表
    private List<GoodsBean> goodsitemEntities=new ArrayList<>();

    //存储含有标题的第一个含有商品类别名称的条目的下表
    private List<Integer> titlePois = new ArrayList<>();
    private List<Integer> num=new ArrayList<>();
    //上一个标题的小标
    private int lastTitlePoi;
    private RecyclerView recyclerView;
    private RightMenuAdapter rightMenuAdapter;
    private StickyHeadersItemDecoration top;
    private BigramHeaderAdapter headerAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    public static ShopDetailsBean shopDetailsBean;
    public static String id;
    public static Context context;
    private boolean isOnClick=false;
    private int isnum;
    public DishDetailDialog dialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_goods, container, false);
        initView(view);
        initData();
        return view;
    }
    public static GoodsFragment newInstance( String id, ShopDetailsBean bean, Context context) {
        GoodsFragment tabFragment = new GoodsFragment();
        GoodsFragment.id = id;
        GoodsFragment.shopDetailsBean = bean;
        GoodsFragment.context = context;
        return tabFragment;
    }

    public void setClear() {
        rightMenuAdapter.clearAll();
    }

    public void setJian(String id, boolean isGuige, int gp){
        rightMenuAdapter.setJian(id, gp, isGuige);
    }

    public void setJia(String id, boolean isGuige, String type, int gp){
        rightMenuAdapter.setJia(id, type, gp, isGuige);
    }

    public void setGuige(String id, int select, int preSelet) {
        rightMenuAdapter.setGuige(id, select, preSelet);
    }

    public void setPeicai(String id, int pmenu, int pdish, boolean isSelect) {
        rightMenuAdapter.setPeicai(id, pmenu, pdish, isSelect);
    }

    public List<GoodsBean> getCart(){
        return rightMenuAdapter.getCart();
    }

    private void initData() {
        int i = 0;
        int j = 0;
        boolean isFirst;
        for (ShopDetailsBean.MsgBean.ShopmessageBean dataItem : shopDetailsBean.getMsg().getShopmessage()) {
            goodscatrgoryEntities.add(dataItem);
            isFirst = true;
            for (GoodsBean goodsitemEntity : dataItem.getGoods()) {
                if (isFirst) {
                    titlePois.add(j);
                    isFirst = false;
                }
                j++;
                goodsitemEntity.setId(i);
                goodsitemEntities.add(goodsitemEntity);
            }
            i++;
        }
        mGoodsCategoryListAdapter = new RecycleGoodsCategoryListAdapter(shopDetailsBean.getMsg().getShopmessage(), getActivity());
        mGoodsCateGoryList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mGoodsCateGoryList.setAdapter(mGoodsCategoryListAdapter);
        mGoodsCategoryListAdapter.setOnItemClickListener(new RecycleGoodsCategoryListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                isOnClick=true;
                if (position < titlePois.size()) {
                    recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView,null,titlePois.get(position)+position+2);
                }
                mGoodsCategoryListAdapter.setCheckPosition(position);
            }
        });

        mLinearLayoutManager =new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLinearLayoutManager);
        rightMenuAdapter = new RightMenuAdapter(getActivity(),goodsitemEntities,shopDetailsBean.getMsg().getShopmessage());
        rightMenuAdapter.setmActivity(getActivity());
        rightMenuAdapter.setListener(this);
        headerAdapter=new BigramHeaderAdapter(getActivity(),goodsitemEntities,shopDetailsBean.getMsg().getShopmessage());
        top = new StickyHeadersBuilder()
                .setAdapter(rightMenuAdapter)
                .setRecyclerView(recyclerView)
                .setStickyHeadersAdapter(headerAdapter)
                .setOnHeaderClickListener(this)
                .build();
        recyclerView.addItemDecoration(top);
        recyclerView.setAdapter(rightMenuAdapter);

        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                isnum=newState;
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                for (int i=0;i<titlePois.size();i++){
                    if(mLinearLayoutManager.findFirstVisibleItemPosition()>= titlePois.get(i)&&!isOnClick){
                        mGoodsCategoryListAdapter.setCheckPosition(i);
                    }
                }
                if (isnum!=2)
                isOnClick=false;
            }
        });

    }


    private void initView(View view) {
        mGoodsCateGoryList = (RecyclerView)view.findViewById(R.id.goods_category_list);
        recyclerView = (RecyclerView) view.findViewById(R.id.goods_recycleView);
    }


    @Override
    public void onNumChange() {

    }

    @Override
    public void onHeaderClick(View header, long headerId) {
        TextView text = (TextView)header.findViewById(R.id.tvGoodsItemTitle);
        Toast.makeText(getActivity(), "Click on " + text.getText(), Toast.LENGTH_SHORT).show();
    }

    /**
     * 处理滑动 是两个ListView联动
     */
    private class MyOnGoodsScrollListener implements AbsListView.OnScrollListener {

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (!(lastTitlePoi == goodsitemEntities.get(firstVisibleItem).getId())) {
                lastTitlePoi =goodsitemEntities
                        .get(firstVisibleItem)
                        .getId();
                mGoodsCategoryListAdapter.setCheckPosition(goodsitemEntities
                        .get(firstVisibleItem)
                        .getId());
                mGoodsCategoryListAdapter.notifyDataSetChanged();
            }
        }
    }

    /**
     * 添加 或者  删除  商品发送的消息处理
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(GoodsListEvent event) {
        if(event.buyNums.length>0){
            for (int i=0;i<event.buyNums.length;i++){
                goodscatrgoryEntities.get(i).setBugNum(event.buyNums[i]);
            }
            mGoodsCategoryListAdapter.changeData(goodscatrgoryEntities);
        }
    }
    @Override
    public void showDialog(String id, GoodsBean goodsBean) {
        if(!NoFastClickUtils.isFastClick()) {
            FragmentTransaction manager = getFragmentManager().beginTransaction();
            double longx = TextUtils.isEmpty(shopDetailsBean.getMsg().getLongX()) ? -1 : Double.parseDouble(shopDetailsBean.getMsg().getLongX());
            double freeMaxmoney = TextUtils.isEmpty(shopDetailsBean.getMsg().getMaxprice()) ? -1 : Double.parseDouble(shopDetailsBean.getMsg().getMaxprice());
            double km = TextUtils.isEmpty(shopDetailsBean.getMsg().getMaxlong()) ? -1 : Double.parseDouble(shopDetailsBean.getMsg().getMaxlong());
            double kmmoney = TextUtils.isEmpty(shopDetailsBean.getMsg().getMaxmoney()) ? 0 : Double.parseDouble(shopDetailsBean.getMsg().getMaxmoney());
            double lkmoney = TextUtils.isEmpty(shopDetailsBean.getMsg().getLkmoney()) ? 0 : Double.parseDouble(shopDetailsBean.getMsg().getLkmoney());
            double lmoney = TextUtils.isEmpty(shopDetailsBean.getMsg().getLmoney()) ? 0 : NumberFormatUtil.round(Double.parseDouble(shopDetailsBean.getMsg().getLmoney()), 2);
            Map<String, Double> shopInfo = new HashMap<>();
            shopInfo.put("freeMaxmoney", freeMaxmoney);
            shopInfo.put("longx", longx);
            shopInfo.put("juli", shopDetailsBean.getMsg().getJuli());
            shopInfo.put("km", km);
            shopInfo.put("kmmoney", kmmoney);
            shopInfo.put("lkmoney", lkmoney);
            shopInfo.put("lmoney", lmoney);
            dialog = DishDetailDialog.newInstance(id, goodsBean, rightMenuAdapter.buyNum, rightMenuAdapter.totalPrice, shopInfo, shopDetailsBean.getMsg().getFlag());
            dialog.setChangeMenuListener(this);
            dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (v.getId() == dialog.settlement.getId()) {
                        //结算
                        if (getCart() == null || getCart().size() == 0) {
                            return;
                        }
                        MyApplication.goods = null;
                        Intent intent = new Intent();
                        if (shopDetailsBean.getMsg().getFlag() == 1) {
                            intent.setClass(getActivity(), SubmitOrdersActivity.class);
                        } else {
                            intent.setClass(getActivity(), PickupOrdersActivity.class);
                        }
                        intent.putExtra("shopid", GoodsFragment.id);
                        intent.putExtra("shopicon", shopDetailsBean.getMsg().getShophead());
                        intent.putExtra("shopname", shopDetailsBean.getMsg().getShopname());
                        intent.putExtra("time", shopDetailsBean.getMsg().getPeisongtime());
                        MyApplication.goods = getCart();
                        startActivityForResult(intent, ORDER_REQUEST);
//                    dialog.dismiss();
                    } else {
                        if (getCart().isEmpty() || getCart() == null) {
                            dialog.defaultText.setVisibility(View.VISIBLE);
                        } else {
                            dialog.defaultText.setVisibility(View.GONE);
                            dialog.shopAdapter = new ShopAdapter(getContext(), getCart());
                            dialog.shopproductListView.setAdapter(dialog.shopAdapter);
                            dialog.shopAdapter.setShopToDetailListener(new ShopToDetailListener() {
                                @Override
                                public void onUpdateDetailList(GoodsBean product, String type, int gp) {
                                    if (!product.getGuige().isEmpty() || !product.getCaidan().isEmpty()) {
                                        //有规格的购物车点击加号
                                        setJia(product.getGoodsid(), true, type, gp);
                                    } else {
                                        setJia(product.getGoodsid(), false, type, gp);
                                    }
                                    dialog.addNum(product.getGoodsid(), product.getRealPrice());
                                }

                                @Override
                                public void onRemovePriduct(GoodsBean product, int gp) {
                                    if (!product.getGuige().isEmpty() || !product.getCaidan().isEmpty()) {
                                        //有规格的购物车点击加号
                                        setJian(product.getGoodsid(), true, gp);
                                    } else {
                                        setJian(product.getGoodsid(), false, gp);
                                    }
                                    dialog.subNum(product.getGoodsid(), product.getRealPrice());
                                }
                            });
                        }

                        if (dialog.cardShopLayout.getVisibility() == View.VISIBLE) {
                            dialog.noShop.setVisibility(View.GONE);
                            dialog.bgLayout.setVisibility(View.GONE);
                            dialog.cardShopLayout.setVisibility(View.GONE);
                        } else {
                            dialog.noShop.setVisibility(View.VISIBLE);
                            // 加载动画
                            Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.push_bottom_in);
                            // 动画开始
                            dialog.cardShopLayout.setVisibility(View.VISIBLE);
                            dialog.cardShopLayout.startAnimation(animation);
                            dialog.bgLayout.setVisibility(View.VISIBLE);
                        }
                    }
                }
            });
            dialog.show(manager, "dish");
        }
    }

    @Override
    public void add(String id, String type , boolean isGuige) {
        setJia(id, isGuige, type, -1);
    }

    @Override
    public void sub(String id, boolean isGuige) {
        setJian(id, isGuige,  -1);
    }

    @Override
    public void onGuiGeSelected(String id, int select, int preSelet) {
        setGuige(id, select, preSelet);
    }

    @Override
    public void onPeiCaiSelected(String id, int pmenu, int pdish, boolean isSelect) {
        setPeicai(id, pmenu, pdish, isSelect);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ORDER_REQUEST && resultCode == ORDER_RESULT) {
            //下单成功
            Log.i("xiadanchenggong", "onActivityResult: " + "下单成功1");
            setClear();
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }
}