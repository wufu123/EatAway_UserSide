package com.australia.administrator.australiandelivery.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.australia.administrator.australiandelivery.R;
import com.australia.administrator.australiandelivery.bean.CaidanBean;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by local123 on 2017/11/27.
 */

public class PeiCaiAdapter extends RecyclerView.Adapter<PeiCaiAdapter.ViewHolder> {
    private Context context;
    private List<CaidanBean> cData;
    private List<CaidanBean.PeicaiBean> mData;
    private onSelectedListener listener;

    public onSelectedListener getListener() {
        return listener;
    }

    public void setListener(onSelectedListener listener) {
        this.listener = listener;
    }

    public PeiCaiAdapter(Context context, List<CaidanBean.PeicaiBean> data, List<CaidanBean> cdata) {
        this.context = context;
        this.mData = data;
        this.cData = cdata;
    }

    public void setData(List<CaidanBean.PeicaiBean> data) {
        mData = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recyclerview_peicai, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (position == 0) {
            holder.tvPeicaiCaidan.setVisibility(View.VISIBLE);
            holder.tvPeicaiCaidan.setText(cData.get(mData.get(position).getId()).getPcaidan());
        } else if (mData.get(position - 1).getId() == mData.get(position).getId()) {
            holder.tvPeicaiCaidan.setVisibility(View.GONE);
        } else {
            holder.tvPeicaiCaidan.setText(cData.get(mData.get(position).getId()).getPcaidan());
        }
        holder.tvPeicai.setText(mData.get(position).getPcname());
        holder.tvPeicaiPrice.setText("$" + mData.get(position).getPcprice());
        if (mData.get(position).isselect()) {
            holder.cbPeicai.setChecked(true);
        } else {
            holder.cbPeicai.setChecked(false);
        }
        holder.llPeicai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    if (holder.cbPeicai.isChecked()) {
                        holder.cbPeicai.setChecked(false);
                    } else {
                        holder.cbPeicai.setChecked(true);
                    }
                    listener.onPeiCaiSelected(mData.get(position).getId(), mData.get(position).getPosition(), holder.cbPeicai.isChecked());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mData != null) {
            return mData.size();
        }
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return mData.get(position).hashCode();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_peicai_price)
        TextView tvPeicaiPrice;
        @Bind(R.id.cb_peicai)
        CheckBox cbPeicai;
        @Bind(R.id.tv_peicai)
        TextView tvPeicai;
        @Bind(R.id.ll_peicai)
        LinearLayout llPeicai;
        @Bind(R.id.tv_peicai_caidan)
        TextView tvPeicaiCaidan;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface onSelectedListener {
        void onPeiCaiSelected(int pmenu, int pname, boolean isSelect);
    }
}
