package com.australia.administrator.australiandelivery.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Administrator on 2017/7/5.
 */

public class ShopDetailsBean {
    /**
     * status : 1
     * msg : {"maxmoney":"1","shopname":"四川辣子
     鸡","shopphoto":"http://wm.sawfree.com/uploads/05eab6d8954a842c2c611e4cf2e0dd7a.jpg","mobile":"15230868184","lmoney":"2","c
     ontent":"","shophead":"http://wm.sawfree.com/uploads/2d37b2e92b5ca8f9d11df90688c98e77.jpg","state":"1","detailed_address":"
     阿德莱德大学","coordinate":"138.6062277,-34.92060300000001","gotime":"
     ","long":"100","maxprice":"2","maxlong":"1","lkmoney":"1","juli":14228.48,"userspingjia":
     [{"username":"EA_15230868185","head_photo":"http://wm.sawfree.com/uploads/469cc198a02f2b7d43440a16fac88c3e.jpg","eid":"52",
     "pingjia":"4","content":"嗯嗯不
     错","photo1":"http://wm.sawfree.com/uploads/e652ae172ba53a3097089dbdee9c9f5a.jpg","photo2":"","state":"1","addtime":"2017-
     10-12 12:15:37","backpingjia":""}],"shopmessage":[{"cid":"364","cname":"呃呃
     呃","num":"0","shopid":"45","up":"370","end":"376","addtime":"2017-11-07 14:24:11","goods":[]},{"cid":"366","cname":"阿卡丽
     ","num":"0","shopid":"45","up":"375","end":"367","addtime":"2017-11-07 14:24:35","goods":[{"goodsid":"1979","goodsname":"阿
     什顿
     发","goodsphoto":"http://wm.sawfree.com/uploads/f9c8f85f5757ea276e7931c1f387a98a.jpg","goodsprice":"12.00","goodscontent":"
     安抚","cid":"366","shopid":"45","flag":"1","zhekou":null,"statts":null,"starttime":null,"stoptime":null,"addtime":"2017-11
     -08 10:22:20","wzhekou":"0","gzhekou":null,"goodsstate":3,"guige":[{"gid":"4","goodsid":"1979","guige":"刚师
     傅","primoney":"11","addtime":"0000-00-00 00:00:00"}],"caidan":[{"pid":"4","goodsid":"1979","pcaidan":"而案
     发","addtime":"0000-00-00 00:00:00","peicai":[]}]},{"goodsid":"1986","goodsname":"好运姐 厄运小
     姐","goodsphoto":"http://wm.sawfree.com/uploads/9c95ffa9d28e58641f5d428a6e9fea09.jpg","goodsprice":"188.00","goodscontent":
     "lolo","cid":"366","shopid":"45","flag":"1","zhekou":"0","statts":null,"starttime":null,"stoptime":null,"addtime":"2017-11
     -08 13:58:02","wzhekou":"0","gzhekou":"0","goodsstate":3,"guige":[{"gid":"7","goodsid":"1986","guige":"泳池派
     对","primoney":"1800","addtime":"2017-11-08 13:58:02"},{"gid":"8","goodsid":"1986","guige":"s6赛季专
     属","primoney":"200","addtime":"2017-11-08 13:58:02"},{"gid":"9","goodsid":"1986","guige":"西部牛
     仔","primoney":"98","addtime":"2017-11-08 13:58:02"},{"gid":"12","goodsid":"1986","guige":"电玩女
     神","primoney":"188","addtime":"0000-00-00 00:00:00"}],"caidan":[{"pid":"6","goodsid":"1986","pcaidan":"红
     叉","addtime":"2017-11-08 13:58:02","peicai":[{"pcid":"1","pcname":"sad","pcprice":"0.99","fid":"6","addtime":"2017-11-09
     16:22:44"},{"pcid":"2","pcname":"提3阿发","pcprice":"0.99","fid":"6","addtime":"2017-11-09 16:23:12"},
     {"pcid":"3","pcname":"撒发达","pcprice":"200.00","fid":"6","addtime":"2017-11-09 16:23:35"},{"pcid":"4","pcname":"什
     么","pcprice":"200.00","fid":"6","addtime":"0000-00-00 00:00:00"},
     {"pcid":"5","pcname":"asdfas","pcprice":"12.00","fid":"6","addtime":"0000-00-00 00:00:00"}]},
     {"pid":"7","goodsid":"1986","pcaidan":"电刀","addtime":"2017-11-08 13:58:02","peicai":[]},
     {"pid":"8","goodsid":"1986","pcaidan":"无尽之力","addtime":"2017-11-08 13:58:02","peicai":[]},
     {"pid":"11","goodsid":"1986","pcaidan":"饮血剑","addtime":"2017-11-09 14:57:29","peicai":[]},
     {"pid":"12","goodsid":"1986","pcaidan":"破败之刃","addtime":"2017-11-09 15:00:00","peicai":
     [{"pcid":"9","pcname":"aaa","pcprice":"0.00","fid":"12","addtime":"0000-00-00 00:00:00"}]}]},
     {"goodsid":"1988","goodsname":"啊士大夫萨
     芬","goodsphoto":"http://wm.sawfree.com/uploads/dec6059ef89b1ae216e965e20dd050b6.jpg","goodsprice":"1.00","goodscontent":"
     修改测
     试","cid":"366","shopid":"45","flag":"1","zhekou":null,"statts":null,"starttime":null,"stoptime":null,"addtime":"2017-11-22
     14:52:47","wzhekou":"0","gzhekou":null,"goodsstate":3,"guige":
     [{"gid":"13","goodsid":"1988","guige":"ceshi1","primoney":"10","addtime":"2017-11-22 14:52:47"},
     {"gid":"14","goodsid":"1988","guige":"ceshi3","primoney":"3","addtime":"2017-11-22 14:52:47"}],"caidan":
     [{"pid":"15","goodsid":"1988","pcaidan":"配菜1","addtime":"2017-11-22 14:52:47","peicai":[]},
     {"pid":"16","goodsid":"1988","pcaidan":"配菜2","addtime":"2017-11-22 14:52:47","peicai":[]}]},
     {"goodsid":"1989","goodsname":"551","goodsphoto":"http://wm.sawfree.com/false","goodsprice":"12.00","goodscontent":"","cid"
     :"366","shopid":"45","flag":"1","zhekou":"10","statts":"1","starttime":null,"stoptime":null,"addtime":"2017-11-22
     15:06:19","wzhekou":"0","gzhekou":"10","goodsstate":3,"guige":[],"caidan":[]},
     {"goodsid":"1990","goodsname":"","goodsphoto":"http://wm.sawfree.com/false","goodsprice":"0.00","goodscontent":"","cid":"36
     6","shopid":"45","flag":"1","zhekou":"10","statts":"1","starttime":null,"stoptime":null,"addtime":"2017-11-22
     15:07:49","wzhekou":"0","gzhekou":"10","goodsstate":3,"guige":[],"caidan":[]},{"goodsid":"1991","goodsname":"测试
     1","goodsphoto":"http://wm.sawfree.com/uploads/2e19f21504cbb58b7f30728e21581cc2.jpg","goodsprice":"100.00","goodscontent":"
     阿什顿发
     的","cid":"366","shopid":"45","flag":"1","zhekou":"10","statts":"1","starttime":null,"stoptime":null,"addtime":"2017-11-22
     15:42:22","wzhekou":"0","gzhekou":"10","goodsstate":3,"guige":
     [{"gid":"15","goodsid":"1991","guige":"大","primoney":"10","addtime":"2017-11-22 15:42:22"}],"caidan":
     [{"pid":"17","goodsid":"1991","pcaidan":"酱汁","addtime":"2017-11-22 15:42:22","peicai":[]}]},
     {"goodsid":"1992","goodsname":"阿什顿
     发","goodsphoto":"http://wm.sawfree.com/uploads/2428d240715abee3ec77c7bb79f01be5.jpg","goodsprice":"1231.00","goodscontent"
     :"阿什顿发","cid":"366","shopid":"45","flag":"1","zhekou":"2131243123","statts":"1","starttime":"2017-11-13
     00:00:00","stoptime":"2017-12-06 00:00:00","addtime":"2017-11-22
     16:09:09","wzhekou":"0","gzhekou":"2131243123","goodsstate":2,"guige":[],"caidan":[]}]},{"cid":"367","cname":"新菜
     单","num":"0","shopid":"45","up":"366","end":"373","addtime":"2017-11-07 17:05:12","goods":[{"goodsid":"1978","goodsname":"
     黄
     瓜","goodsphoto":"http://wm.sawfree.com/uploads/29cebc3e2e199485ad76b5c51e0d8058.jpg","goodsprice":"100.00","goodscontent":
     "新菜品","cid":"367","shopid":"45","flag":"1","zhekou":"10","statts":null,"starttime":null,"stoptime":null,"addtime":"2017
     -11-08 10:20:24","wzhekou":"0","gzhekou":"10","goodsstate":3,"guige":
     [{"gid":"1","goodsid":"1978","guige":"长","primoney":"120","addtime":"0000-00-00 00:00:00"},
     {"gid":"2","goodsid":"1978","guige":"粗","primoney":"130","addtime":"0000-00-00 00:00:00"},
     {"gid":"3","goodsid":"1978","guige":"又长又粗","primoney":"200","addtime":"0000-00-00 00:00:00"}],"caidan":
     [{"pid":"1","goodsid":"1978","pcaidan":"酱汁","addtime":"0000-00-00 00:00:00","peicai":[]},
     {"pid":"2","goodsid":"1978","pcaidan":"辣椒","addtime":"0000-00-00 00:00:00","peicai":[]},
     {"pid":"3","goodsid":"1978","pcaidan":"糖","addtime":"0000-00-00 00:00:00","peicai":[]}]},{"goodsid":"1981","goodsname":"阿
     什顿发","goodsphoto":"http://wm.sawfree.com/false","goodsprice":"12.00","goodscontent":"阿什顿
     发","cid":"367","shopid":"45","flag":"1","zhekou":null,"statts":null,"starttime":null,"stoptime":null,"addtime":"2017-11-08
     10:43:45","wzhekou":"0","gzhekou":null,"goodsstate":3,"guige":[],"caidan":[]},{"goodsid":"1982","goodsname":"阿什顿
     发","goodsphoto":"http://wm.sawfree.com/uploads/c48325698b03df71a7f326d8e1b0841c.jpg","goodsprice":"12.00","goodscontent":"
     阿什顿
     发","cid":"367","shopid":"45","flag":"1","zhekou":null,"statts":null,"starttime":null,"stoptime":null,"addtime":"2017-11-08
     10:44:14","wzhekou":"0","gzhekou":null,"goodsstate":3,"guige":[{"gid":"6","goodsid":"1982","guige":"发到
     付","primoney":"12","addtime":"0000-00-00 00:00:00"}],"caidan":[]}]},{"cid":"368","cname":"新
     的","num":"0","shopid":"45","up":"373","end":"374","addtime":"2017-11-07 17:33:52","goods":[]},{"cid":"370","cname":"佛
     佛","num":"0","shopid":"45","up":"-1","end":"364","addtime":"2017-11-07 17:37:18","goods":[{"goodsid":"1985","goodsname":"
     沙发","goodsphoto":"http://wm.sawfree.com/false","goodsprice":"12.00","goodscontent":"阿什顿
     发","cid":"370","shopid":"45","flag":"1","zhekou":"12","statts":null,"starttime":null,"stoptime":null,"addtime":"2017-11-08
     12:02:34","wzhekou":"0","gzhekou":"12","goodsstate":3,"guige":[],"caidan":[]}]},{"cid":"373","cname":"啊啊
     啊","num":"0","shopid":"45","up":"367","end":"368","addtime":"2017-11-09 18:00:33","goods":[{"goodsid":"1987","goodsname":"
     阿什顿
     发","goodsphoto":"http://wm.sawfree.com/uploads/1d9a967d1892ecb7ea3a57f820a4f877.jpg","goodsprice":"12.00","goodscontent":"
     俺的沙发","cid":"373","shopid":"45","flag":"1","zhekou":"0","statts":null,"starttime":null,"stoptime":null,"addtime":"2017
     -11-09 18:01:01","wzhekou":"0","gzhekou":"0","goodsstate":3,"guige":[],"caidan":[{"pid":"14","goodsid":"1987","pcaidan":"阿
     什顿发撒地方","addtime":"2017-11-09 18:01:59","peicai":[{"pcid":"11","pcname":"阿什顿
     发","pcprice":"11111.00","fid":"14","addtime":"2017-11-09 18:03:24"}]}]}]},{"cid":"374","cname":"得
     得","num":"0","shopid":"45","up":"368","end":"0","addtime":"2017-11-10 19:04:45","goods":[]},{"cid":"375","cname":"呃呃呃呃
     呃呃呃","num":"0","shopid":"45","up":"376","end":"366","addtime":"2017-11-10 19:07:44","goods":[]},{"cid":"376","cname":"哦
     银河","num":"0","shopid":"45","up":"364","end":"375","addtime":"2017-11-10 19:09:55","goods":[]}]}
     */

    private int status;
    private MsgBean msg;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public MsgBean getMsg() {
        return msg;
    }

    public void setMsg(MsgBean msg) {
        this.msg = msg;
    }

    public static class MsgBean {
        /**
         * maxmoney : 1
         * shopname : 四川辣子鸡
         * shopphoto : http://wm.sawfree.com/uploads/05eab6d8954a842c2c611e4cf2e0dd7a.jpg
         * mobile : 15230868184
         * lmoney : 2
         * content :
         * shophead : http://wm.sawfree.com/uploads/2d37b2e92b5ca8f9d11df90688c98e77.jpg
         * state : 1
         * detailed_address : 阿德莱德大学
         * coordinate : 138.6062277,-34.92060300000001
         * gotime :
         * long : 100
         * maxprice : 2
         * maxlong : 1
         * lkmoney : 1
         * juli : 14228.48
         * userspingjia :
         [{"username":"EA_15230868185","head_photo":"http://wm.sawfree.com/uploads/469cc198a02f2b7d43440a16fac88c3e.jpg","eid":"52",
         "pingjia":"4","content":"嗯嗯不
         错","photo1":"http://wm.sawfree.com/uploads/e652ae172ba53a3097089dbdee9c9f5a.jpg","photo2":"","state":"1","addtime":"2017-
         10-12 12:15:37","backpingjia":""}]
         * shopmessage : [{"cid":"364","cname":"呃呃呃","num":"0","shopid":"45","up":"370","end":"376","addtime":"2017-11-
         07 14:24:11","goods":[]},{"cid":"366","cname":"阿卡丽","num":"0","shopid":"45","up":"375","end":"367","addtime":"2017-11-07
         14:24:35","goods":[{"goodsid":"1979","goodsname":"阿什顿
         发","goodsphoto":"http://wm.sawfree.com/uploads/f9c8f85f5757ea276e7931c1f387a98a.jpg","goodsprice":"12.00","goodscontent":"
         安抚","cid":"366","shopid":"45","flag":"1","zhekou":null,"statts":null,"starttime":null,"stoptime":null,"addtime":"2017-11
         -08 10:22:20","wzhekou":"0","gzhekou":null,"goodsstate":3,"guige":[{"gid":"4","goodsid":"1979","guige":"刚师
         傅","primoney":"11","addtime":"0000-00-00 00:00:00"}],"caidan":[{"pid":"4","goodsid":"1979","pcaidan":"而案
         发","addtime":"0000-00-00 00:00:00","peicai":[]}]},{"goodsid":"1986","goodsname":"好运姐 厄运小
         姐","goodsphoto":"http://wm.sawfree.com/uploads/9c95ffa9d28e58641f5d428a6e9fea09.jpg","goodsprice":"188.00","goodscontent":
         "lolo","cid":"366","shopid":"45","flag":"1","zhekou":"0","statts":null,"starttime":null,"stoptime":null,"addtime":"2017-11
         -08 13:58:02","wzhekou":"0","gzhekou":"0","goodsstate":3,"guige":[{"gid":"7","goodsid":"1986","guige":"泳池派
         对","primoney":"1800","addtime":"2017-11-08 13:58:02"},{"gid":"8","goodsid":"1986","guige":"s6赛季专
         属","primoney":"200","addtime":"2017-11-08 13:58:02"},{"gid":"9","goodsid":"1986","guige":"西部牛
         仔","primoney":"98","addtime":"2017-11-08 13:58:02"},{"gid":"12","goodsid":"1986","guige":"电玩女
         神","primoney":"188","addtime":"0000-00-00 00:00:00"}],"caidan":[{"pid":"6","goodsid":"1986","pcaidan":"红
         叉","addtime":"2017-11-08 13:58:02","peicai":[{"pcid":"1","pcname":"sad","pcprice":"0.99","fid":"6","addtime":"2017-11-09
         16:22:44"},{"pcid":"2","pcname":"提3阿发","pcprice":"0.99","fid":"6","addtime":"2017-11-09 16:23:12"},
         {"pcid":"3","pcname":"撒发达","pcprice":"200.00","fid":"6","addtime":"2017-11-09 16:23:35"},{"pcid":"4","pcname":"什
         么","pcprice":"200.00","fid":"6","addtime":"0000-00-00 00:00:00"},
         {"pcid":"5","pcname":"asdfas","pcprice":"12.00","fid":"6","addtime":"0000-00-00 00:00:00"}]},
         {"pid":"7","goodsid":"1986","pcaidan":"电刀","addtime":"2017-11-08 13:58:02","peicai":[]},
         {"pid":"8","goodsid":"1986","pcaidan":"无尽之力","addtime":"2017-11-08 13:58:02","peicai":[]},
         {"pid":"11","goodsid":"1986","pcaidan":"饮血剑","addtime":"2017-11-09 14:57:29","peicai":[]},
         {"pid":"12","goodsid":"1986","pcaidan":"破败之刃","addtime":"2017-11-09 15:00:00","peicai":
         [{"pcid":"9","pcname":"aaa","pcprice":"0.00","fid":"12","addtime":"0000-00-00 00:00:00"}]}]},
         {"goodsid":"1988","goodsname":"啊士大夫萨
         芬","goodsphoto":"http://wm.sawfree.com/uploads/dec6059ef89b1ae216e965e20dd050b6.jpg","goodsprice":"1.00","goodscontent":"
         修改测
         试","cid":"366","shopid":"45","flag":"1","zhekou":null,"statts":null,"starttime":null,"stoptime":null,"addtime":"2017-11-22
         14:52:47","wzhekou":"0","gzhekou":null,"goodsstate":3,"guige":
         [{"gid":"13","goodsid":"1988","guige":"ceshi1","primoney":"10","addtime":"2017-11-22 14:52:47"},
         {"gid":"14","goodsid":"1988","guige":"ceshi3","primoney":"3","addtime":"2017-11-22 14:52:47"}],"caidan":
         [{"pid":"15","goodsid":"1988","pcaidan":"配菜1","addtime":"2017-11-22 14:52:47","peicai":[]},
         {"pid":"16","goodsid":"1988","pcaidan":"配菜2","addtime":"2017-11-22 14:52:47","peicai":[]}]},
         {"goodsid":"1989","goodsname":"551","goodsphoto":"http://wm.sawfree.com/false","goodsprice":"12.00","goodscontent":"","cid"
         :"366","shopid":"45","flag":"1","zhekou":"10","statts":"1","starttime":null,"stoptime":null,"addtime":"2017-11-22
         15:06:19","wzhekou":"0","gzhekou":"10","goodsstate":3,"guige":[],"caidan":[]},
         {"goodsid":"1990","goodsname":"","goodsphoto":"http://wm.sawfree.com/false","goodsprice":"0.00","goodscontent":"","cid":"36
         6","shopid":"45","flag":"1","zhekou":"10","statts":"1","starttime":null,"stoptime":null,"addtime":"2017-11-22
         15:07:49","wzhekou":"0","gzhekou":"10","goodsstate":3,"guige":[],"caidan":[]},{"goodsid":"1991","goodsname":"测试
         1","goodsphoto":"http://wm.sawfree.com/uploads/2e19f21504cbb58b7f30728e21581cc2.jpg","goodsprice":"100.00","goodscontent":"
         阿什顿发
         的","cid":"366","shopid":"45","flag":"1","zhekou":"10","statts":"1","starttime":null,"stoptime":null,"addtime":"2017-11-22
         15:42:22","wzhekou":"0","gzhekou":"10","goodsstate":3,"guige":
         [{"gid":"15","goodsid":"1991","guige":"大","primoney":"10","addtime":"2017-11-22 15:42:22"}],"caidan":
         [{"pid":"17","goodsid":"1991","pcaidan":"酱汁","addtime":"2017-11-22 15:42:22","peicai":[]}]},
         {"goodsid":"1992","goodsname":"阿什顿
         发","goodsphoto":"http://wm.sawfree.com/uploads/2428d240715abee3ec77c7bb79f01be5.jpg","goodsprice":"1231.00","goodscontent"
         :"阿什顿发","cid":"366","shopid":"45","flag":"1","zhekou":"2131243123","statts":"1","starttime":"2017-11-13
         00:00:00","stoptime":"2017-12-06 00:00:00","addtime":"2017-11-22
         16:09:09","wzhekou":"0","gzhekou":"2131243123","goodsstate":2,"guige":[],"caidan":[]}]},{"cid":"367","cname":"新菜
         单","num":"0","shopid":"45","up":"366","end":"373","addtime":"2017-11-07 17:05:12","goods":[{"goodsid":"1978","goodsname":"
         黄
         瓜","goodsphoto":"http://wm.sawfree.com/uploads/29cebc3e2e199485ad76b5c51e0d8058.jpg","goodsprice":"100.00","goodscontent":
         "新菜品","cid":"367","shopid":"45","flag":"1","zhekou":"10","statts":null,"starttime":null,"stoptime":null,"addtime":"2017
         -11-08 10:20:24","wzhekou":"0","gzhekou":"10","goodsstate":3,"guige":
         [{"gid":"1","goodsid":"1978","guige":"长","primoney":"120","addtime":"0000-00-00 00:00:00"},
         {"gid":"2","goodsid":"1978","guige":"粗","primoney":"130","addtime":"0000-00-00 00:00:00"},
         {"gid":"3","goodsid":"1978","guige":"又长又粗","primoney":"200","addtime":"0000-00-00 00:00:00"}],"caidan":
         [{"pid":"1","goodsid":"1978","pcaidan":"酱汁","addtime":"0000-00-00 00:00:00","peicai":[]},
         {"pid":"2","goodsid":"1978","pcaidan":"辣椒","addtime":"0000-00-00 00:00:00","peicai":[]},
         {"pid":"3","goodsid":"1978","pcaidan":"糖","addtime":"0000-00-00 00:00:00","peicai":[]}]},{"goodsid":"1981","goodsname":"阿
         什顿发","goodsphoto":"http://wm.sawfree.com/false","goodsprice":"12.00","goodscontent":"阿什顿
         发","cid":"367","shopid":"45","flag":"1","zhekou":null,"statts":null,"starttime":null,"stoptime":null,"addtime":"2017-11-08
         10:43:45","wzhekou":"0","gzhekou":null,"goodsstate":3,"guige":[],"caidan":[]},{"goodsid":"1982","goodsname":"阿什顿
         发","goodsphoto":"http://wm.sawfree.com/uploads/c48325698b03df71a7f326d8e1b0841c.jpg","goodsprice":"12.00","goodscontent":"
         阿什顿
         发","cid":"367","shopid":"45","flag":"1","zhekou":null,"statts":null,"starttime":null,"stoptime":null,"addtime":"2017-11-08
         10:44:14","wzhekou":"0","gzhekou":null,"goodsstate":3,"guige":[{"gid":"6","goodsid":"1982","guige":"发到
         付","primoney":"12","addtime":"0000-00-00 00:00:00"}],"caidan":[]}]},{"cid":"368","cname":"新
         的","num":"0","shopid":"45","up":"373","end":"374","addtime":"2017-11-07 17:33:52","goods":[]},{"cid":"370","cname":"佛
         佛","num":"0","shopid":"45","up":"-1","end":"364","addtime":"2017-11-07 17:37:18","goods":[{"goodsid":"1985","goodsname":"
         沙发","goodsphoto":"http://wm.sawfree.com/false","goodsprice":"12.00","goodscontent":"阿什顿
         发","cid":"370","shopid":"45","flag":"1","zhekou":"12","statts":null,"starttime":null,"stoptime":null,"addtime":"2017-11-08
         12:02:34","wzhekou":"0","gzhekou":"12","goodsstate":3,"guige":[],"caidan":[]}]},{"cid":"373","cname":"啊啊
         啊","num":"0","shopid":"45","up":"367","end":"368","addtime":"2017-11-09 18:00:33","goods":[{"goodsid":"1987","goodsname":"
         阿什顿
         发","goodsphoto":"http://wm.sawfree.com/uploads/1d9a967d1892ecb7ea3a57f820a4f877.jpg","goodsprice":"12.00","goodscontent":"
         俺的沙发","cid":"373","shopid":"45","flag":"1","zhekou":"0","statts":null,"starttime":null,"stoptime":null,"addtime":"2017
         -11-09 18:01:01","wzhekou":"0","gzhekou":"0","goodsstate":3,"guige":[],"caidan":[{"pid":"14","goodsid":"1987","pcaidan":"阿
         什顿发撒地方","addtime":"2017-11-09 18:01:59","peicai":[{"pcid":"11","pcname":"阿什顿
         发","pcprice":"11111.00","fid":"14","addtime":"2017-11-09 18:03:24"}]}]}]},{"cid":"374","cname":"得
         得","num":"0","shopid":"45","up":"368","end":"0","addtime":"2017-11-10 19:04:45","goods":[]},{"cid":"375","cname":"呃呃呃呃
         呃呃呃","num":"0","shopid":"45","up":"376","end":"366","addtime":"2017-11-10 19:07:44","goods":[]},{"cid":"376","cname":"哦
         银河","num":"0","shopid":"45","up":"364","end":"375","addtime":"2017-11-10 19:09:55","goods":[]}]
         */

        private int flag;
        private String maxmoney;
        private String shopname;
        private String shopphoto;
        private String mobile;
        private String lmoney;
        private String content;
        private String shophead;
        private String state;
        private String detailed_address;
        private String coordinate;
        private String gotime;
        @SerializedName("long")
        private String longX;
        private String maxprice;
        private String maxlong;
        private String lkmoney;
        private double juli;
        private String peisongtime;
        private List<UserspingjiaBean> userspingjia;
        private List<ShopmessageBean> shopmessage;

        public String getPeisongtime() {
            return peisongtime;
        }

        public void setPeisongtime(String peisongtime) {
            this.peisongtime = peisongtime;
        }

        public int getFlag() {
            return flag;
        }

        public void setFlag(int flag) {
            this.flag = flag;
        }

        public String getMaxmoney() {
            return maxmoney;
        }

        public void setMaxmoney(String maxmoney) {
            this.maxmoney = maxmoney;
        }

        public String getShopname() {
            return shopname;
        }

        public void setShopname(String shopname) {
            this.shopname = shopname;
        }

        public String getShopphoto() {
            return shopphoto;
        }

        public void setShopphoto(String shopphoto) {
            this.shopphoto = shopphoto;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getLmoney() {
            return lmoney;
        }

        public void setLmoney(String lmoney) {
            this.lmoney = lmoney;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getShophead() {
            return shophead;
        }

        public void setShophead(String shophead) {
            this.shophead = shophead;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getDetailed_address() {
            return detailed_address;
        }

        public void setDetailed_address(String detailed_address) {
            this.detailed_address = detailed_address;
        }

        public String getCoordinate() {
            return coordinate;
        }

        public void setCoordinate(String coordinate) {
            this.coordinate = coordinate;
        }

        public String getGotime() {
            return gotime;
        }

        public void setGotime(String gotime) {
            this.gotime = gotime;
        }

        public String getLongX() {
            return longX;
        }

        public void setLongX(String longX) {
            this.longX = longX;
        }

        public String getMaxprice() {
            return maxprice;
        }

        public void setMaxprice(String maxprice) {
            this.maxprice = maxprice;
        }

        public String getMaxlong() {
            return maxlong;
        }

        public void setMaxlong(String maxlong) {
            this.maxlong = maxlong;
        }

        public String getLkmoney() {
            return lkmoney;
        }

        public void setLkmoney(String lkmoney) {
            this.lkmoney = lkmoney;
        }

        public double getJuli() {
            return juli;
        }

        public void setJuli(double juli) {
            this.juli = juli;
        }

        public List<UserspingjiaBean> getUserspingjia() {
            return userspingjia;
        }

        public void setUserspingjia(List<UserspingjiaBean> userspingjia) {
            this.userspingjia = userspingjia;
        }

        public List<ShopmessageBean> getShopmessage() {
            return shopmessage;
        }

        public void setShopmessage(List<ShopmessageBean> shopmessage) {
            this.shopmessage = shopmessage;
        }

        public static class UserspingjiaBean {
            /**
             * username : EA_15230868185
             * head_photo : http://wm.sawfree.com/uploads/469cc198a02f2b7d43440a16fac88c3e.jpg
             * eid : 52
             * pingjia : 4
             * content : 嗯嗯不错
             * photo1 : http://wm.sawfree.com/uploads/e652ae172ba53a3097089dbdee9c9f5a.jpg
             * photo2 :
             * state : 1
             * addtime : 2017-10-12 12:15:37
             * backpingjia :
             */

            private String username;
            private String head_photo;
            private String eid;
            private String pingjia;
            private String content;
            private String photo1;
            private String photo2;
            private String state;
            private String addtime;
            private String backpingjia;

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getHead_photo() {
                return head_photo;
            }

            public void setHead_photo(String head_photo) {
                this.head_photo = head_photo;
            }

            public String getEid() {
                return eid;
            }

            public void setEid(String eid) {
                this.eid = eid;
            }

            public String getPingjia() {
                return pingjia;
            }

            public void setPingjia(String pingjia) {
                this.pingjia = pingjia;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getPhoto1() {
                return photo1;
            }

            public void setPhoto1(String photo1) {
                this.photo1 = photo1;
            }

            public String getPhoto2() {
                return photo2;
            }

            public void setPhoto2(String photo2) {
                this.photo2 = photo2;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getAddtime() {
                return addtime;
            }

            public void setAddtime(String addtime) {
                this.addtime = addtime;
            }

            public String getBackpingjia() {
                return backpingjia;
            }

            public void setBackpingjia(String backpingjia) {
                this.backpingjia = backpingjia;
            }
        }

        public static class ShopmessageBean {
            /**
             * cid : 364
             * cname : 呃呃呃
             * num : 0
             * shopid : 45
             * up : 370
             * end : 376
             * addtime : 2017-11-07 14:24:11
             * goods : []
             */

            private String cid;
            private String cname;
            private String num;
            private String shopid;
            private String up;
            private String end;
            private String addtime;
            private int bugNum;
            private List<GoodsBean> goods;

            public int getBugNum() {
                return bugNum;
            }

            public void setBugNum(int bugNum) {
                this.bugNum = bugNum;
            }

            public String getCid() {
                return cid;
            }

            public void setCid(String cid) {
                this.cid = cid;
            }

            public String getCname() {
                return cname;
            }

            public void setCname(String cname) {
                this.cname = cname;
            }

            public String getNum() {
                return num;
            }

            public void setNum(String num) {
                this.num = num;
            }

            public String getShopid() {
                return shopid;
            }

            public void setShopid(String shopid) {
                this.shopid = shopid;
            }

            public String getUp() {
                return up;
            }

            public void setUp(String up) {
                this.up = up;
            }

            public String getEnd() {
                return end;
            }

            public void setEnd(String end) {
                this.end = end;
            }

            public String getAddtime() {
                return addtime;
            }

            public void setAddtime(String addtime) {
                this.addtime = addtime;
            }

            public List<GoodsBean> getGoods() {
                return goods;
            }

            public void setGoods(List<GoodsBean> goods) {
                this.goods = goods;
            }
        }
    }
}
