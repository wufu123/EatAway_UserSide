package com.australia.administrator.australiandelivery.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.australia.administrator.australiandelivery.MyApplication;
import com.australia.administrator.australiandelivery.R;
import com.australia.administrator.australiandelivery.adapter.AddressListAdapter;
import com.australia.administrator.australiandelivery.bean.AddressBean1;
import com.australia.administrator.australiandelivery.comm.*;
import com.australia.administrator.australiandelivery.utils.Contants;
import com.australia.administrator.australiandelivery.utils.HttpUtils;
import com.australia.administrator.australiandelivery.utils.ToastUtils;
import com.australia.administrator.australiandelivery.view.TopBar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuRecyclerView;

import butterknife.Bind;
import okhttp3.Call;

public class SelectAddressActivity extends com.australia.administrator.australiandelivery.comm.BaseActivity
        implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
    private final int SELECT_ADDRESS_RESULT = 10087;
    @Bind(R.id.tb_activity_address_select)
    TopBar tbActivityAddressSelect;
    @Bind(R.id.tv_new_address)
    TextView tvNewAddress;
    @Bind(R.id.rv_activity_address_select)
    SwipeMenuRecyclerView rvActivityAddressSelect;
    @Bind(R.id.tv_current_position)
    TextView tvCurrentPosition;
    @Bind(R.id.tv_retry)
    TextView tvRetry;
    @Bind(R.id.ll_current_position)
    LinearLayout llCurrentPosition;
    @Bind(R.id.tv_error)
    TextView tvError;
    private GoogleApiClient mGoogleApiClient;
    private boolean mLocationPermissionGranted;
    private Location mLastKnownLocation;
    private Login login = MyApplication.getLogin();
    private AddressListAdapter adapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_select_address;
    }

    @Override
    protected void initDate() {
        getDeviceLocation();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */,
                        this /* OnConnectionFailedListener */)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tbActivityAddressSelect.setTbLeftIv(R.drawable.img_icon_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tbActivityAddressSelect.setTbCenterTv(R.string.delivery_address, R.color.color_white);
        tvCurrentPosition.setText(R.string.text_loading);
        tvRetry.setVisibility(View.GONE);
        tvNewAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SelectAddressActivity.this,AddressReleaseActiviyty.class));
            }
        });
        tvRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvRetry.setVisibility(View.GONE);
                tvCurrentPosition.setText(R.string.text_loading);
                mGoogleApiClient.reconnect();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        showDialog();
        getData();
    }

    private void getData() {
        HttpUtils httpUtils = new HttpUtils(Contants.URL_GETADDRESS) {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.i("address_my", "onError: "+e.getMessage());
                hidDialog();
                tvError.setVisibility(View.VISIBLE);
                tvError.setText(R.string.load_fail);
                tvError.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tvError.setVisibility(View.GONE);
                        showDialog();
                        getData();
                    }
                });
                Toast.makeText(SelectAddressActivity.this, R.string.please_check_your_network_connection, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(String response, int id) {
                Log.i("address_my", "onResponse: "+response);
                hidDialog();
                tvError.setVisibility(View.GONE);
                rvActivityAddressSelect.setLayoutManager(new LinearLayoutManager(SelectAddressActivity.this, LinearLayoutManager.VERTICAL, false));
                AddressBean1 bean = new Gson().fromJson(response, AddressBean1.class);
                adapter = new AddressListAdapter(SelectAddressActivity.this, bean, "4", SelectAddressActivity.this);
                adapter.setListener(new AddressListAdapter.onAddressSelected() {
                    @Override
                    public void selected(double la, double lo) {
                        MyApplication.mLocation = new LatLng(la, lo);
                        setResult(SELECT_ADDRESS_RESULT);
                        finish();
                    }
                });
                rvActivityAddressSelect.setAdapter(adapter);
            }
        };
        if (login != null) {
            httpUtils.addParam("userid", login.getUserId()).addParams("token", login.getToken());
            httpUtils.clicent();
        } else {
            ToastUtils.showToast(R.string.Please_Log_on_again, SelectAddressActivity.this);
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        PendingResult<PlaceLikelihoodBuffer> result = Places.PlaceDetectionApi
                .getCurrentPlace(mGoogleApiClient, null);
        result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
            @Override
            public void onResult(final PlaceLikelihoodBuffer likelyPlaces) {
//                for (PlaceLikelihood placeLikelihood : likelyPlaces) {
//                    Log.i("======", String.format("Place '%s' has likelihood: %g",
//                            placeLikelihood.getPlace().getName(),
//                            placeLikelihood.getLikelihood()));
//                }
                Log.i("=========", "onConnected: ");
                if (likelyPlaces.getCount() > 1) {
                    final LatLng lalo = likelyPlaces.get(0).getPlace().getLatLng();
                    tvRetry.setVisibility(View.VISIBLE);
                    tvCurrentPosition.setText(likelyPlaces.get(0).getPlace().getAddress());
                    tvCurrentPosition.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mGoogleApiClient.isConnected()) {
                                MyApplication.mLocation = lalo;
                                setResult(SELECT_ADDRESS_RESULT);
                                finish();
                            }
                        }
                    });
                }
                likelyPlaces.release();
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i("=========", "onConnectionSuspended: " + i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        tvRetry.setVisibility(View.VISIBLE);
        tvCurrentPosition.setText(R.string.ding_wei_shi_bai);
    }

    private void getDeviceLocation() {
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.RECORD_AUDIO},
                    1000);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1000: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                    getlat();
                } else {
                    ToastUtils.showToast(R.string.huo_qu_lu_yin_quan_xian, this);
                }
            }
        }
    }

    private void getlat() {
        mLastKnownLocation = getmLastKnownLocation();
        if (mLastKnownLocation != null) {
            MyApplication.mLocation = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
        }

    }

    private Location getmLastKnownLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }
        mLastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastKnownLocation == null) {
            getmLastKnownLocation();
        }
        return mLastKnownLocation;
    }
}
