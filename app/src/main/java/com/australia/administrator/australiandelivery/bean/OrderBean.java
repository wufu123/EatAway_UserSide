package com.australia.administrator.australiandelivery.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/7/10.
 */

public class OrderBean {

    /**
     * status : 1
     * msg : {"shopid":"45","shopname":"四川辣子鸡","shophead":"http://13.210.182.98/uploads/2d37b2e92b5ca8f9d11df90688c98e77.jpg","shopphoto":"http://13.210.182.98/uploads/05eab6d8954a842c2c611e4cf2e0dd7a.jpg","phone":"15230868185","shopphone":"15230868184","cometime":"ASAP","statu":"2","money":"0.00","juli":"0.78227","allprice":"9004.00","orderid":"152308681851512541276","state":"1","address":"2018 万达广场3号公寓","beizhu":"","addtime":"2017-12-06 16:51:17","goods":[{"orid":"114","orderid":"152308681851512541276","goodsname":"好运姐 厄运小姐","goodsphoto":"http://13.210.182.98/uploads/43a0d77ef8602552a1b920bc48eca3d4.jpg","zprice":"96","guigename":"泳池派对","pcname":"哦了,aaa","num":"1"},{"orid":"115","orderid":"152308681851512541276","goodsname":"阿什顿发","goodsphoto":"http://13.210.182.98/uploads/1d9a967d1892ecb7ea3a57f820a4f877.jpg","zprice":"106","guigename":null,"pcname":"","num":"1"},{"orid":"116","orderid":"152308681851512541276","goodsname":"阿什顿发","goodsphoto":"http://13.210.182.98/uploads/1d9a967d1892ecb7ea3a57f820a4f877.jpg","zprice":"9004","guigename":null,"pcname":"阿什顿发","num":"1"}]}
     */

    private int status;
    private MsgBean msg;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public MsgBean getMsg() {
        return msg;
    }

    public void setMsg(MsgBean msg) {
        this.msg = msg;
    }

    public static class MsgBean {
        /**
         * shopid : 45
         * shopname : 四川辣子鸡
         * shophead : http://13.210.182.98/uploads/2d37b2e92b5ca8f9d11df90688c98e77.jpg
         * shopphoto : http://13.210.182.98/uploads/05eab6d8954a842c2c611e4cf2e0dd7a.jpg
         * phone : 15230868185
         * shopphone : 15230868184
         * cometime : ASAP
         * statu : 2
         * money : 0.00
         * juli : 0.78227
         * allprice : 9004.00
         * orderid : 152308681851512541276
         * state : 1
         * address : 2018 万达广场3号公寓
         * beizhu :
         * addtime : 2017-12-06 16:51:17
         * goods : [{"orid":"114","orderid":"152308681851512541276","goodsname":"好运姐 厄运小姐","goodsphoto":"http://13.210.182.98/uploads/43a0d77ef8602552a1b920bc48eca3d4.jpg","zprice":"96","guigename":"泳池派对","pcname":"哦了,aaa","num":"1"},{"orid":"115","orderid":"152308681851512541276","goodsname":"阿什顿发","goodsphoto":"http://13.210.182.98/uploads/1d9a967d1892ecb7ea3a57f820a4f877.jpg","zprice":"106","guigename":null,"pcname":"","num":"1"},{"orid":"116","orderid":"152308681851512541276","goodsname":"阿什顿发","goodsphoto":"http://13.210.182.98/uploads/1d9a967d1892ecb7ea3a57f820a4f877.jpg","zprice":"9004","guigename":null,"pcname":"阿什顿发","num":"1"}]
         */

        private String shopid;
        private String shopname;
        private String shophead;
        private String shopphoto;
        private String phone;
        private String shopphone;
        private String cometime;
        private String statu;
        private String money;
        private double juli;
        private String allprice;
        private String orderid;
        private String state;
        private String address;
        private String beizhu;
        private String addtime;
        private String youhuiall;
        private String ycodema;
        private List<GoodsBean> goods;
        private String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getYcodema() {
            return ycodema;
        }

        public void setYcodema(String ycodema) {
            this.ycodema = ycodema;
        }

        public String getYouhuiall() {
            return youhuiall;
        }

        public void setYouhuiall(String youhuiall) {
            this.youhuiall = youhuiall;
        }

        public String getShopid() {
            return shopid;
        }

        public void setShopid(String shopid) {
            this.shopid = shopid;
        }

        public String getShopname() {
            return shopname;
        }

        public void setShopname(String shopname) {
            this.shopname = shopname;
        }

        public String getShophead() {
            return shophead;
        }

        public void setShophead(String shophead) {
            this.shophead = shophead;
        }

        public String getShopphoto() {
            return shopphoto;
        }

        public void setShopphoto(String shopphoto) {
            this.shopphoto = shopphoto;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getShopphone() {
            return shopphone;
        }

        public void setShopphone(String shopphone) {
            this.shopphone = shopphone;
        }

        public String getCometime() {
            return cometime;
        }

        public void setCometime(String cometime) {
            this.cometime = cometime;
        }

        public String getStatu() {
            return statu;
        }

        public void setStatu(String statu) {
            this.statu = statu;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public double getJuli() {
            return juli;
        }

        public void setJuli(double juli) {
            this.juli = juli;
        }

        public String getAllprice() {
            return allprice;
        }

        public void setAllprice(String allprice) {
            this.allprice = allprice;
        }

        public String getOrderid() {
            return orderid;
        }

        public void setOrderid(String orderid) {
            this.orderid = orderid;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getBeizhu() {
            return beizhu;
        }

        public void setBeizhu(String beizhu) {
            this.beizhu = beizhu;
        }

        public String getAddtime() {
            return addtime;
        }

        public void setAddtime(String addtime) {
            this.addtime = addtime;
        }

        public List<GoodsBean> getGoods() {
            return goods;
        }

        public void setGoods(List<GoodsBean> goods) {
            this.goods = goods;
        }

        public static class GoodsBean {
            /**
             * orid : 114
             * orderid : 152308681851512541276
             * goodsname : 好运姐 厄运小姐
             * goodsphoto : http://13.210.182.98/uploads/43a0d77ef8602552a1b920bc48eca3d4.jpg
             * zprice : 96
             * guigename : 泳池派对
             * pcname : 哦了,aaa
             * num : 1
             */

            private String orid;
            private String orderid;
            private String goodsname;
            private String goodsphoto;
            private String zprice;
            private String guigename;
            private String pcname;
            private String num;

            public String getOrid() {
                return orid;
            }

            public void setOrid(String orid) {
                this.orid = orid;
            }

            public String getOrderid() {
                return orderid;
            }

            public void setOrderid(String orderid) {
                this.orderid = orderid;
            }

            public String getGoodsname() {
                return goodsname;
            }

            public void setGoodsname(String goodsname) {
                this.goodsname = goodsname;
            }

            public String getGoodsphoto() {
                return goodsphoto;
            }

            public void setGoodsphoto(String goodsphoto) {
                this.goodsphoto = goodsphoto;
            }

            public String getZprice() {
                return zprice;
            }

            public void setZprice(String zprice) {
                this.zprice = zprice;
            }

            public String getGuigename() {
                return guigename;
            }

            public void setGuigename(String guigename) {
                this.guigename = guigename;
            }

            public String getPcname() {
                return pcname;
            }

            public void setPcname(String pcname) {
                this.pcname = pcname;
            }

            public String getNum() {
                return num;
            }

            public void setNum(String num) {
                this.num = num;
            }
        }
    }
}
