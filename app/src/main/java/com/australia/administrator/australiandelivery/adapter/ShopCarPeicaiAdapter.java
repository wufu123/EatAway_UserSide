package com.australia.administrator.australiandelivery.adapter;

import android.content.Context;
import android.media.JetPlayer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.australia.administrator.australiandelivery.R;
import com.australia.administrator.australiandelivery.bean.CaidanBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by local123 on 2017/11/30.
 */

public class ShopCarPeicaiAdapter extends RecyclerView.Adapter<ShopCarPeicaiAdapter.ViewHolder> {
    private Context context;
    private List<CaidanBean.PeicaiBean> pData;

    public ShopCarPeicaiAdapter(Context context, List<CaidanBean.PeicaiBean> pData) {
        this.context = context;
        //改变为一维数组
//        if (mData != null) {
//            pData = new ArrayList<>();
//            for (CaidanBean bean : mData) {
//                for (CaidanBean.PeicaiBean peicaiBean : bean.getPeicai()) {
//                    if (peicaiBean.isselect()) {
//                        pData.add(peicaiBean);
//                    }
//                }
//            }
//        }
        this.pData = pData;
    }

    @Override
    public ShopCarPeicaiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recyclerview_shopcar_peicai, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ShopCarPeicaiAdapter.ViewHolder holder, int position) {
        holder.tvPeicai.setText(pData.get(position).getPcname());
    }

    @Override
    public int getItemCount() {
        if (pData != null) {
            return pData.size();
        }
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.tv_peicai)
        TextView tvPeicai;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
