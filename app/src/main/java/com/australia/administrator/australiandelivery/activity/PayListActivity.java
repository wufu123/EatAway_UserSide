package com.australia.administrator.australiandelivery.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.australia.administrator.australiandelivery.R;
import com.australia.administrator.australiandelivery.utils.Contants;
import com.australia.administrator.australiandelivery.utils.HttpUtils;
import com.australia.administrator.australiandelivery.view.TopBar;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by Administrator on 2017/7/10.
 */

public class PayListActivity extends AppCompatActivity {
    @Bind(R.id.tb_pay_list)
    TopBar tbPayList;
    @Bind(R.id.iv_yinhangka_left)
    ImageView ivYinhangkaLeft;
    @Bind(R.id.iv_yinghangka_right)
    ImageView ivYinghangkaRight;
    @Bind(R.id.iv_yinghangka_right1)
    ImageView ivYinghangkaRight1;
    @Bind(R.id.rl_yinhangka)
    RelativeLayout rlYinhangka;
    @Bind(R.id.iv_weixin_left)
    ImageView ivWeixinLeft;
    @Bind(R.id.rl_weixin)
    RelativeLayout rlWeixin;
    @Bind(R.id.iv_zhifubao_left)
    ImageView ivZhifubaoLeft;
    @Bind(R.id.rl_zhifubao)
    RelativeLayout rlZhifubao;
    private Intent i;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pay_list_activity);
        ButterKnife.bind(this);
        initTopBar();
        i = new Intent();
    }
    @OnClick({R.id.rl_yinhangka, R.id.rl_weixin, R.id.rl_zhifubao})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_yinhangka:
                i.putExtra("type",3);
                i.putExtra("payname",getString(R.string.xin_yong_ka));
                setResult(11,i);
                finish();
                break;
            case R.id.rl_weixin:

                break;
            case R.id.rl_zhifubao:
                i.putExtra("type",2);
                i.putExtra("payname",getString(R.string.zhi_fu_bao_zhi_fu));
                setResult(11,i);
                finish();
                break;
        }
    }

    private void initTopBar() {
        tbPayList.setTbCenterTv(R.string.xuan_ze_zhi_fu_fang_shi, R.color.white);
        tbPayList.setTbLeftIv(R.drawable.img_icon_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
