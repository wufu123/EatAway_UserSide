package com.australia.administrator.australiandelivery.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.australia.administrator.australiandelivery.R;
import com.australia.administrator.australiandelivery.bean.OrderBean;
import com.australia.administrator.australiandelivery.utils.GlideUtils;

import java.util.List;

import butterknife.Bind;

/**
 * Created by Administrator on 2017/7/10.
 */

public class OrderListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<OrderBean.MsgBean.GoodsBean> bean;

    public OrderListAdapter(Context context) {
        this.context = context;
    }

    public OrderListAdapter(Context context, List<OrderBean.MsgBean.GoodsBean> bean) {
        this.context = context;
        this.bean = bean;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(context).inflate(R.layout.item_order_details_middle_order_second, parent, false);
        OrderHolder holder = new OrderHolder(view);
        holder.llPeicai = (LinearLayout) view.findViewById(R.id.ll_peicai);
        holder.itemOrderDetailsMiddleOrderFoodname = (TextView) view.findViewById(R.id.item_order_details_middle_order_foodname);
        holder.itemOrderDetailsMiddleOrderFoodnum = (TextView) view.findViewById(R.id.item_order_details_middle_order_foodnum);
        holder.itemOrderDetailsMiddleOrderFoodfee = (TextView) view.findViewById(R.id.item_order_details_middle_order_foodfee);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        initOrderList((OrderHolder) holder, position);
    }

    @Override
    public int getItemCount() {
        if (bean != null)
            return bean.size();
        return 0;
    }

    private void initOrderList(OrderHolder holder, int position) {
        holder.itemOrderDetailsMiddleOrderFoodnum.setText("× " + bean.get(position).getNum());
        if (!TextUtils.isEmpty(bean.get(position).getGuigename())) {
            holder.itemOrderDetailsMiddleOrderFoodname.setText(bean.get(position).getGoodsname() + "(" +
            bean.get(position).getGuigename() + ")");
        }else {
            holder.itemOrderDetailsMiddleOrderFoodname.setText(bean.get(position).getGoodsname());
        }
        holder.itemOrderDetailsMiddleOrderFoodfee.setText("$" + bean.get(position).getZprice());
        String[] split = bean.get(position).getPcname().split(",");
        holder.llPeicai.removeAllViews();
        for (int i = 0; i < split.length; i++) {
            if (!TextUtils.equals(split[i], "")) {
                TextView tvPeicai = new TextView(context);
                tvPeicai.setText("+" + split[i]);
                holder.llPeicai.addView(tvPeicai);
            }
        }
    }

    /**
     * holders
     */
    class OrderHolder extends RecyclerView.ViewHolder {
        public OrderHolder(View itemView) {
            super(itemView);
        }

        TextView itemOrderDetailsMiddleOrderFoodname;
        LinearLayout llPeicai;
        TextView itemOrderDetailsMiddleOrderFoodnum;
        TextView itemOrderDetailsMiddleOrderFoodfee;
    }
}
