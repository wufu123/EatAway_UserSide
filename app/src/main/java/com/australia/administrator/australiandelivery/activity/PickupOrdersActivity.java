package com.australia.administrator.australiandelivery.activity;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.australia.administrator.australiandelivery.MyApplication;
import com.australia.administrator.australiandelivery.R;
import com.australia.administrator.australiandelivery.adapter.OrdersAdapterSecond;
import com.australia.administrator.australiandelivery.alipay.PayResult;
import com.australia.administrator.australiandelivery.bean.GoodsBean;
import com.australia.administrator.australiandelivery.comm.*;
import com.australia.administrator.australiandelivery.utils.Contants;
import com.australia.administrator.australiandelivery.utils.GlideUtils;
import com.australia.administrator.australiandelivery.utils.HttpUtils;
import com.australia.administrator.australiandelivery.utils.NumberFormatUtil;
import com.australia.administrator.australiandelivery.view.TopBar;
import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.dropin.utils.PaymentMethodType;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;

import static com.australia.administrator.australiandelivery.utils.Contants.URL_ALIPAY_CANCLE;

/**
 * Created by Administrator on 2017/7/8.
 */

public class PickupOrdersActivity extends com.australia.administrator.australiandelivery.comm.BaseActivity {
    private final int BANK = 1, ALIPAY = 2, WECHAT = 3;
    private final int DISCOUNT_REQUEST_CODE = 1110, DISCOUNT_RESULT_CODE = 1111;
    private final int BEIZHU_REQUEST_CODE = 7;
    public static final int ORDER_RESULT = 1001;
    @Bind(R.id.tb_pickup_orders)
    TopBar tbPickupOrders;
    @Bind(R.id.shopping_cart)
    TextView shoppingCart;
    @Bind(R.id.shoppingPrise)
    TextView shoppingPrise;
    @Bind(R.id.settlement)
    TextView settlement;
    @Bind(R.id.toolBar)
    LinearLayout toolBar;
    @Bind(R.id.et_name)
    EditText etName;
    @Bind(R.id.et_phone)
    EditText etPhone;
    @Bind(R.id.list_line)
    View listLine;
    @Bind(R.id.tv_orders_time)
    TextView tvOrdersTime;
    @Bind(R.id.tv_hint)
    TextView tvHint;
    @Bind(R.id.ll_time)
    LinearLayout llTime;
    @Bind(R.id.iv_orders_shop)
    CircleImageView ivOrdersShop;
    @Bind(R.id.tv_orders_shop_name)
    TextView tvOrdersShopName;
    @Bind(R.id.rv_orders)
    RecyclerView rvOrders;
    @Bind(R.id.tv_orders_discount)
    TextView tvOrdersDiscount;
    @Bind(R.id.tv_orders_money)
    TextView tvOrdersMoney;
    @Bind(R.id.tv_orders_taste_notes)
    TextView tvOrdersTasteNotes;
    @Bind(R.id.ll_beizhu)
    LinearLayout llBeizhu;
    @Bind(R.id.tv_orders_method_of_payment)
    TextView tvOrdersMethodOfPayment;
    @Bind(R.id.rl_orders_pay)
    RelativeLayout rlOrdersPay;
    @Bind(R.id.tv_orders_get_discount)
    TextView tvOrdersGetDiscount;
    @Bind(R.id.rl_orders_discount)
    RelativeLayout rlOrdersDiscount;
    @Bind(R.id.sv_pickup_orders)
    NestedScrollView svPickupOrders;

    private BraintreeFragment mBraintreeFragment;
    private String mAuthorization;
    private int hour;
    private int minute;
    private List<GoodsBean> goods;
    private OrdersAdapterSecond adapter;
    private String name;
    private String phone;
    private String sex;
    private String weizhi;
    private String id;
    private String coordinate;
    private String shopid;
    private String price;
    private double price1;
    private TimePickerDialog dialog;

    private String format1 = "0";
    private boolean isp = true;
    private String am = "am";
    private int pay = 5;//支付方式：4货到付款3银行卡、2支付宝、1微信

    private String newtime;
    //优惠码id
    private String yid = "";
    //优惠码对应折扣金额
    private double ymoney = 0;
    //优惠码金额
    private double discountNum;
    //优惠码类型：1百分比 2折扣金额
    private int discountState;
    //优惠码是否使用过
    private boolean isUsedDiscout = false;
    //原有价格
    private double prePrice = 0;
    //折扣价格
    private double discount = 0;

    private String nonce;

    //支付宝支付相关：
    // 商户PID
    public static final String PARTNER = "2088721824853457";
    // 商户收款账号
    public static final String SELLER = "2088721824853457";

    private static final int SDK_PAY_FLAG = 1;

    private String sign = "";

    private String time = "";

    private String orderid;

    private Handler mHandler = new Handler() {
        @SuppressWarnings("unused")
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG: {
                    PayResult payResult = new PayResult((String) msg.obj);
                    /**
                     * 同步返回的结果必须放置到服务端进行验证（验证的规则请看https://doc.open.alipay.com/doc2/
                     * detail.htm?spm=0.0.0.0.xdvAU6&treeId=59&articleId=103665&
                     * docType=1) 建议商户依赖异步通知
                     */
                    String resultInfo = payResult.getResult();// 同步返回需要验证的信息

                    String resultStatus = payResult.getResultStatus();
                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                    if (TextUtils.equals(resultStatus, "9000")) {
                        Toast.makeText(PickupOrdersActivity.this, R.string.xia_dan_cheng_gong, Toast.LENGTH_SHORT).show();
                        setResult(ORDER_RESULT);
                        finish();
                    } else {
                        // 判断resultStatus 为非"9000"则代表可能支付失败
                        // "8000"代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            Toast.makeText(PickupOrdersActivity.this, "支付结果确认中", Toast.LENGTH_SHORT).show();
                        } else {
                            // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
//                            Toast.makeText(PickupOrdersActivity.this, "支付失败", Toast.LENGTH_SHORT).show();
                            CancelOrder(orderid);
                        }
                    }
                    break;
                }
                default:
                    break;
            }
        }
    };
    private String ycode;

    private String openTime;
    private String[] times;

    /**
     *支付失败后的调用接口*/
    protected void CancelOrder(String orderid) {
        showDialog();
        if (!TextUtils.isEmpty(orderid)) {
            HttpUtils httpUtils = new HttpUtils(URL_ALIPAY_CANCLE) {
                @Override
                public void onError(Call call, Exception e, int id) {
                    hidDialog();
                    Toast.makeText(PickupOrdersActivity.this, R.string.please_check_your_network_connection, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onResponse(String response, int id) {
                    Log.i("zhifubao", "onResponse: " + response);
                    hidDialog();
                    try {
                        JSONObject o = new JSONObject(response);
                        int status = o.getInt("status");
                        if (status == 1) {
                            Toast.makeText(PickupOrdersActivity.this, R.string.ding_dan_yi_qu_xiao, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            };
            httpUtils.addParam("orderid", orderid);
            httpUtils.clicent();
        }else {
            hidDialog();
            Toast.makeText(this, "orderid is empty!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_pickup_orders;
    }

    @Override
    protected void initDate() {
        goods = MyApplication.goods;
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        rvOrders.setLayoutManager(manager);
        rvOrders.setNestedScrollingEnabled(false);
        rvOrders.setHasFixedSize(true);
        adapter = new OrdersAdapterSecond(this, goods);
        rvOrders.setAdapter(adapter);
        for (int i = 0; i < goods.size(); i++) {
            discount += goods.get(i).getDisPrice() * goods.get(i).getNumber();
            price1 += goods.get(i).getRealPrice() * goods.get(i).getNumber();
        }
        discount = NumberFormatUtil.round(discount, 2);
        price1 = NumberFormatUtil.round(price1, 2);
        tvOrdersDiscount.setText("-$" + discount);
        tvOrdersMoney.setText("$" + price1);
        shoppingPrise.setText("$" + price1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        final Calendar c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);
        shopid = getIntent().getStringExtra("shopid");
        openTime = getIntent().getStringExtra("time");
        times = openTime.split(",");
        if (times.length > 1) {
            if (MyApplication.isEnglish) {
                tvHint.setText("Please choose pick-up time between " + times[0] + "or" + times[1] + " otherwise the merchant may refuse the order. If the order overtime, please communicate with the merchants in advance, otherwise the merchants have the right to cancel the goods and refuse to refund.");
            }else {
                tvHint.setText("请您在" + times[0] + "或" + times[1] + "之间选择取货时间，否则商家可能拒绝订单。如超时请" +
                        "提前与商家沟通，否则商家有权取消该商品并拒绝退款");
            }
        }else {
            if (MyApplication.isEnglish) {
                tvHint.setText("Please choose pick-up time between " + times[0] + " otherwise the merchant may refuse the order. If the order overtime, please communicate with the merchants in advance, otherwise the merchants have the right to cancel the goods and refuse to refund.");
            }else {
                tvHint.setText("请您在" + times[0] + "之间选择取货时间，否则商家可能拒绝订单。如超时请" +
                        "提前与商家沟通，否则商家有权取消该商品并拒绝退款");
            }
        }
        GlideUtils.load(this, getIntent().getStringExtra("shopicon"), ivOrdersShop, GlideUtils.Shape.ShopIcon);
        tvOrdersShopName.setText(getIntent().getStringExtra("shopname"));
        initTopBar();
    }

    private void initTopBar() {
        tbPickupOrders.setTbCenterTv(R.string.ti_jiao_ding_dan_title, R.color.white);
        tbPickupOrders.setTbLeftIv(R.drawable.img_icon_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.tv_orders_time, R.id.rl_orders_pay,
            R.id.settlement, R.id.ll_beizhu,
            R.id.rl_orders_discount})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_beizhu:
                Intent intent3 = new Intent(this, BeiZhuActivity.class);
                startActivityForResult(intent3, BEIZHU_REQUEST_CODE);
                break;
            case R.id.rl_orders_discount:
                Intent intent2 = new Intent(this, DisCountNumberActivity.class);
                startActivityForResult(intent2, DISCOUNT_REQUEST_CODE);
                break;
            case R.id.tv_orders_time:
                dialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (hourOfDay > 12) {
                            hourOfDay = hourOfDay - 12;
                            am = "pm";
                        } else {
                            am = "am";
                        }
                        newtime = hourOfDay + ":" + minute + " " + am;
                        tvOrdersTime.setText(getString(R.string.zi_qu_shi_jian) + ":" + hourOfDay + ":" + minute + " " + am);
                    }
                }, hour, minute, false);
                dialog.show();
                break;
            case R.id.rl_orders_pay:
                Intent intent1 = new Intent(PickupOrdersActivity.this, PayListActivity.class);
                startActivityForResult(intent1, 10);
                break;
            case R.id.settlement:
               if (pay == 3) {
                    if (TextUtils.isEmpty(etName.getText())) {
                        Toast.makeText(this, "请输入您的姓名", Toast.LENGTH_SHORT).show();
                        return;
                    }else if (TextUtils.isEmpty(etPhone.getText())) {
                        Toast.makeText(this, "请输入您的联系方式", Toast.LENGTH_SHORT).show();
                        return;
                    }else if (TextUtils.equals(tvOrdersTime.getText(), getString(R.string.zi_qu_shi_jian))) {
                        Toast.makeText(this, "请选择您的自取时间", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    showDialog();
                    HttpUtils httpUtils = new HttpUtils(Contants.URL_GET_TOKEN) {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            hidDialog();
                            Log.i("brtoken", "onError: " + e.getMessage());
                            Toast.makeText(PickupOrdersActivity.this, R.string.please_check_your_network_connection, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onResponse(String response, int id) {
                            hidDialog();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                mAuthorization = jsonObject.getString("token");
//                                    mBraintreeFragment = BraintreeFragment.newInstance(SubmitOrdersActivity.this, mAuthorization);
                                DropInRequest dropInRequest = new DropInRequest().clientToken(mAuthorization);//.clientToken(clientToken)
                                startActivityForResult(dropInRequest.getIntent(PickupOrdersActivity.this), 4);

                                DropInResult.fetchDropInResult(PickupOrdersActivity.this, mAuthorization, new DropInResult.DropInResultListener() {
                                    @Override
                                    public void onError(Exception exception) {
                                        Log.i("dropui", "onError: " + exception.getMessage());
                                        // an error occurred
                                    }

                                    @Override
                                    public void onResult(DropInResult result) {
                                        if (result.getPaymentMethodType() != null) {
                                            Log.i("dropui", "onResult: " + result.getPaymentMethodNonce().getNonce());
                                            // use the icon and name to show in your UI
                                            int icon = result.getPaymentMethodType().getDrawable();
                                            int name = result.getPaymentMethodType().getLocalizedName();

                                            if (result.getPaymentMethodType() == PaymentMethodType.ANDROID_PAY) {
                                                // The last payment method the user used was Android Pay. The Android Pay
                                                // flow will need to be performed by the user again at the time of checkout
                                                // using AndroidPay#requestAndroidPay(...). No PaymentMethodNonce will be
                                                // present in result.getPaymentMethodNonce(), this is only an indication that
                                                // the user last used Android Pay. Note: if you have enabled preauthorization
                                                // and the user checked the "Use selected info for future purchases from this app"
                                                // last time Android Pay was used you may need to call
                                                // AndroidPay#requestAndroidPay(...) and then offer the user the option to change
                                                // the default payment method using AndroidPay#changePaymentMethod(...)
                                            } else {
                                                // show the payment method in your UI and charge the user at the
                                                // time of checkout using the nonce: paymentMethod.getNonce()
                                                nonce = result.getPaymentMethodNonce().getNonce();
                                            }
                                        } else {
                                            // there was no existing payment method
                                            Log.i("dropui", "onResult: " + "null");
                                        }
                                    }
                                });
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
//                                catch (InvalidArgumentException e) {
//                                    e.printStackTrace();
//                                }
                        }
                    };
                    Log.i("brtoken", "onViewClicked: " + MyApplication.getLogin().getUserId());
                    httpUtils.addParam("userid", MyApplication.getLogin().getUserId());
                    httpUtils.addParam("token", MyApplication.getLogin().getToken());
                    httpUtils.clicent();
                } else if (pay == 1) {

                } else if (pay == 2) {   //支付宝
                    if (TextUtils.isEmpty(etName.getText())) {
                        Toast.makeText(this, "请输入您的姓名", Toast.LENGTH_SHORT).show();
                        return;
                    }else if (TextUtils.isEmpty(etPhone.getText())) {
                        Toast.makeText(this, "请输入您的联系方式", Toast.LENGTH_SHORT).show();
                        return;
                    }else if (TextUtils.equals(tvOrdersTime.getText(), getString(R.string.zi_qu_shi_jian))) {
                        Toast.makeText(this, "请选择您的自取时间", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    HttpUtils httpUtils = new HttpUtils(Contants.URL_ALIPAY_SIGN) {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            Log.i("zhifubao", "onError: " + e.getMessage());
                            Toast.makeText(PickupOrdersActivity.this, R.string.please_check_your_network_connection, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onResponse(String response, int id) {
                            Log.i("zhifubao", "onResponse: " + response);
                                try {
                                    JSONObject o = new JSONObject(response);
                                    sign = o.getString("msg");
                                    orderid = o.getString("orderid");
                                    if (TextUtils.isEmpty(sign)) {
                                        Runnable payRunnable = new Runnable() {
                                            @Override
                                            public void run() {
                                                // 构造PayTask 对象
                                                PayTask alipay = new PayTask(PickupOrdersActivity.this);
                                                // 调用支付接口，获取支付结果
                                                String result = alipay.pay(sign, true);

                                                Message msg = new Message();
                                                msg.what = SDK_PAY_FLAG;
                                                msg.obj = result;
                                                mHandler.sendMessage(msg);
                                            }
                                        };
                                        // 必须异步调用
                                        Thread payThread = new Thread(payRunnable);
                                        payThread.start();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                        }
                    };
                    httpUtils.addParam("allprices", price1 + Double.parseDouble(format1) + "").addParams("shopid", shopid)
                            .addParams("money", format1 + "").addParams("userid", MyApplication.getLogin().getUserId())
                            .addParams("cometime", newtime).addParams("name", etName.getText().toString())
                            .addParams("phone", etPhone.getText().toString()).addParams("beizhu", tvOrdersTasteNotes.getText().toString())
                            .addParams("token", MyApplication.getLogin().getToken());
                   for (int i = 0; i < goods.size(); i++) {
                       httpUtils.addParam("goods[" + i + "][0]", goods.get(i).getGoodsid());
                       httpUtils.addParam("goods[" + i + "][1]", goods.get(i).getNumber() + "");
                       if (goods.get(i).getSelectGui() != null) {
                           httpUtils.addParam("goods[" + i + "][2][0]", goods.get(i).getSelectGui().getGid());
                           httpUtils.addParam("goods[" + i + "][2][1]", goods.get(i).getSelectGui().getGuige());
                           httpUtils.addParam("goods[" + i + "][2][2]", goods.get(i).getSelectGui().getPrimoney());
                       }
                       if (goods.get(i).getSelectPei() != null) {
                           for (int j = 0; j < goods.get(i).getSelectPei().size(); j++) {
                               httpUtils.addParam("goods[" + i + "][3]" + "[" + j + "][0]", goods.get(i).getSelectPei().get(j).getPcid());
                               httpUtils.addParam("goods[" + i + "][3]" + "[" + j + "][1]", goods.get(i).getSelectPei().get(j).getPcname());
                               httpUtils.addParam("goods[" + i + "][3]" + "[" + j + "][2]", goods.get(i).getSelectPei().get(j).getPcprice());
                           }
                       }
                   }
                   if (yid != null && !TextUtils.equals(yid, "")) {
                       Log.i("xiadan", "yid: " + yid);
                       httpUtils.addParam("yid", yid);
                       httpUtils.addParam("ymoney", ymoney + "");
                       httpUtils.addParam("ycodema", ycode);
                   }
                    httpUtils.addParam("youhuiall", (ymoney + discount) + "");
                    httpUtils.addParam("type", "2");
                    httpUtils.addParam("partner", PARTNER);
                    httpUtils.addParam("service", "mobile.securitypay.pay");
                    httpUtils.clicent();
                } else if (pay == 4) {
                    //测试用：货到付款
//                    postOrder(0, Contants.URL_HADDODER);
                    if (TextUtils.isEmpty(etName.getText())) {
                        Toast.makeText(this, "请输入您的姓名", Toast.LENGTH_SHORT).show();
                        return;
                    }else if (TextUtils.isEmpty(etPhone.getText())) {
                        Toast.makeText(this, "请输入您的联系方式", Toast.LENGTH_SHORT).show();
                        return;
                    }else if (TextUtils.equals(tvOrdersTime.getText(), getString(R.string.zi_qu_shi_jian))) {
                        Toast.makeText(this, "请选择您的自取时间", Toast.LENGTH_SHORT).show();
                        return;
                    }
                   showDialog();
                    postOrder(0, Contants.URL_ADDODER);
                } else {
                    Toast.makeText(this, R.string.qing_xuan_ze_zhi_fu_fang_shi, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DISCOUNT_REQUEST_CODE) {
            if (resultCode == DISCOUNT_RESULT_CODE) {
                if (!isUsedDiscout) {
                    prePrice = price1;
                    isUsedDiscout = true;
                    //没有使用过的话，计算
                    getDiscountmoney(data);
                } else {
                    //使用过，使用原来的价格重新计算
                    price1 = prePrice;
                    getDiscountmoney(data);
                }
            }
        } else if (requestCode == 0) {
            if (resultCode == 3) {
                name = data.getStringExtra("name");
                phone = data.getStringExtra("phone");
                sex = data.getStringExtra("sex");
                weizhi = data.getStringExtra("weizhi");
                id = data.getStringExtra("id");
                coordinate = data.getStringExtra("coordinate");
                showDialog();
            }
        } else if (requestCode == BEIZHU_REQUEST_CODE) {
            if (resultCode == 9) {
                tvOrdersTasteNotes.setText(data.getStringExtra("note"));
            }
        } else if (requestCode == 10) {
            if (resultCode == 11) {
                pay = data.getIntExtra("type", 4);
                tvOrdersMethodOfPayment.setText(data.getStringExtra("payname"));
            }
        } else if (requestCode == 4) {
            if (resultCode == Activity.RESULT_OK) {
                Log.i("dropui", "onResult: " + "------");
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                // use the result to update your UI and send the payment method nonce to your server
                if (TextUtils.isEmpty(nonce)) {
                    nonce = result.getPaymentMethodNonce().getNonce();
                }
//                result.
                showDialog();
                postOrder(BANK, Contants.URL_ADDODER);
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            // the user canceled
            Toast.makeText(this, R.string.qu_xiao_ding_dan, Toast.LENGTH_SHORT).show();
        } else {
            // handle errors here, an exception may be available in
            Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
            Toast.makeText(this, R.string.xia_dan_shi_bai, Toast.LENGTH_SHORT).show();
        }
    }


    class MyBean {

        /**
         * status : 1
         * msg : {"coordinate":"138.6062277,-34.92060300000001","maxprice":"-1","maxlong":"-1","lkmoney":"2","long":"20000","juli":1.01}
         */

        private int status;
        private MsgBean msg;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public MsgBean getMsg() {
            return msg;
        }

        public void setMsg(MsgBean msg) {
            this.msg = msg;
        }

        public class MsgBean {
            /**
             * coordinate : 138.6062277,-34.92060300000001
             * maxprice : -1
             * maxlong : -1
             * lkmoney : 2
             * long : 20000
             * juli : 1.01
             */

            private String coordinate;
            private String maxprice;    //满多少钱免费
            private String lkmoney;
            @SerializedName("long")
            private String longX;       //商家的最大配送距离
            private String juli;

            //新增
            private String minprice;
            private String maxlong;
            private String maxmoney;

            public String getMaxmoney() {
                return maxmoney;
            }

            public void setMaxmoney(String maxmoney) {
                this.maxmoney = maxmoney;
            }

            public String getMinprice() {
                return minprice;
            }

            public void setMinprice(String minprice) {
                this.minprice = minprice;
            }

            public String getCoordinate() {
                return coordinate;
            }

            public void setCoordinate(String coordinate) {
                this.coordinate = coordinate;
            }

            public String getMaxprice() {
                return maxprice;
            }

            public void setMaxprice(String maxprice) {
                this.maxprice = maxprice;
            }

            public String getMaxlong() {
                return maxlong;
            }

            public void setMaxlong(String maxlong) {
                this.maxlong = maxlong;
            }

            public String getLkmoney() {
                return lkmoney;
            }

            public void setLkmoney(String lkmoney) {
                this.lkmoney = lkmoney;
            }

            public String getLongX() {
                return longX;
            }

            public void setLongX(String longX) {
                this.longX = longX;
            }

            public String getJuli() {
                return juli;
            }

            public void setJuli(String juli) {
                this.juli = juli;
            }
        }
    }

    private void postOrder(int type, String url) {
        Log.i("xiadan", "url" + url);
        HttpUtils httpUtils = new HttpUtils(url) {
            @Override
            public void onError(Call call, Exception e, int id) {
                hidDialog();
                Log.i("xiadan", e.getMessage());
                Toast.makeText(PickupOrdersActivity.this, R.string.please_check_your_network_connection, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(String response, int id) {
                Log.i("zhifu", "onResponse: " + response);
                hidDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    if (status == 0) {
                        Toast.makeText(PickupOrdersActivity.this, R.string.xia_dan_shi_bai, Toast.LENGTH_SHORT).show();
                    } else if (status == 1) {
                        Toast.makeText(PickupOrdersActivity.this, R.string.xia_dan_cheng_gong, Toast.LENGTH_SHORT).show();
                        finish();
                    } else if (status == 9) {
                        Toast.makeText(PickupOrdersActivity.this, R.string.login_agin, Toast.LENGTH_SHORT).show();
                        MyApplication.saveLogin(null);
                        startActivity(new Intent(PickupOrdersActivity.this, LoginActivity.class));
                        finish();
                    } else if (status == 4) {
                        Toast.makeText(PickupOrdersActivity.this, R.string.shang_dian_wei_ying_ye, Toast.LENGTH_SHORT).show();
                    } else if (status == 5) {
                        Toast.makeText(PickupOrdersActivity.this, R.string.chao_chu_zui_da_pei_song_ju_li, Toast.LENGTH_SHORT).show();
                    } else if (status == 11) {
                        //优惠码已停用
                        Toast.makeText(PickupOrdersActivity.this, R.string.you_hui_ma_yi_ting_yong, Toast.LENGTH_SHORT).show();
                    } else if (status == 12) {
                        //优惠活动未开始
                        Toast.makeText(PickupOrdersActivity.this, R.string.you_hui_huo_dong_wei_kai_shi, Toast.LENGTH_SHORT).show();
                    } else if (status == 13) {
                        //优惠码已过期
                        Toast.makeText(PickupOrdersActivity.this, R.string.you_hui_ma_yi_guo_qi, Toast.LENGTH_SHORT).show();
                    } else if (status == 14) {
                        //修改优惠码总使用次数失败
                        Toast.makeText(PickupOrdersActivity.this, R.string.xiu_gai_you_hui_ma_zong_shi_yong_ci_shi_shi_bai, Toast.LENGTH_SHORT).show();
                    } else if (status == 15) {
                        //优惠码使用次数超限
                        Toast.makeText(PickupOrdersActivity.this, R.string.gai_you_hui_ma_yi_chao_chu_shi_yong_shang_xian, Toast.LENGTH_SHORT).show();
                    } else if (status == 16) {
                        //优惠码个人使用次数修改失败
                        Toast.makeText(PickupOrdersActivity.this, R.string.you_hui_ma_ge_ren_shi_yong_ci_shu_xiu_gai_shi_bai, Toast.LENGTH_SHORT).show();
                    } else if (status == 17) {
                        //优惠码个人使用信息添加失败
                        Toast.makeText(PickupOrdersActivity.this, R.string.you_hui_ma_ge_ren_shi_yong_ci_shu_tian_jia_shi_bai, Toast.LENGTH_SHORT).show();
                    } else if (status == 18) {
                        //优惠码错误（被删除未找到）
                        Toast.makeText(PickupOrdersActivity.this, R.string.gai_you_hui_ma_yi_bei_shan_chu, Toast.LENGTH_SHORT).show();
                    } else if (status == 19) {
                        //商品价格有变动，请重新下单
                        Toast.makeText(PickupOrdersActivity.this, R.string.shang_pin_jia_ge_bian_dong, Toast.LENGTH_SHORT).show();
                    } else if (status == 20) {
                        //订单添加失败
                        Toast.makeText(PickupOrdersActivity.this, R.string.ding_dan_tian_jia_shi_bai, Toast.LENGTH_SHORT).show();
                    } else if (status == 21) {
                        //订单商品信息添加失败
                        Toast.makeText(PickupOrdersActivity.this, R.string.ding_dan_shang_pin_xin_xi_tian_jia_shi_bai, Toast.LENGTH_SHORT).show();
                    } else if (status == 22) {
                        //商品信息不能为空
                        Toast.makeText(PickupOrdersActivity.this, R.string.shang_pin_xin_xi_bu_neng_wei_kong, Toast.LENGTH_SHORT).show();
                    } else if (status == 23) {
                        //收入明细添加失败
                        Toast.makeText(PickupOrdersActivity.this, R.string.shou_ru_ming_xi_tian_jia_shi_bai, Toast.LENGTH_SHORT).show();
                    } else if (status == 24) {
                        //极光推送请求异常
                        Toast.makeText(PickupOrdersActivity.this, R.string.ding_dan_tong_zhi_qing_qiu_yi_chang, Toast.LENGTH_SHORT).show();
                    } else if (status == 25) {
                        //极光推送回复异常
                        Toast.makeText(PickupOrdersActivity.this, R.string.ding_dan_tong_zhi_hui_fu_yi_chang, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        String trim = tvOrdersTime.getText().toString().trim();
        String s1 = trim.replaceAll(getString(R.string.zi_qu_shi_jian), "");
        Log.i("xiadan", "postOrder: " + price1);
        httpUtils.addParam("allprices", price1 + "").addParams("shopid", shopid)
                .addParams("userid", MyApplication.getLogin().getUserId())
                .addParams("cometime", newtime).addParams("name", etName.getText().toString())
                .addParams("phone", etPhone.getText().toString()).addParams("beizhu", tvOrdersTasteNotes.getText().toString())
                .addParams("token", MyApplication.getLogin().getToken());
        for (int i = 0; i < goods.size(); i++) {
            httpUtils.addParam("goods[" + i + "][0]", goods.get(i).getGoodsid());
            httpUtils.addParam("goods[" + i + "][1]", goods.get(i).getNumber() + "");
            if (goods.get(i).getSelectGui() != null) {
                httpUtils.addParam("goods[" + i + "][2][0]", goods.get(i).getSelectGui().getGid());
                httpUtils.addParam("goods[" + i + "][2][1]", goods.get(i).getSelectGui().getGuige());
                httpUtils.addParam("goods[" + i + "][2][2]", goods.get(i).getSelectGui().getPrimoney());
            }
            if (goods.get(i).getSelectPei() != null) {
                for (int j = 0; j < goods.get(i).getSelectPei().size(); j++) {
                    httpUtils.addParam("goods[" + i + "][3]" + "[" + j + "][0]", goods.get(i).getSelectPei().get(j).getPcid());
                    httpUtils.addParam("goods[" + i + "][3]" + "[" + j + "][1]", goods.get(i).getSelectPei().get(j).getPcname());
                    httpUtils.addParam("goods[" + i + "][3]" + "[" + j + "][2]", goods.get(i).getSelectPei().get(j).getPcprice());
                }
            }
        }
        if (yid != null && !TextUtils.equals(yid, "")) {
            Log.i("xiadan", "yid: " + yid);
            httpUtils.addParam("yid", yid);
            httpUtils.addParam("ymoney", ymoney + "");
            httpUtils.addParam("ycodema", ycode);
        }
        if (type == BANK) {
            //银行卡
            httpUtils.addParam("nonce", nonce);
            httpUtils.addParam("pay", "3");
        }
        httpUtils.addParam("youhuiall", (ymoney + discount) + "");
        httpUtils.addParam("type", "2");
        httpUtils.clicent();
    }

    /**
     * 根据原价格进行优惠码的优惠计算
     * 注：该函数中刷新的是参数price1
     */
    private void getDiscountmoney(Intent data) {
        //优惠码
        yid = data.getStringExtra("yid");
        discountState = data.getIntExtra("state", 0);
        discountNum = data.getDoubleExtra("discount", 0);
        ycode = data.getStringExtra("ycode");
        if (discountState == 1) {
            ymoney = NumberFormatUtil.round(price1 * (discountNum / 100), 2);
            price1 = NumberFormatUtil.round(price1 - ymoney, 2);
            tvOrdersGetDiscount.setText((int) discountNum + "% off");
            tvOrdersDiscount.setText("-$" + NumberFormatUtil.round((ymoney + discount),2));
            tvOrdersMoney.setText("$" + price1);
            shoppingPrise.setText("$" + price1);
        } else if (discountState == 2) {
            if (price1 - discountNum > 0) {
                ymoney = NumberFormatUtil.round(discountNum, 2);
                price1 = NumberFormatUtil.round(price1 - discountNum, 2);
                tvOrdersDiscount.setText("-$" + (discountNum + discount));
            } else {
                ymoney = price1;
                tvOrdersDiscount.setText("-$" + price1);
                price1 = 0;
            }
            tvOrdersGetDiscount.setText("-$" + NumberFormatUtil.round((discountNum + discount), 2));
            tvOrdersMoney.setText("$" + price1);
            shoppingPrise.setText("$" + price1);
        } else {
            discountNum = 0;
        }
    }
}
