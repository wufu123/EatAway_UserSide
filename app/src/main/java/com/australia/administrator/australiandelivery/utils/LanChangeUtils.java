package com.australia.administrator.australiandelivery.utils;

/**
 * Created by local123 on 2017/11/25.
 */

public class LanChangeUtils {
    public static String getString(String str, boolean isEnglish) {
        String[] split = str.split("\\(");
        if (split.length > 1) {
            if (isEnglish) {
                return split[0];
            }else {
                return split[1].substring(0, split[1].length()-1);
            }
        }else {
            return split[0];
        }
    }
}
