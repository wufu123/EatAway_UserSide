package com.australia.administrator.australiandelivery.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.australia.administrator.australiandelivery.MyApplication;
import com.australia.administrator.australiandelivery.R;
import com.australia.administrator.australiandelivery.adapter.CurrentOrderAdapter;
import com.australia.administrator.australiandelivery.bean.CurrentOrderBean;
import com.australia.administrator.australiandelivery.comm.BaseActivity;
import com.australia.administrator.australiandelivery.loadmore.LoadMoreWrapper;
import com.australia.administrator.australiandelivery.utils.Contants;
import com.australia.administrator.australiandelivery.utils.HttpUtils;
import com.australia.administrator.australiandelivery.utils.ToastUtils;
import com.australia.administrator.australiandelivery.view.TopBar;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.Call;

public class CurrentOrderActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener{
    private final int MAX_SERVER_INFO = 15;
    @Bind(R.id.rv_current_order)
    public RecyclerView rvCurrentOrder;
    @Bind(R.id.tp_current_order)
    TopBar tpCurrentOrder;
    @Bind(R.id.tv_current_order_none)
    public TextView tvCurrentOrderNone;
    @Bind(R.id.sr_current_order)
    SwipeRefreshLayout srCurrentOrder;
    private CurrentOrderAdapter adapter;

    CurrentOrderBean bean;

    private LoadMoreWrapper mLoadMoreWrapper;
    private final int REQUEST_CODE = 10086;
    private static final int REFRESH_COMPLETE = 0X110;
    private boolean iSDestroy = false;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case REFRESH_COMPLETE:
                    if (iSDestroy) {
                        return;
                    }
                    loadData();
                    break;

            }
        }
    };

    @Override
    public void onRefresh() {
        mHandler.sendEmptyMessageDelayed(REFRESH_COMPLETE, 2000);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_current_order;
    }

    @Override
    protected void initDate() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        initTopBar();
        initRecylerView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showDialog();
        loadData();
    }

    private void initRecylerView() {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        rvCurrentOrder.setLayoutManager(manager);
        adapter = new CurrentOrderAdapter(this);

        srCurrentOrder.setOnRefreshListener(this);
        srCurrentOrder.setColorScheme(android.R.color.holo_green_light);
//        srCurrentOrder.setRefreshing(true);
        rvCurrentOrder.setAdapter(adapter);
    }

    private void initTopBar() {
        tpCurrentOrder.setTbCenterTv(R.string.dang_qian_ding_dan, R.color.white);
        tpCurrentOrder.setTbLeftIv(R.drawable.img_icon_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void loadData() {
        if (MyApplication.getLogin() != null) {
            HttpUtils httpUtils = new HttpUtils(Contants.URL_CURRENT) {
                @Override
                public void onError(Call call, Exception e, int id) {
                    hidDialog();
                    srCurrentOrder.setRefreshing(false);
                    Toast.makeText(CurrentOrderActivity.this, R.string.please_check_your_network_connection, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onResponse(String response, int id) {
                    Log.i("dangqiandingdan", response);
                    srCurrentOrder.setRefreshing(false);
                    hidDialog();
                    try {
                        JSONObject o = new JSONObject(response);
                        int status = o.getInt("status");
                        if (status == 0) {
                            Toast.makeText(CurrentOrderActivity.this, R.string.please_check_your_network_connection, Toast.LENGTH_SHORT).show();
                        } else if (status == 1) {
                            bean = new Gson().fromJson(response, CurrentOrderBean.class);
                            if (bean.getMsg() != null && bean.getMsg().size() > 0) {
                                rvCurrentOrder.setVisibility(View.VISIBLE);
                                tvCurrentOrderNone.setVisibility(View.GONE);
                                adapter.setBean(bean);
                                adapter.notifyDataSetChanged();
                            }
                        } else if (status == 9) {
                            Toast.makeText(CurrentOrderActivity.this, R.string.Please_Log_on_again, Toast.LENGTH_SHORT).show();
                            MyApplication.saveLogin(null);
                            goToActivity(LoginActivity.class);
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            };
            httpUtils.addParam("userid", MyApplication.getLogin().getUserId());
            httpUtils.addParam("token", MyApplication.getLogin().getToken());
            httpUtils.clicent();
        } else {
            hidDialog();
            ToastUtils.showToast(R.string.Please_Log_on_again, CurrentOrderActivity.this);
            goToActivity(LoginActivity.class);
            finish();
        }
    }
}
