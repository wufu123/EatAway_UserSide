package com.australia.administrator.australiandelivery.bean;

/**
 * Created by local123 on 2017/11/19.
 */

public class DiscountNumberBean {

    /**
     * status : 1
     * msg : {"yid":"4","codema":"10off","state":"1","money":"10","num":"0","big":"5","flag":"1","start":"2017-11-01 00:00:00","stop":"2017-11-30 00:00:00","addtime":"2017-11-15 10:16:59"}
     */

    private int status;
    private MsgBean msg;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public MsgBean getMsg() {
        return msg;
    }

    public void setMsg(MsgBean msg) {
        this.msg = msg;
    }

    public static class MsgBean {
        /**
         * yid : 4
         * codema : 10off
         * state : 1
         * money : 10
         * num : 0
         * big : 5
         * flag : 1
         * start : 2017-11-01 00:00:00
         * stop : 2017-11-30 00:00:00
         * addtime : 2017-11-15 10:16:59
         */

        private String yid;
        private String codema;
        private String state;
        private String money;
        private String num;
        private String big;
        private String flag;
        private String start;
        private String stop;
        private String addtime;

        public String getYid() {
            return yid;
        }

        public void setYid(String yid) {
            this.yid = yid;
        }

        public String getCodema() {
            return codema;
        }

        public void setCodema(String codema) {
            this.codema = codema;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getNum() {
            return num;
        }

        public void setNum(String num) {
            this.num = num;
        }

        public String getBig() {
            return big;
        }

        public void setBig(String big) {
            this.big = big;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        public String getStop() {
            return stop;
        }

        public void setStop(String stop) {
            this.stop = stop;
        }

        public String getAddtime() {
            return addtime;
        }

        public void setAddtime(String addtime) {
            this.addtime = addtime;
        }
    }
}
