package com.australia.administrator.australiandelivery.utils;

import static com.australia.administrator.australiandelivery.utils.Contants.ADDRESS_DISTANCE;

/**
 * Created by local123 on 2017/11/23.
 */

public class AddressRequestUtils {
    /**
     * 拼接google矩阵计算距离接口的函数
     * @param self  自己的坐标
     * @param destination   目的地坐标
     * @param key   google key
     * @param isKm  是否单位为千米
     * @return  请求url
     * https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial
     * &origins=41.43206,-81.38992|-33.86748,151.20699&key=YOUR_API_KEY
     */
    public static String getUrl(String self, String destination, String key, boolean isKm) {
        if (isKm) {
            return ADDRESS_DISTANCE + "&origins=" + self + "&destinations=" + destination +"&units=metric" + "&mode=bicycling" + "&language=fr-FR" + key;
        }else {
            return ADDRESS_DISTANCE + "&origins=" + self + "|" + destination + "&units=imperial" + "&language=fr-FR" + key;
        }
    }
}
