package com.australia.administrator.australiandelivery.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Administrator on 2017/7/28.
 */

public class CurrentOrderBean {

    /**
     * status : 1
     * msg : [{"shopid":"45","shopname":"四川辣子鸡","pickup":"3","mobile":"15230868184","content":"","shophead":"http://13.210.182.98/uploads/2d37b2e92b5ca8f9d11df90688c98e77.jpg","shopphoto":"http://13.210.182.98/uploads/05eab6d8954a842c2c611e4cf2e0dd7a.jpg","state":"1","states":"1","detailed_address":" Xingyuan Street, Yuhua, Shijiazhuang","coordinate":"114.5375765,38.0274239","gotime":" ","monday":"04:00-23:00","tuesday":"04:00-23:00","wednesday":"04:00-23:00","thursday":"04:00-23:00","friday":"04:00-23:00","saturday":"04:00-23:00","sunday":"04:00-23:00","lmoney":"2","long":"100","maxprice":"12","maxlong":"0.8","maxmoney":"1","lkmoney":"2","bkm":"","zhekou1":"0","zhekou2":"0","zhekou3":"20","zhekou4":"0","zhekou5":"0","zhekou6":"100","zhekou7":"0","flag":"1","addtime":"2017-12-06 16:51:17","orderid":"152308681851512541276","uid":"17","yid":null,"ycodema":null,"ymoney":null,"money":"0.00","allprice":"9004.00","cometime":"ASAP","endtime":null,"name":"张鑫","sex":"1","phone":"15230868185","address":"2018 万达广场3号公寓","beizhu":"","statu":"2","juli":"0.78227","pay":"3","todaynums":"1","goodsinfo":null,"lifetime":"2017-12-06 17:06:17","updatetime":"0000-00-00 00:00:00","youhuiall":"2251.00","goods":[{"orid":"114","orderid":"152308681851512541276","goodsname":"好运姐 厄运小姐","goodsphoto":"uploads/43a0d77ef8602552a1b920bc48eca3d4.jpg","zprice":"96","guigename":"泳池派对","pcname":"哦了,aaa","num":"1"},{"orid":"115","orderid":"152308681851512541276","goodsname":"阿什顿发","goodsphoto":"uploads/1d9a967d1892ecb7ea3a57f820a4f877.jpg","zprice":"106","guigename":null,"pcname":"","num":"1"},{"orid":"116","orderid":"152308681851512541276","goodsname":"阿什顿发","goodsphoto":"uploads/1d9a967d1892ecb7ea3a57f820a4f877.jpg","zprice":"9004","guigename":null,"pcname":"阿什顿发","num":"1"}]}]
     */

    private int status;
    private List<MsgBean> msg;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<MsgBean> getMsg() {
        return msg;
    }

    public void setMsg(List<MsgBean> msg) {
        this.msg = msg;
    }

    public static class MsgBean {
        /**
         * shopid : 45
         * shopname : 四川辣子鸡
         * pickup : 3
         * mobile : 15230868184
         * content :
         * shophead : http://13.210.182.98/uploads/2d37b2e92b5ca8f9d11df90688c98e77.jpg
         * shopphoto : http://13.210.182.98/uploads/05eab6d8954a842c2c611e4cf2e0dd7a.jpg
         * state : 1
         * states : 1
         * detailed_address :  Xingyuan Street, Yuhua, Shijiazhuang
         * coordinate : 114.5375765,38.0274239
         * gotime :
         * monday : 04:00-23:00
         * tuesday : 04:00-23:00
         * wednesday : 04:00-23:00
         * thursday : 04:00-23:00
         * friday : 04:00-23:00
         * saturday : 04:00-23:00
         * sunday : 04:00-23:00
         * lmoney : 2
         * long : 100
         * maxprice : 12
         * maxlong : 0.8
         * maxmoney : 1
         * lkmoney : 2
         * bkm :
         * zhekou1 : 0
         * zhekou2 : 0
         * zhekou3 : 20
         * zhekou4 : 0
         * zhekou5 : 0
         * zhekou6 : 100
         * zhekou7 : 0
         * flag : 1
         * addtime : 2017-12-06 16:51:17
         * orderid : 152308681851512541276
         * uid : 17
         * yid : null
         * ycodema : null
         * ymoney : null
         * money : 0.00
         * allprice : 9004.00
         * cometime : ASAP
         * endtime : null
         * name : 张鑫
         * sex : 1
         * phone : 15230868185
         * address : 2018 万达广场3号公寓
         * beizhu :
         * statu : 2
         * juli : 0.78227
         * pay : 3
         * todaynums : 1
         * goodsinfo : null
         * lifetime : 2017-12-06 17:06:17
         * updatetime : 0000-00-00 00:00:00
         * youhuiall : 2251.00
         * goods : [{"orid":"114","orderid":"152308681851512541276","goodsname":"好运姐 厄运小姐","goodsphoto":"uploads/43a0d77ef8602552a1b920bc48eca3d4.jpg","zprice":"96","guigename":"泳池派对","pcname":"哦了,aaa","num":"1"},{"orid":"115","orderid":"152308681851512541276","goodsname":"阿什顿发","goodsphoto":"uploads/1d9a967d1892ecb7ea3a57f820a4f877.jpg","zprice":"106","guigename":null,"pcname":"","num":"1"},{"orid":"116","orderid":"152308681851512541276","goodsname":"阿什顿发","goodsphoto":"uploads/1d9a967d1892ecb7ea3a57f820a4f877.jpg","zprice":"9004","guigename":null,"pcname":"阿什顿发","num":"1"}]
         */

        private String shopid;
        private String shopname;
        private String pickup;
        private String mobile;
        private String content;
        private String shophead;
        private String shopphoto;
        private String state;
        private String states;
        private String detailed_address;
        private String coordinate;
        private String gotime;
        private String monday;
        private String tuesday;
        private String wednesday;
        private String thursday;
        private String friday;
        private String saturday;
        private String sunday;
        private String lmoney;
        @SerializedName("long")
        private String longX;
        private String maxprice;
        private String maxlong;
        private String maxmoney;
        private String lkmoney;
        private String bkm;
        private String zhekou1;
        private String zhekou2;
        private String zhekou3;
        private String zhekou4;
        private String zhekou5;
        private String zhekou6;
        private String zhekou7;
        private String flag;
        private String addtime;
        private String orderid;
        private String uid;
        private Object yid;
        private Object ycodema;
        private Object ymoney;
        private String money;
        private String allprice;
        private String cometime;
        private Object endtime;
        private String name;
        private String sex;
        private String phone;
        private String address;
        private String beizhu;
        private String statu;
        private double juli;
        private String pay;
        private String todaynums;
        private Object goodsinfo;
        private String lifetime;
        private String updatetime;
        private String youhuiall;
        private List<GoodsBean> goods;
        private String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getShopid() {
            return shopid;
        }

        public void setShopid(String shopid) {
            this.shopid = shopid;
        }

        public String getShopname() {
            return shopname;
        }

        public void setShopname(String shopname) {
            this.shopname = shopname;
        }

        public String getPickup() {
            return pickup;
        }

        public void setPickup(String pickup) {
            this.pickup = pickup;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getShophead() {
            return shophead;
        }

        public void setShophead(String shophead) {
            this.shophead = shophead;
        }

        public String getShopphoto() {
            return shopphoto;
        }

        public void setShopphoto(String shopphoto) {
            this.shopphoto = shopphoto;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getStates() {
            return states;
        }

        public void setStates(String states) {
            this.states = states;
        }

        public String getDetailed_address() {
            return detailed_address;
        }

        public void setDetailed_address(String detailed_address) {
            this.detailed_address = detailed_address;
        }

        public String getCoordinate() {
            return coordinate;
        }

        public void setCoordinate(String coordinate) {
            this.coordinate = coordinate;
        }

        public String getGotime() {
            return gotime;
        }

        public void setGotime(String gotime) {
            this.gotime = gotime;
        }

        public String getMonday() {
            return monday;
        }

        public void setMonday(String monday) {
            this.monday = monday;
        }

        public String getTuesday() {
            return tuesday;
        }

        public void setTuesday(String tuesday) {
            this.tuesday = tuesday;
        }

        public String getWednesday() {
            return wednesday;
        }

        public void setWednesday(String wednesday) {
            this.wednesday = wednesday;
        }

        public String getThursday() {
            return thursday;
        }

        public void setThursday(String thursday) {
            this.thursday = thursday;
        }

        public String getFriday() {
            return friday;
        }

        public void setFriday(String friday) {
            this.friday = friday;
        }

        public String getSaturday() {
            return saturday;
        }

        public void setSaturday(String saturday) {
            this.saturday = saturday;
        }

        public String getSunday() {
            return sunday;
        }

        public void setSunday(String sunday) {
            this.sunday = sunday;
        }

        public String getLmoney() {
            return lmoney;
        }

        public void setLmoney(String lmoney) {
            this.lmoney = lmoney;
        }

        public String getLongX() {
            return longX;
        }

        public void setLongX(String longX) {
            this.longX = longX;
        }

        public String getMaxprice() {
            return maxprice;
        }

        public void setMaxprice(String maxprice) {
            this.maxprice = maxprice;
        }

        public String getMaxlong() {
            return maxlong;
        }

        public void setMaxlong(String maxlong) {
            this.maxlong = maxlong;
        }

        public String getMaxmoney() {
            return maxmoney;
        }

        public void setMaxmoney(String maxmoney) {
            this.maxmoney = maxmoney;
        }

        public String getLkmoney() {
            return lkmoney;
        }

        public void setLkmoney(String lkmoney) {
            this.lkmoney = lkmoney;
        }

        public String getBkm() {
            return bkm;
        }

        public void setBkm(String bkm) {
            this.bkm = bkm;
        }

        public String getZhekou1() {
            return zhekou1;
        }

        public void setZhekou1(String zhekou1) {
            this.zhekou1 = zhekou1;
        }

        public String getZhekou2() {
            return zhekou2;
        }

        public void setZhekou2(String zhekou2) {
            this.zhekou2 = zhekou2;
        }

        public String getZhekou3() {
            return zhekou3;
        }

        public void setZhekou3(String zhekou3) {
            this.zhekou3 = zhekou3;
        }

        public String getZhekou4() {
            return zhekou4;
        }

        public void setZhekou4(String zhekou4) {
            this.zhekou4 = zhekou4;
        }

        public String getZhekou5() {
            return zhekou5;
        }

        public void setZhekou5(String zhekou5) {
            this.zhekou5 = zhekou5;
        }

        public String getZhekou6() {
            return zhekou6;
        }

        public void setZhekou6(String zhekou6) {
            this.zhekou6 = zhekou6;
        }

        public String getZhekou7() {
            return zhekou7;
        }

        public void setZhekou7(String zhekou7) {
            this.zhekou7 = zhekou7;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getAddtime() {
            return addtime;
        }

        public void setAddtime(String addtime) {
            this.addtime = addtime;
        }

        public String getOrderid() {
            return orderid;
        }

        public void setOrderid(String orderid) {
            this.orderid = orderid;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public Object getYid() {
            return yid;
        }

        public void setYid(Object yid) {
            this.yid = yid;
        }

        public Object getYcodema() {
            return ycodema;
        }

        public void setYcodema(Object ycodema) {
            this.ycodema = ycodema;
        }

        public Object getYmoney() {
            return ymoney;
        }

        public void setYmoney(Object ymoney) {
            this.ymoney = ymoney;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getAllprice() {
            return allprice;
        }

        public void setAllprice(String allprice) {
            this.allprice = allprice;
        }

        public String getCometime() {
            return cometime;
        }

        public void setCometime(String cometime) {
            this.cometime = cometime;
        }

        public Object getEndtime() {
            return endtime;
        }

        public void setEndtime(Object endtime) {
            this.endtime = endtime;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getBeizhu() {
            return beizhu;
        }

        public void setBeizhu(String beizhu) {
            this.beizhu = beizhu;
        }

        public String getStatu() {
            return statu;
        }

        public void setStatu(String statu) {
            this.statu = statu;
        }

        public double getJuli() {
            return juli;
        }

        public void setJuli(double juli) {
            this.juli = juli;
        }

        public String getPay() {
            return pay;
        }

        public void setPay(String pay) {
            this.pay = pay;
        }

        public String getTodaynums() {
            return todaynums;
        }

        public void setTodaynums(String todaynums) {
            this.todaynums = todaynums;
        }

        public Object getGoodsinfo() {
            return goodsinfo;
        }

        public void setGoodsinfo(Object goodsinfo) {
            this.goodsinfo = goodsinfo;
        }

        public String getLifetime() {
            return lifetime;
        }

        public void setLifetime(String lifetime) {
            this.lifetime = lifetime;
        }

        public String getUpdatetime() {
            return updatetime;
        }

        public void setUpdatetime(String updatetime) {
            this.updatetime = updatetime;
        }

        public String getYouhuiall() {
            return youhuiall;
        }

        public void setYouhuiall(String youhuiall) {
            this.youhuiall = youhuiall;
        }

        public List<GoodsBean> getGoods() {
            return goods;
        }

        public void setGoods(List<GoodsBean> goods) {
            this.goods = goods;
        }

        public static class GoodsBean {
            /**
             * orid : 114
             * orderid : 152308681851512541276
             * goodsname : 好运姐 厄运小姐
             * goodsphoto : uploads/43a0d77ef8602552a1b920bc48eca3d4.jpg
             * zprice : 96
             * guigename : 泳池派对
             * pcname : 哦了,aaa
             * num : 1
             */

            private String orid;
            private String orderid;
            private String goodsname;
            private String goodsphoto;
            private String zprice;
            private String guigename;
            private String pcname;
            private String num;

            public String getOrid() {
                return orid;
            }

            public void setOrid(String orid) {
                this.orid = orid;
            }

            public String getOrderid() {
                return orderid;
            }

            public void setOrderid(String orderid) {
                this.orderid = orderid;
            }

            public String getGoodsname() {
                return goodsname;
            }

            public void setGoodsname(String goodsname) {
                this.goodsname = goodsname;
            }

            public String getGoodsphoto() {
                return goodsphoto;
            }

            public void setGoodsphoto(String goodsphoto) {
                this.goodsphoto = goodsphoto;
            }

            public String getZprice() {
                return zprice;
            }

            public void setZprice(String zprice) {
                this.zprice = zprice;
            }

            public String getGuigename() {
                return guigename;
            }

            public void setGuigename(String guigename) {
                this.guigename = guigename;
            }

            public String getPcname() {
                return pcname;
            }

            public void setPcname(String pcname) {
                this.pcname = pcname;
            }

            public String getNum() {
                return num;
            }

            public void setNum(String num) {
                this.num = num;
            }
        }
    }
}
