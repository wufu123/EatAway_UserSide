package com.australia.administrator.australiandelivery.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.australia.administrator.australiandelivery.MyApplication;
import com.australia.administrator.australiandelivery.R;
import com.australia.administrator.australiandelivery.adapter.MainPageAdapter;
import com.australia.administrator.australiandelivery.bean.ShopBean;
import com.australia.administrator.australiandelivery.comm.BaseActivity;
import com.australia.administrator.australiandelivery.fragment.LeftFragment;
import com.australia.administrator.australiandelivery.loadmore.LoadMoreWrapper;
import com.australia.administrator.australiandelivery.utils.Contants;
import com.australia.administrator.australiandelivery.utils.HttpUtils;
import com.australia.administrator.australiandelivery.utils.MyClassOnclik;
import com.australia.administrator.australiandelivery.utils.ToastUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.gson.Gson;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuRecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

import static com.australia.administrator.australiandelivery.MyApplication.isNeedAutoLocate;

public class MainActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener
        , GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
    private final int MAX_SERVER_INFO = 15;
    private final int SELECT_ADDRESS_REQUEST = 10086;
    private final int SELECT_ADDRESS_RESULT = 10087;
    @Bind(R.id.iv_waimai)
    public ImageView ivWaimai;
    @Bind(R.id.tv_waimai)
    public TextView tvWaimai;
    @Bind(R.id.ll_waimai)
    LinearLayout llWaimai;
    @Bind(R.id.iv_ziqu)
    public ImageView ivZiqu;
    @Bind(R.id.tv_ziqu)
    public TextView tvZiqu;
    @Bind(R.id.ll_ziqu)
    LinearLayout llZiqu;
    private GoogleApiClient mGoogleApiClient;
    private boolean mLocationPermissionGranted;
    private Location mLastKnownLocation;
    //1：外卖  2：自取   3：外卖+自取
    private int pickup = 1;

    @Bind(R.id.iv_left_main)
    ImageView ivLeftMain;
    @Bind(R.id.iv_right)
    ImageView ivRight;
//    @Bind(R.id.sort_default)
//    public TextView sortDefault;
//    @Bind(R.id.sort_recommend)
//    public TextView sortRecommend;
//    @Bind(R.id.sort_distance)
//    public TextView sortDistance;
//    @Bind(R.id.sort_sale)
//    public TextView sortSale;
    @Bind(R.id.rv_main)
    SwipeMenuRecyclerView rvMain;
    @Bind(R.id.content_frame)
    FrameLayout contentFrame;
    @Bind(R.id.lv_sortbar)
    LinearLayout lvSortbar;
    @Bind(R.id.rl_search_bar)
    RelativeLayout rlSearchBar;
    @Bind(R.id.sr_main)
    SwipeRefreshLayout srMain;

    private LoadMoreWrapper mLoadMoreWrapper;
    private MainPageAdapter mainPageAdapter;
    public SlidingMenu menu;
    private View.OnClickListener listener;
    private int scrollOffset = 0;
    private ShopBean shopBean;

    //当前页页码：
    private int currentPage = 1;
    private boolean isScroll = true;
    private int num = 0;

    private LinearLayoutManager manager;
    private final int REQUEST_CODE = 10086;

    private SwipeMenuRecyclerView.LoadMoreListener moreListener;
    private static final int REFRESH_COMPLETE = 0X110;
    private boolean iSDestroy = false;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case REFRESH_COMPLETE:
                    if (iSDestroy) {
                        return;
                    }
                    currentPage = 1;
                    loadData();
                    break;

            }
        }
    };

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initDate() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        //初始化页面
        initMainRecylerView();
        initClickListener();
        initSlidingMenu();
        srMain.setOnRefreshListener(this);
        srMain.setColorScheme(android.R.color.holo_green_light);
        srMain.setRefreshing(true);
        ivRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MyApplication.getLogin() != null) {
//                    Intent i = new Intent(MainActivity.this, AddressActivity.class);
                    Intent i = new Intent(MainActivity.this, SelectAddressActivity.class);
                    i.putExtra("type", "3");
                    startActivityForResult(i, SELECT_ADDRESS_REQUEST);
                } else {
                    goToActivity(LoginActivity.class);
                }
            }
        });
        getDeviceLocation();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_ADDRESS_REQUEST && resultCode == SELECT_ADDRESS_RESULT) {
            currentPage = 1;
            //地址不会再重新自动定位
            isNeedAutoLocate = false;
            loadData();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        currentPage = 1;
//        loadData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        iSDestroy = true;
    }

    @Override
    public void onRefresh() {
        mHandler.sendEmptyMessageDelayed(REFRESH_COMPLETE, 2000);
    }

    private void getDeviceLocation() {
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            //如果有了权限
            initGoogleApi();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.RECORD_AUDIO},
                    REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                Log.i("main_acitivity", "onRequestPermissionsResult: ");
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initGoogleApi();
                } else {
                    ToastUtils.showToast(R.string.huo_qu_lu_yin_quan_xian, this);
                    loadData();
                }
            }
        }
    }

    /**
     * 初始化谷歌定位
     */
    private void initGoogleApi() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */,
                        this /* OnConnectionFailedListener */)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        mGoogleApiClient.connect();
    }

    /**
     * 初始化函数
     */
    private void initSlidingMenu() {
        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        // 设置触摸屏幕的模式
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(R.color.colorAccent);

        // 设置滑动菜单视图的宽度
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        // 设置渐入渐出效果的值
        menu.setFadeDegree(0.35f);
//        if (mainPageAdapter.banner != null) menu.addIgnoredView(mainPageAdapter.banner);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT, false);
        //为侧滑菜单设置布局
        menu.setMenu(R.layout.lv_left_popwindow);
        getSupportFragmentManager().beginTransaction().replace(R.id.left_frame, new LeftFragment()).commit();
    }

    private void initMainRecylerView() {
        manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        manager.setAutoMeasureEnabled(true);
        rvMain.setLayoutManager(manager);
        mainPageAdapter = new MainPageAdapter(this, shopBean, new MyClassOnclik() {
            @Override
            public void setRefreshing(int pickup, int page) {
                MainActivity.this.pickup = pickup;
                MainActivity.this.currentPage = page;
                loadData();
            }
        });
        //上拉加载把你用的adapter传进去
        mLoadMoreWrapper = new LoadMoreWrapper(this, mainPageAdapter);
        mLoadMoreWrapper.setOnLoadListener(new LoadMoreWrapper.OnLoadListener() {
            @Override
            public void onRetry() {
                //重试处理
                if (isNeedAutoLocate) mGoogleApiClient.reconnect();
                else {
                    loadData();
                }
            }

            @Override
            public void onLoadMore() {
                if (shopBean == null || shopBean.getMsg().size() < 15) {
                    mLoadMoreWrapper.showLoadComplete();
                } else {
                    //加载更多
                    loadData();
                }
            }
        });
        rvMain.setAdapter(mLoadMoreWrapper);
        mLoadMoreWrapper.showLoadComplete();
        rvMain.addOnScrollListener(new EndLoadOnScrollListener());
    }

    private void initClickListener() {
        rlSearchBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(SearchActivity.class);
            }
        });
        listener = mainPageAdapter.getSortBarListener();
//        sortDefault.setOnClickListener(listener);
//        sortDistance.setOnClickListener(listener);
//        sortRecommend.setOnClickListener(listener);
//        sortSale.setOnClickListener(listener);
        llWaimai.setOnClickListener(listener);
        llZiqu.setOnClickListener(listener);
    }

    /**
     * 辅助函数
     */
    private void loadData() {
        if (currentPage == 1) srMain.setRefreshing(true);
        HttpUtils http = new HttpUtils(Contants.URL_SHOPLIST) {
            @Override
            public void onError(Call call, Exception e, int id) {
                hidDialog();
                Log.i("main_acitivity", "onError: " + e.getMessage());
                srMain.setRefreshing(false);
                mLoadMoreWrapper.showLoadError();
                Toast.makeText(MainActivity.this, R.string.please_check_your_network_connection, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(String response, int id) {
                Log.i("main_acitivity", "onResponse: " + response);
                hidDialog();
                srMain.setRefreshing(false);
                mLoadMoreWrapper.showLoadComplete();
                try {
                    JSONObject o = new JSONObject(response);
                    int status = o.getInt("status");
                    if (status == 1) {
                        shopBean = new Gson().fromJson(response, ShopBean.class);
                        if (currentPage > 1) {
                            mainPageAdapter.addBean(shopBean);
                            mainPageAdapter.notifyDataSetChanged();
                        } else {
                            mainPageAdapter.setShopBean(shopBean, pickup);
                        }
                        mLoadMoreWrapper.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        http.addParam("pickup", pickup+"");
        http.addParam("page", currentPage + "")
                .addParams("num",  "2")
                .addParams("coor", MyApplication.mLocation.longitude + "," + MyApplication.mLocation.latitude);
        Log.i("main_acitivity", "loadData: " + MyApplication.mLocation.longitude + "," + MyApplication.mLocation.latitude);
//        if (num == 2 || num == 3) {
//            if (currentPage == 1) {
//                http.addParam("row", "0");
//            } else {
                http.addParam("row", (currentPage - 1) * 15 + "");
//            }
//        } else {
//            http.addParam("row", "");
//        }
        http.clicent();
    }

    /**
     * 辅助类
     */
    //监听滑动距离判断是否令首页样式改变
    private class EndLoadOnScrollListener extends RecyclerView.OnScrollListener {

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            //首页样式判断
//            scrollOffset += dy;
            Log.i("taa", "onScrolled: " + getScollYDistance() + " " + scrollOffset + " " + rvMain.getTop() + " " + recyclerView.computeVerticalScrollOffset());
            if (recyclerView.computeVerticalScrollOffset() >= mainPageAdapter.BannerHeight) {
                lvSortbar.setVisibility(View.VISIBLE);
                lvSortbar.bringToFront();
            } else {
                lvSortbar.setVisibility(View.GONE);
            }
        }
    }

    public int getScollYDistance() {
        int position = manager.findFirstVisibleItemPosition();
        View firstVisiableChildView = manager.findViewByPosition(position);
        int itemHeight = firstVisiableChildView.getHeight();
        return (position) * itemHeight - firstVisiableChildView.getTop();
    }

    @OnClick(R.id.iv_left_main)
    public void onClick() {
        menu.toggle();
    }

    private long exitTime = 0;

    /**
     * 再按一次退出程序（间隔2秒之后再按返回才会提示）
     */
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getAction() == KeyEvent.ACTION_DOWN) {
            if ((System.currentTimeMillis() - exitTime) > 2000) {
                if (MyApplication.isEnglish) {
                    Toast.makeText(this, "Press again to exit the program",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "再按一次退出程序",
                            Toast.LENGTH_SHORT).show();
                }
                exitTime = System.currentTimeMillis();
            } else {
                finish();
                System.exit(0);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationPermissionGranted = true;
        if (isNeedAutoLocate) {
            Log.i("main_acitivity", "onConnected: ");
            PendingResult<PlaceLikelihoodBuffer> result = Places.PlaceDetectionApi
                    .getCurrentPlace(mGoogleApiClient, null);
            result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
                @Override
                public void onResult(final PlaceLikelihoodBuffer likelyPlaces) {
                    Log.i("=========", "onConnected: ");
                    if (likelyPlaces.getCount() > 1) {
                        MyApplication.mLocation = likelyPlaces.get(0).getPlace().getLatLng();
                        loadData();
                    }
                    likelyPlaces.release();
                }
            });
        } else {
            loadData();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        ToastUtils.showToast(R.string.ding_wei_shi_bai, this);
        loadData();
    }
}
