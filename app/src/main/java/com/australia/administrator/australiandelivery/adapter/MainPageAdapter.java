package com.australia.administrator.australiandelivery.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.australia.administrator.australiandelivery.MyApplication;
import com.australia.administrator.australiandelivery.R;
import com.australia.administrator.australiandelivery.activity.GoodsListActivity1;
import com.australia.administrator.australiandelivery.activity.MainActivity;
import com.australia.administrator.australiandelivery.bean.ShopBean;
import com.australia.administrator.australiandelivery.utils.GlideUtils;
import com.australia.administrator.australiandelivery.utils.MyClassOnclik;
import com.australia.administrator.australiandelivery.utils.NumberFormatUtil;
import com.australia.administrator.australiandelivery.utils.ViewHeightCalculator;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.bingoogolapple.bgabanner.BGABanner;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Administrator on 2017/6/8.
 */

public class MainPageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<View> list = new ArrayList<>();
    private ShopBean mesBean;
    private ShopBean addBean;

    private SortBarClickListener listener = new SortBarClickListener();

    public int BannerHeight = 0;
    public int SortBarHeight = 0;
    public int ResItemHeight = 0;
    public BGABanner banner;
    private MyClassOnclik myClassOnclik;
    private SortBarHolder sortBarHolder;
    private int type = 0;   //0代表外卖，1代表自取

    public MainPageAdapter(Context context, ShopBean mesBean, MyClassOnclik myClassOnclik) {
        this.context = context;
        this.mesBean = mesBean;
        this.myClassOnclik = myClassOnclik;
    }

    public void addBean(ShopBean addBean) {
        for (int i = 0; i < addBean.getMsg().size(); i++) {
            mesBean.getMsg().add(addBean.getMsg().get(i));
        }
    }

    public ShopBean getShopBean() {
        return mesBean;
    }

    public void setShopBean(ShopBean shopBean, int type) {
        this.mesBean = shopBean;
        this.type = type;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        Log.i("tag", "position:" + "");
        if (viewType == 0) {
            view = LayoutInflater.from(context).inflate(R.layout.item_main_top_banner, parent, false);
            TopBannerHolder holder = new TopBannerHolder(view);
            BannerHeight = ViewHeightCalculator.ViewHeightCaluclate(view);
            banner = (BGABanner) view.findViewById(R.id.banner_main_top);
            ((MainActivity) context).menu.addIgnoredView(banner);
            holder.banner = banner;
            Log.i("banner", "height:" + BannerHeight);
            return holder;
        } else if (viewType == 1) {
            view = LayoutInflater.from(context).inflate(R.layout.item_main_sortbar_second, parent, false);
            sortBarHolder = new SortBarHolder(view);
            SortBarHeight = ViewHeightCalculator.ViewHeightCaluclate(view);
//            sortBarHolder.sortDefault = (TextView) view.findViewById(R.id.sort_default);
//            sortBarHolder.sortRecommend = (TextView) view.findViewById(R.id.sort_recommend);
//            sortBarHolder.sortDistance = (TextView) view.findViewById(R.id.sort_distance);
//            sortBarHolder.sortSale = (TextView) view.findViewById(R.id.sort_sale);
            return sortBarHolder;
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.item_main_restaurant, parent, false);
            ResItemHeight = ViewHeightCalculator.ViewHeightCaluclate(view);
            PageListHolder holder = new PageListHolder(view);
            return holder;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.i("tag", "position:" + position + "");
        if (position == 0) {
            if (mesBean != null && mesBean.getMsg1() != null && mesBean.getMsg1().size() != 0)
                initTopBanner((TopBannerHolder) holder);
        } else if (position == 1) {
            initSortBar((SortBarHolder) holder);
        } else {
            initResList((PageListHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {

        if (mesBean != null && mesBean.getMsg().size() != 0) {
            return mesBean.getMsg().size() + 2;
        } else {
            return 2;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /**
     * Holders
     */
    class TopBannerHolder extends RecyclerView.ViewHolder {

        public TopBannerHolder(View itemView) {
            super(itemView);
        }

        public BGABanner banner;
    }

    public class SortBarHolder extends RecyclerView.ViewHolder {
        ImageView ivWaimai;
        TextView tvWaimai;
        LinearLayout llWaimai;
        ImageView ivZiqu;
        TextView tvZiqu;
        LinearLayout llZiqu;
        public SortBarHolder(View itemView) {
            super(itemView);
            llZiqu = (LinearLayout) itemView.findViewById(R.id.ll_ziqu);
            llWaimai = (LinearLayout) itemView.findViewById(R.id.ll_waimai);
            ivZiqu = (ImageView) itemView.findViewById(R.id.iv_ziqu);
            ivWaimai = (ImageView) itemView.findViewById(R.id.iv_waimai);
            tvWaimai = (TextView) itemView.findViewById(R.id.tv_waimai);
            tvZiqu  = (TextView) itemView.findViewById(R.id.tv_ziqu);
        }

//        TextView sortDefault, sortRecommend, sortDistance, sortSale;
    }

    class PageListHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.iv_main_res_icon)
        CircleImageView ivMainResIcon;
        @Bind(R.id.line_vertical)
        View lineVertical;
        @Bind(R.id.tv_main_res_label_01)
        TextView tvMainResLabel01;
        @Bind(R.id.tv_main_res_label_02)
        TextView tvMainResLabel02;
        @Bind(R.id.tv_main_res_label_03)
        TextView tvMainResLabel03;
        @Bind(R.id.line_horizontal)
        View lineHorizontal;
        @Bind(R.id.img_main_res_img)
        ImageView imgMainResImg;
        @Bind(R.id.tv_res_main_name)
        TextView tvResMainName;
        @Bind(R.id.tv_res_main_distance)
        TextView tvResMainDistance;
        @Bind(R.id.ll_reslist_item)
        LinearLayout llReslistItem;
        @Bind(R.id.img_main_res_tag)
        ImageView imgMainResTag;
        @Bind(R.id.rl_shape)
        RelativeLayout rlShape;

        public PageListHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    /**
     * Holders初始化
     *
     * @param holder
     */
    private void initTopBanner(TopBannerHolder holder) {
        list.clear();
        for (int i = 0; i < mesBean.getMsg1().size(); i++) {
            ImageView imageView = new ImageView(context);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            GlideUtils.load(context, mesBean.getMsg1().get(i).getShopphoto(), imageView, GlideUtils.Shape.ShopPic);
            list.add(imageView);
        }
        holder.banner.setData(list);
        holder.banner.setDelegate(new BGABanner.Delegate() {
            @Override
            public void onBannerItemClick(BGABanner banner, View itemView, Object model, int position) {
                Intent intent = new Intent(context, GoodsListActivity1.class);
                intent.putExtra("id", mesBean.getMsg1().get(position).getShopid());
                intent.putExtra("flag", type);
                context.startActivity(intent);
            }
        });
    }

    private void initSortBar(SortBarHolder holder) {
//        holder.sortDefault.setOnClickListener(listener);
//        holder.sortDistance.setOnClickListener(listener);
//        holder.sortRecommend.setOnClickListener(listener);
//        holder.sortSale.setOnClickListener(listener);
        holder.llWaimai.setOnClickListener(listener);
        holder.llZiqu.setOnClickListener(listener);
    }

    private double getDouble(String str) {
        return TextUtils.isEmpty(str) ? 0 : Double.parseDouble(str);
    }

    private void initResList(final PageListHolder holder, final int position) {
        holder.tvResMainName.setText(mesBean.getMsg().get(position - 2).getShopname());
        if (type == 2) {
            if (mesBean.getMsg().get(position - 2).getJuli() < 1000) {
                holder.tvResMainDistance.setText(mesBean.getMsg().get(position - 2).getJuli() + "m");
            } else {
                holder.tvResMainDistance.setText(NumberFormatUtil.round(mesBean.getMsg().get(position - 2).getJuli() / 1000, 2) + "km");
            }
        } else {
            if (getDouble(mesBean.getMsg().get(position - 2).getLongX()) * 1000 < mesBean.getMsg().get(position - 2).getJuli()) {
                holder.tvResMainDistance.setText(R.string.chao_chu_pei_song);
            } else {
                if (mesBean.getMsg().get(position - 2).getJuli() < 1000) {
                    holder.tvResMainDistance.setText(mesBean.getMsg().get(position - 2).getJuli() + "m");
                } else {
                    holder.tvResMainDistance.setText(NumberFormatUtil.round(mesBean.getMsg().get(position - 2).getJuli() / 1000, 2) + "km");
                }
            }
        }
        GlideUtils.load(context, mesBean.getMsg().get(position - 2).getShophead(), holder.ivMainResIcon, GlideUtils.Shape.UserIcon);
        GlideUtils.load(context, mesBean.getMsg().get(position - 2).getShopphoto(), holder.imgMainResImg, GlideUtils.Shape.UserIcon);
        if (mesBean.getMsg().get(position - 2).getFlag().equals("1")) {
            //表示店铺正常
            if (mesBean.getMsg().get(position - 2).getState().equals("1")) {
                if (MyApplication.isEnglish) {
                    holder.imgMainResTag.setImageResource(R.drawable.img_main_res_tag_on_en);
                } else {
                    holder.imgMainResTag.setImageResource(R.drawable.img_main_res_tag_on);
                }
                holder.rlShape.setVisibility(View.GONE);
            } else {
                if (MyApplication.isEnglish) {
                    holder.imgMainResTag.setImageResource(R.drawable.img_main_res_tag_close_en);
                } else {
                    holder.imgMainResTag.setImageResource(R.drawable.img_main_res_tag_off);
                }
                holder.rlShape.setVisibility(View.VISIBLE);
            }
        } else {
            //表示店铺需要强制关闭
            if (MyApplication.isEnglish) {
                holder.imgMainResTag.setImageResource(R.drawable.img_main_res_tag_close_en);
            } else {
                holder.imgMainResTag.setImageResource(R.drawable.img_main_res_tag_off);
            }
            holder.rlShape.setVisibility(View.VISIBLE);
        }
//        holder.tvMainResLabel01.setVisibility(View.VISIBLE);
        if (mesBean.getMsg().get(position - 2).getWzhekou() != null && !"0".equals(mesBean.getMsg().get(position - 2).getWzhekou())
                && !"".equals(mesBean.getMsg().get(position - 2).getWzhekou())) {
            if (MyApplication.isEnglish) {
                holder.tvMainResLabel01.setText("Discount " + mesBean.getMsg().get(position - 2).getWzhekou() + "%");
            } else {
                holder.tvMainResLabel01.setText("折扣" + mesBean.getMsg().get(position - 2).getWzhekou() + "%");
            }
        } else {
            if (MyApplication.isEnglish) {
                holder.tvMainResLabel01.setText("No Discount");
            } else {
                holder.tvMainResLabel01.setText("今日无抵扣");
            }
        }

        holder.llReslistItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, GoodsListActivity1.class);
                intent.putExtra("id", mesBean.getMsg().get(position - 2).getShopid());
                intent.putExtra("flag", type);
                context.startActivity(intent);
            }
        });
    }

    /**
     * 辅助类
     */

    private class SortBarClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
//                case R.id.sort_default:
//                    sortBarHolder.sortDefault.setTextColor(context.getResources().getColor(R.color.text_black));
//                    sortBarHolder.sortDistance.setTextColor(context.getResources().getColor(R.color.list_line));
//                    sortBarHolder.sortRecommend.setTextColor(context.getResources().getColor(R.color.list_line));
//                    sortBarHolder.sortSale.setTextColor(context.getResources().getColor(R.color.list_line));
//                    ((MainActivity) context).sortDefault.setTextColor(context.getResources().getColor(R.color.text_black));
//                    ((MainActivity) context).sortDistance.setTextColor(context.getResources().getColor(R.color.list_line));
//                    ((MainActivity) context).sortRecommend.setTextColor(context.getResources().getColor(R.color.list_line));
//                    ((MainActivity) context).sortSale.setTextColor(context.getResources().getColor(R.color.list_line));
//                    myClassOnclik.setRefreshing(0, 1);
//                    break;
//                case R.id.sort_recommend:
//                    sortBarHolder.sortRecommend.setTextColor(context.getResources().getColor(R.color.text_black));
//                    sortBarHolder.sortDistance.setTextColor(context.getResources().getColor(R.color.list_line));
//                    sortBarHolder.sortDefault.setTextColor(context.getResources().getColor(R.color.list_line));
//                    sortBarHolder.sortSale.setTextColor(context.getResources().getColor(R.color.list_line));
//                    ((MainActivity) context).sortDefault.setTextColor(context.getResources().getColor(R.color.list_line));
//                    ((MainActivity) context).sortDistance.setTextColor(context.getResources().getColor(R.color.list_line));
//                    ((MainActivity) context).sortRecommend.setTextColor(context.getResources().getColor(R.color.text_black));
//                    ((MainActivity) context).sortSale.setTextColor(context.getResources().getColor(R.color.list_line));
//                    myClassOnclik.setRefreshing(1, 1);
//                    break;
//                case R.id.sort_distance:
//                    sortBarHolder.sortDistance.setTextColor(context.getResources().getColor(R.color.text_black));
//                    sortBarHolder.sortDefault.setTextColor(context.getResources().getColor(R.color.list_line));
//                    sortBarHolder.sortSale.setTextColor(context.getResources().getColor(R.color.list_line));
//                    sortBarHolder.sortRecommend.setTextColor(context.getResources().getColor(R.color.list_line));
//                    ((MainActivity) context).sortDefault.setTextColor(context.getResources().getColor(R.color.list_line));
//                    ((MainActivity) context).sortDistance.setTextColor(context.getResources().getColor(R.color.text_black));
//                    ((MainActivity) context).sortRecommend.setTextColor(context.getResources().getColor(R.color.list_line));
//                    ((MainActivity) context).sortSale.setTextColor(context.getResources().getColor(R.color.list_line));
//                    myClassOnclik.setRefreshing(2, 1);
//                    break;
//                case R.id.sort_sale:
//                    sortBarHolder.sortSale.setTextColor(context.getResources().getColor(R.color.text_black));
//                    sortBarHolder.sortDistance.setTextColor(context.getResources().getColor(R.color.list_line));
//                    sortBarHolder.sortDefault.setTextColor(context.getResources().getColor(R.color.list_line));
//                    sortBarHolder.sortRecommend.setTextColor(context.getResources().getColor(R.color.list_line));
//                    ((MainActivity) context).sortDefault.setTextColor(context.getResources().getColor(R.color.list_line));
//                    ((MainActivity) context).sortDistance.setTextColor(context.getResources().getColor(R.color.list_line));
//                    ((MainActivity) context).sortRecommend.setTextColor(context.getResources().getColor(R.color.list_line));
//                    ((MainActivity) context).sortSale.setTextColor(context.getResources().getColor(R.color.text_black));
//                    myClassOnclik.setRefreshing(3, 1);
//                    break;
                case R.id.ll_waimai:
                    sortBarHolder.tvWaimai.setTextColor(context.getResources().getColor(R.color.actionBar));
                    sortBarHolder.tvZiqu.setTextColor(context.getResources().getColor(R.color.list_line));
                    sortBarHolder.ivWaimai.setImageResource(R.mipmap.icon_waimai);
                    sortBarHolder.ivZiqu.setImageResource(R.mipmap.icon_ziqu_gray);
                    ((MainActivity) context).tvWaimai.setTextColor(context.getResources().getColor(R.color.actionBar));
                    ((MainActivity) context).tvZiqu.setTextColor(context.getResources().getColor(R.color.list_line));
                    ((MainActivity) context).ivWaimai.setImageResource(R.mipmap.icon_waimai);
                    ((MainActivity) context).ivZiqu.setImageResource(R.mipmap.icon_ziqu_gray);
                    myClassOnclik.setRefreshing(1, 1);
                    break;
                case R.id.ll_ziqu:
                    sortBarHolder.tvWaimai.setTextColor(context.getResources().getColor(R.color.list_line));
                    sortBarHolder.tvZiqu.setTextColor(context.getResources().getColor(R.color.actionBar));
                    sortBarHolder.ivWaimai.setImageResource(R.mipmap.icon_waimai_gray);
                    sortBarHolder.ivZiqu.setImageResource(R.mipmap.icon_ziqu);
                    ((MainActivity) context).tvWaimai.setTextColor(context.getResources().getColor(R.color.list_line));
                    ((MainActivity) context).tvZiqu.setTextColor(context.getResources().getColor(R.color.actionBar));
                    ((MainActivity) context).ivWaimai.setImageResource(R.mipmap.icon_waimai_gray);
                    ((MainActivity) context).ivZiqu.setImageResource(R.mipmap.icon_ziqu);
                    myClassOnclik.setRefreshing(2, 1);
                    break;
            }
        }
    }

    public View.OnClickListener getSortBarListener() {
        return this.listener;
    }

    public static class ShopDistanceBean {

        /**
         * destination_addresses : ["1 Parker St, Mile End SA 5031, Australia"]
         * origin_addresses : ["251 North Terrace, Adelaide SA 5000, Australia"]
         * rows : [{"elements":[{"distance":{"text":"2.0 mi","value":3268},"duration":{"text":"13 mins","value":752},"status":"OK"}]}]
         * status : OK
         */

        private String status;
        private List<String> destination_addresses;
        private List<String> origin_addresses;
        private List<RowsBean> rows;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<String> getDestination_addresses() {
            return destination_addresses;
        }

        public void setDestination_addresses(List<String> destination_addresses) {
            this.destination_addresses = destination_addresses;
        }

        public List<String> getOrigin_addresses() {
            return origin_addresses;
        }

        public void setOrigin_addresses(List<String> origin_addresses) {
            this.origin_addresses = origin_addresses;
        }

        public List<RowsBean> getRows() {
            return rows;
        }

        public void setRows(List<RowsBean> rows) {
            this.rows = rows;
        }

        public static class RowsBean {
            private List<ElementsBean> elements;

            public List<ElementsBean> getElements() {
                return elements;
            }

            public void setElements(List<ElementsBean> elements) {
                this.elements = elements;
            }

            public static class ElementsBean {
                /**
                 * distance : {"text":"2.0 mi","value":3268}
                 * duration : {"text":"13 mins","value":752}
                 * status : OK
                 */

                private DistanceBean distance;
                private DurationBean duration;
                private String status;

                public DistanceBean getDistance() {
                    return distance;
                }

                public void setDistance(DistanceBean distance) {
                    this.distance = distance;
                }

                public DurationBean getDuration() {
                    return duration;
                }

                public void setDuration(DurationBean duration) {
                    this.duration = duration;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public static class DistanceBean {
                    /**
                     * text : 2.0 mi
                     * value : 3268
                     */

                    private String text;
                    private int value;

                    public String getText() {
                        return text;
                    }

                    public void setText(String text) {
                        this.text = text;
                    }

                    public int getValue() {
                        return value;
                    }

                    public void setValue(int value) {
                        this.value = value;
                    }
                }

                public static class DurationBean {
                    /**
                     * text : 13 mins
                     * value : 752
                     */

                    private String text;
                    private int value;

                    public String getText() {
                        return text;
                    }

                    public void setText(String text) {
                        this.text = text;
                    }

                    public int getValue() {
                        return value;
                    }

                    public void setValue(int value) {
                        this.value = value;
                    }
                }
            }
        }
    }

}
