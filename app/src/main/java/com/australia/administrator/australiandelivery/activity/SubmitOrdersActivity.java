package com.australia.administrator.australiandelivery.activity;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.australia.administrator.australiandelivery.MyApplication;
import com.australia.administrator.australiandelivery.R;
import com.australia.administrator.australiandelivery.adapter.OrdersAdapterSecond;
import com.australia.administrator.australiandelivery.alipay.PayResult;
import com.australia.administrator.australiandelivery.bean.GoodsBean;
import com.australia.administrator.australiandelivery.utils.Contants;
import com.australia.administrator.australiandelivery.utils.GlideUtils;
import com.australia.administrator.australiandelivery.utils.HttpUtils;
import com.australia.administrator.australiandelivery.utils.NumberFormatUtil;
import com.australia.administrator.australiandelivery.view.TopBar;
import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.dropin.utils.PaymentMethodType;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;

import static com.australia.administrator.australiandelivery.utils.Contants.URL_ALIPAY_CANCLE;

/**
 * Created by Administrator on 2017/7/8.
 */

public class SubmitOrdersActivity extends com.australia.administrator.australiandelivery.comm.BaseActivity {
    private final int BANK = 1, ALIPAY = 2, WECHAT = 3;
    private final int DISCOUNT_REQUEST_CODE = 1110, DISCOUNT_RESULT_CODE = 1111;
    public static final int ORDER_RESULT = 1001;
    private final int BEIZHU_REQUEST_CODE = 7;
    @Bind(R.id.tb_submit_orders)
    TopBar tbSubmitOrders;
    @Bind(R.id.tv_weizhi)
    TextView tvWeizhi;
    @Bind(R.id.tv_submit_name)
    TextView tvSubmitName;
    @Bind(R.id.tv_submit_sex)
    TextView tvSubmitSex;
    @Bind(R.id.tv_submit_phone)
    TextView tvSubmitPhone;
    @Bind(R.id.iv_orders_shop)
    CircleImageView ivOrdersShop;
    @Bind(R.id.tv_orders_shop_name)
    TextView tvOrdersShopName;
    @Bind(R.id.rv_orders)
    RecyclerView rvOrders;
    @Bind(R.id.tv_orders_delivery)
    TextView tvOrdersDelivery;
    @Bind(R.id.tv_orders_dress)
    TextView tvOrdersDress;
    @Bind(R.id.tv_orders_money)
    TextView tvOrdersMoney;
    @Bind(R.id.tv_orders_taste_notes)
    TextView tvOrdersTasteNotes;
    @Bind(R.id.tv_orders_method_of_payment)
    TextView tvOrdersMethodOfPayment;
    @Bind(R.id.shopping_cart)
    TextView shoppingCart;
    @Bind(R.id.shoppingPrise)
    TextView shoppingPrise;
    @Bind(R.id.settlement)
    TextView settlement;
    @Bind(R.id.toolBar)
    LinearLayout toolBar;
    @Bind(R.id.ll_weizhi)
    LinearLayout llWeizhi;
    @Bind(R.id.tv_orders_time)
    TextView tvOrdersTime;
    @Bind(R.id.rl_orders_pay)
    RelativeLayout rlOrdersPay;
    @Bind(R.id.list_line)
    View listLine;
    @Bind(R.id.tv_orders_discount)
    TextView tvOrdersDiscount;
    @Bind(R.id.tv_orders_get_discount)
    TextView tvOrdersGetDiscount;
    @Bind(R.id.tv_run_time)
    TextView tvRunTime;
    private String mAuthorization;
    private int hour;
    private int minute;
    private List<GoodsBean> goods;
    private OrdersAdapterSecond adapter;
    private String name;
    private String phone;
    private String sex;
    private String weizhi;
    private String id;
    private String coordinate;
    private String shopid;
    private double price1;
    private TimePickerDialog dialog;

    private String format1 = "0";
    private String am = "am";
    private int pay = 5;//支付方式：4货到付款3银行卡、2支付宝、1微信
    private boolean canOrder = false;
    private String juli;
    private String yujitime = "";

    private String orderid;

    //一千米多少钱
    private double feeForLk;
    //超过多少钱时免费
    private double freeMoney;
    //多少千米多少钱
    private double feeForKm;
    private double feeForMoney;

    //商家的最大配送距离
    private double maxX;
    //实际计算的距离
    private double peisong;
    //计算的配送费
    private double psmoney;
    //计算的总价格
    private double calmoney;

    //优惠码id
    private String yid = "";
    //优惠码对应折扣金额
    private double ymoney = 0;
    //优惠码金额
    private double discountNum;
    //优惠码
    private String ycode;
    //优惠码类型：1百分比 2折扣金额
    private int discountState;
    //优惠码是否使用过
    private boolean isUsedDiscout = false;
    //原有价格
    private double prePrice = 0;
    //折扣价格
    private double discount = 0;

    private boolean isGotPsmoney = false;

    //新增，此页无用，最低起送价格
    private String minprice;

    private String nonce;

    //支付宝支付相关：
    // 商户PID
    public static final String PARTNER = "2088721824853457";
    // 商户收款账号
    public static final String SELLER = "2088721824853457";
    // 商户私钥，pkcs8格式
    public static final String RSA_PRIVATE = "";

    private static final int SDK_PAY_FLAG = 1;

    private static String order_num = "";

    private String sign = "";

    private String orderInfo = "";

    private String time="";

    private Handler mHandler = new Handler() {
        @SuppressWarnings("unused")
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG: {
                    PayResult payResult = new PayResult((String) msg.obj);
                    /**
                     * 同步返回的结果必须放置到服务端进行验证（验证的规则请看https://doc.open.alipay.com/doc2/
                     * detail.htm?spm=0.0.0.0.xdvAU6&treeId=59&articleId=103665&
                     * docType=1) 建议商户依赖异步通知
                     */
                    String resultInfo = payResult.getResult();// 同步返回需要验证的信息

                    String resultStatus = payResult.getResultStatus();
                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                    if (TextUtils.equals(resultStatus, "9000")) {
                        Toast.makeText(SubmitOrdersActivity.this, R.string.xia_dan_cheng_gong, Toast.LENGTH_SHORT).show();
                        setResult(ORDER_RESULT);
                        finish();
                    } else {
                        // 判断resultStatus 为非"9000"则代表可能支付失败
                        // "80最终交易是00"代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            Toast.makeText(SubmitOrdersActivity.this, "支付结果确认中", Toast.LENGTH_SHORT).show();
                        } else {
                            // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
//                            Toast.makeText(SubmitOrdersActivity.this, "支付失败", Toast.LENGTH_SHORT).show();
                           CancelOrder(orderid);
                        }
                    }
                    break;
                }
                default:
                    break;
            }
        }
    };
    
    /**
     *支付失败后的调用接口*/
    protected void CancelOrder(String orderid) {
        showDialog();
        if (!TextUtils.isEmpty(orderid)) {
            HttpUtils httpUtils = new HttpUtils(URL_ALIPAY_CANCLE) {
                @Override
                public void onError(Call call, Exception e, int id) {
                    hidDialog();
                    Toast.makeText(SubmitOrdersActivity.this, R.string.please_check_your_network_connection, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onResponse(String response, int id) {
                    Log.i("zhifubao", "onResponse: " + response);
                    hidDialog();
                    try {
                        JSONObject o = new JSONObject(response);
                        int status = o.getInt("status");
                        if (status == 1) {
                            Toast.makeText(SubmitOrdersActivity.this, R.string.ding_dan_yi_qu_xiao, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            };
            httpUtils.addParam("orderid", orderid);
            httpUtils.clicent();
        }else {
            hidDialog();
            Toast.makeText(this, "orderid is empty!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_submit_orders;
    }

    @Override
    protected void initDate() {
        goods = MyApplication.goods;
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        rvOrders.setLayoutManager(manager);
        rvOrders.setNestedScrollingEnabled(false);
        rvOrders.setHasFixedSize(true);
        adapter = new OrdersAdapterSecond(this, goods);
        rvOrders.setAdapter(adapter);
        for (int i = 0; i < goods.size(); i++) {
            discount += goods.get(i).getDisPrice() * goods.get(i).getNumber();
            price1 += goods.get(i).getRealPrice() * goods.get(i).getNumber();
        }
        discount = NumberFormatUtil.round(discount, 2);
        price1 = NumberFormatUtil.round(price1, 2);
        tvOrdersDiscount.setText("-$" + discount);
        tvOrdersMoney.setText("$" + price1);
        shoppingPrise.setText("$" + price1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        final Calendar c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);
        shopid = getIntent().getStringExtra("shopid");
        GlideUtils.load(this, getIntent().getStringExtra("shopicon"), ivOrdersShop, GlideUtils.Shape.ShopIcon);
        tvOrdersShopName.setText(getIntent().getStringExtra("shopname"));
        initTopBar();
        if (MyApplication.isEnglish) {
            time = "ASAP";
        }else {
            time = "尽快送达";
        }
    }

    private void initTopBar() {
        tbSubmitOrders.setTbCenterTv(R.string.ti_jiao_ding_dan_title, R.color.white);
        tbSubmitOrders.setTbLeftIv(R.drawable.img_icon_back_white, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.tv_weizhi, R.id.ll_weizhi,
            R.id.tv_orders_time, R.id.rl_orders_pay,
            R.id.settlement, R.id.ll_beizhu,
            R.id.rl_orders_discount})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_beizhu:
                Intent intent3 = new Intent(this, BeiZhuActivity.class);
                startActivityForResult(intent3, BEIZHU_REQUEST_CODE);
                break;
            case R.id.rl_orders_discount:
                Intent intent2 = new Intent(this, DisCountNumberActivity.class);
                startActivityForResult(intent2, DISCOUNT_REQUEST_CODE);
                break;
            case R.id.tv_weizhi:
            case R.id.ll_weizhi:
                if (MyApplication.getLogin() == null) {
                    Toast.makeText(mContext, R.string.login_agin, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(SubmitOrdersActivity.this, LoginLoginActivity.class)
                            .putExtra("jump", false));
                    return;
                }
                Intent intent = new Intent(SubmitOrdersActivity.this, AddressActivity.class);
                intent.putExtra("type", "2");
                startActivityForResult(intent, 0);
                break;
            case R.id.tv_orders_time:
                dialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (hourOfDay > 12) {
                            hourOfDay = hourOfDay - 12;
                            am = "pm";
                        } else {
                            am = "am";
                        }
                        time = hourOfDay + ":" + minute + " " + am;
                        tvOrdersTime.setText(getString(R.string.the_delivery_time1) + ":" + hourOfDay + ":" + minute + " " + am);
                    }
                }, hour, minute, false);
                dialog.show();
                break;
            case R.id.rl_orders_pay:
                Intent intent1 = new Intent(SubmitOrdersActivity.this, PayListActivity.class);
                startActivityForResult(intent1, 10);
                break;
            case R.id.settlement:
                if (weizhi == null || weizhi.equals("")) {
                    Toast.makeText(mContext, R.string.please_select_ocation, Toast.LENGTH_SHORT).show();
                    return;
                } else if (pay == 3) {
                    if (canOrder) {
                        showDialog();
                        HttpUtils httpUtils = new HttpUtils(Contants.URL_GET_TOKEN) {
                            @Override
                            public void onError(Call call, Exception e, int id) {
                                hidDialog();
                                Log.i("brtoken", "onError: " + e.getMessage());
                                Toast.makeText(SubmitOrdersActivity.this, R.string.please_check_your_network_connection, Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onResponse(String response, int id) {
                                Log.i("brtoken", "onResponse: " + response);
                                hidDialog();
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    mAuthorization = jsonObject.getString("token");
                                    DropInRequest dropInRequest = new DropInRequest().clientToken(mAuthorization);//.clientToken(clientToken)
                                    startActivityForResult(dropInRequest.getIntent(SubmitOrdersActivity.this), 4);

                                    DropInResult.fetchDropInResult(SubmitOrdersActivity.this, mAuthorization, new DropInResult.DropInResultListener() {
                                        @Override
                                        public void onError(Exception exception) {
                                            Log.i("dropui", "onError: " + exception.getMessage());
                                            // an error occurred
                                        }

                                        @Override
                                        public void onResult(DropInResult result) {
                                            if (result.getPaymentMethodType() != null) {
                                                Log.i("dropui", "onResult: " + result.getPaymentMethodNonce().getNonce());
                                                // use the icon and name to show in your UI
                                                int icon = result.getPaymentMethodType().getDrawable();
                                                int name = result.getPaymentMethodType().getLocalizedName();

                                                if (result.getPaymentMethodType() == PaymentMethodType.ANDROID_PAY) {
                                                    // The last payment method the user used was Android Pay. The Android Pay
                                                    // flow will need to be performed by the user again at the time of checkout
                                                    // using AndroidPay#requestAndroidPay(...). No PaymentMethodNonce will be
                                                    // present in result.getPaymentMethodNonce(), this is only an indication that
                                                    // the user last used Android Pay. Note: if you have enabled preauthorization
                                                    // and the user checked the "Use selected info for future purchases from this app"
                                                    // last time Android Pay was used you may need to call
                                                    // AndroidPay#requestAndroidPay(...) and then offer the user the option to change
                                                    // the default payment method using AndroidPay#changePaymentMethod(...)
                                                } else {
                                                    // show the payment method in your UI and charge the user at the
                                                    // time of checkout using the nonce: paymentMethod.getNonce()
                                                    nonce = result.getPaymentMethodNonce().getNonce();
                                                }
                                            } else {
                                                // there was no existing payment method
                                                Log.i("dropui", "onResult: " + "null");
                                            }
                                        }
                                    });
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        };
                        Log.i("brtoken", "onViewClicked: " + MyApplication.getLogin().getUserId());
                        httpUtils.addParam("userid", MyApplication.getLogin().getUserId());
                        httpUtils.addParam("token", MyApplication.getLogin().getToken());
                        httpUtils.clicent();
                    } else {
                        Toast.makeText(this, R.string.chao_chu_pei_song, Toast.LENGTH_SHORT).show();
                    }
                } else if (pay == 1) {

                } else if (pay == 2) {   //支付宝
                    if (canOrder) {
                        showDialog();
                        HttpUtils httpUtils = new HttpUtils(Contants.URL_ALIPAY_SIGN) {
                            @Override
                            public void onError(Call call, Exception e, int id) {
                                hidDialog();
                                Log.i("zhifubao", "onError: " + e.getMessage());
                                Toast.makeText(SubmitOrdersActivity.this, R.string.please_check_your_network_connection, Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onResponse(String response, int id) {
                                hidDialog();
                                Log.i("zhifubao", "onResponse: " + response);
                                try {
                                    JSONObject o = new JSONObject(response);
                                    int status = o.getInt("status");
                                    if (status == 1) {
                                        sign = o.getString("msg");
                                        orderid = o.getString("orderid");
                                        if (!TextUtils.isEmpty(sign)) {
                                            Runnable payRunnable = new Runnable() {
                                                @Override
                                                public void run() {
                                                    // 构造PayTask 对象
                                                    PayTask alipay = new PayTask(SubmitOrdersActivity.this);
                                                    // 调用支付接口，获取支付结果
                                                    String result = alipay.pay(sign, true);

                                                    Message msg = new Message();
                                                    msg.what = SDK_PAY_FLAG;
                                                    msg.obj = result;
                                                    mHandler.sendMessage(msg);
                                                }
                                            };
                                            // 必须异步调用
                                            Thread payThread = new Thread(payRunnable);
                                            payThread.start();
                                        }
                                    }else if (status == 9) {
                                        Toast.makeText(SubmitOrdersActivity.this, R.string.login_agin, Toast.LENGTH_SHORT).show();
                                        MyApplication.saveLogin(null);
                                        startActivity(new Intent(SubmitOrdersActivity.this, LoginLoginActivity.class)
                                        .putExtra("jump", false));
                                    } else if (status == 4) {
                                        Toast.makeText(SubmitOrdersActivity.this, R.string.shang_dian_wei_ying_ye, Toast.LENGTH_SHORT).show();
                                    } else if (status == 5) {
                                        Toast.makeText(SubmitOrdersActivity.this, R.string.chao_chu_zui_da_pei_song_ju_li, Toast.LENGTH_SHORT).show();
                                    } else if (status == 11) {
                                        //优惠码已停用
                                        Toast.makeText(SubmitOrdersActivity.this, R.string.you_hui_ma_yi_ting_yong, Toast.LENGTH_SHORT).show();
                                    } else if (status == 12) {
                                        //优惠活动未开始
                                        Toast.makeText(SubmitOrdersActivity.this, R.string.you_hui_huo_dong_wei_kai_shi, Toast.LENGTH_SHORT).show();
                                    } else if (status == 13) {
                                        //优惠码已过期
                                        Toast.makeText(SubmitOrdersActivity.this, R.string.you_hui_ma_yi_guo_qi, Toast.LENGTH_SHORT).show();
                                    } else if (status == 14) {
                                        //修改优惠码总使用次数失败
                                        Toast.makeText(SubmitOrdersActivity.this, R.string.xiu_gai_you_hui_ma_zong_shi_yong_ci_shi_shi_bai, Toast.LENGTH_SHORT).show();
                                    } else if (status == 15) {
                                        //优惠码使用次数超限
                                        Toast.makeText(SubmitOrdersActivity.this, R.string.gai_you_hui_ma_yi_chao_chu_shi_yong_shang_xian, Toast.LENGTH_SHORT).show();
                                    } else if (status == 16) {
                                        //优惠码个人使用次数修改失败
                                        Toast.makeText(SubmitOrdersActivity.this, R.string.you_hui_ma_ge_ren_shi_yong_ci_shu_xiu_gai_shi_bai, Toast.LENGTH_SHORT).show();
                                    } else if (status == 17) {
                                        //优惠码个人使用信息添加失败
                                        Toast.makeText(SubmitOrdersActivity.this, R.string.you_hui_ma_ge_ren_shi_yong_ci_shu_tian_jia_shi_bai, Toast.LENGTH_SHORT).show();
                                    } else if (status == 18) {
                                        //优惠码错误（被删除未找到）
                                        Toast.makeText(SubmitOrdersActivity.this, R.string.gai_you_hui_ma_yi_bei_shan_chu, Toast.LENGTH_SHORT).show();
                                    } else if (status == 19) {
                                        //商品价格有变动，请重新下单
                                        Toast.makeText(SubmitOrdersActivity.this, R.string.shang_pin_jia_ge_bian_dong, Toast.LENGTH_SHORT).show();
                                    } else if (status == 20) {
                                        //订单添加失败
                                        Toast.makeText(SubmitOrdersActivity.this, R.string.ding_dan_tian_jia_shi_bai, Toast.LENGTH_SHORT).show();
                                    } else if (status == 21) {
                                        //订单商品信息添加失败
                                        Toast.makeText(SubmitOrdersActivity.this, R.string.ding_dan_shang_pin_xin_xi_tian_jia_shi_bai, Toast.LENGTH_SHORT).show();
                                    } else if (status == 22) {
                                        //商品信息不能为空
                                        Toast.makeText(SubmitOrdersActivity.this, R.string.shang_pin_xin_xi_bu_neng_wei_kong, Toast.LENGTH_SHORT).show();
                                    } else if (status == 23) {
                                        //收入明细添加失败
                                        Toast.makeText(SubmitOrdersActivity.this, R.string.shou_ru_ming_xi_tian_jia_shi_bai, Toast.LENGTH_SHORT).show();
                                    } else if (status == 24) {
                                        //极光推送请求异常
                                        Toast.makeText(SubmitOrdersActivity.this, R.string.ding_dan_tong_zhi_qing_qiu_yi_chang, Toast.LENGTH_SHORT).show();
                                    } else if (status == 25) {
                                        //极光推送回复异常
                                        Toast.makeText(SubmitOrdersActivity.this, R.string.ding_dan_tong_zhi_hui_fu_yi_chang, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        };
                        String s;
                        if (sex.equals("女士") || sex.equals("Miss")) {
                            s = "1";
                        } else {
                            s = "0";
                        }
                        String trim = tvOrdersTime.getText().toString().trim();
                        String s1 = trim.replaceAll(getString(R.string.the_delivery_time1) + ":", "");
                        httpUtils.addParam("allprices", price1 + Double.parseDouble(format1) + "").addParams("shopid", shopid)
                                .addParams("money", format1 + "").addParams("userid", MyApplication.getLogin().getUserId())
                                .addParams("cometime", time).addParams("name", name).addParams("sex", s)
                                .addParams("phone", phone).addParams("address", weizhi).addParams("beizhu", tvOrdersTasteNotes.getText().toString()).addParams("juli", peisong + "")
                                .addParams("token", MyApplication.getLogin().getToken());
                        for (int i = 0; i < goods.size(); i++) {
                            httpUtils.addParam("goods[" + i + "][0]", goods.get(i).getGoodsid());
                            httpUtils.addParam("goods[" + i + "][1]", goods.get(i).getNumber() + "");
                            if (goods.get(i).getSelectGui() != null) {
                                httpUtils.addParam("goods[" + i + "][2][0]", goods.get(i).getSelectGui().getGid());
                                httpUtils.addParam("goods[" + i + "][2][1]", goods.get(i).getSelectGui().getGuige());
                                httpUtils.addParam("goods[" + i + "][2][2]", goods.get(i).getSelectGui().getPrimoney());
                            }
                            if (goods.get(i).getSelectPei() != null) {
                                for (int j = 0; j < goods.get(i).getSelectPei().size(); j++) {
                                    httpUtils.addParam("goods[" + i + "][3]" + "[" + j + "][0]", goods.get(i).getSelectPei().get(j).getPcid());
                                    httpUtils.addParam("goods[" + i + "][3]" + "[" + j + "][1]", goods.get(i).getSelectPei().get(j).getPcname());
                                    httpUtils.addParam("goods[" + i + "][3]" + "[" + j + "][2]", goods.get(i).getSelectPei().get(j).getPcprice());
                                }
                            }
                        }
                        if (yid != null && !TextUtils.equals(yid, "")) {
                            Log.i("xiadan", "yid: " + yid);
                            httpUtils.addParam("yid", yid);
                            httpUtils.addParam("ymoney", ymoney + "");
                            httpUtils.addParam("ycodema", ycode);
                        }
                        if (yujitime != null) {
                            httpUtils.addParam("yujitime", yujitime);
                        }
                        httpUtils.addParam("type", "1");
                        httpUtils.addParam("youhuiall", NumberFormatUtil.round((ymoney + discount), 2) + "");
                        httpUtils.addParam("partner", PARTNER);
                        httpUtils.addParam("service", "mobile.securitypay.pay");
                        httpUtils.clicent();
                    } else {
                        Toast.makeText(this, R.string.chao_chu_pei_song, Toast.LENGTH_SHORT).show();
                    }
                } else if (pay == 4) {
                    //测试用：货到付款
                    showDialog();
//                    postOrder(0, Contants.URL_HADDODER);
                    postOrder(0, Contants.URL_ADDODER);
//                    Toast.makeText(this, R.string.qing_xuan_ze_zhi_fu_fang_shi, Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(this, R.string.qing_xuan_ze_zhi_fu_fang_shi, Toast.LENGTH_SHORT).show();
//                    setResult(ORDER_RESULT);
//                    finish();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DISCOUNT_REQUEST_CODE) {
            if (resultCode == DISCOUNT_RESULT_CODE) {
                if (!isUsedDiscout) {
                    prePrice = price1;
                    isUsedDiscout = true;
                    //没有使用过的话，计算
                    getDiscountmoney(data);
                } else {
                    //使用过，使用原来的价格重新计算
                    price1 = prePrice;
                    getDiscountmoney(data);
                }
            }
        } else if (requestCode == 0) {
            if (resultCode == 3) {
                name = data.getStringExtra("name");
                phone = data.getStringExtra("phone");
                sex = data.getStringExtra("sex");
                weizhi = data.getStringExtra("weizhi");
                id = data.getStringExtra("id");
                coordinate = data.getStringExtra("coordinate");
                llWeizhi.setVisibility(View.VISIBLE);
                if (MyApplication.isEnglish) {
                    tvSubmitName.setText(sex);
                    tvSubmitSex.setText(name);
                } else {
                    tvSubmitSex.setText(sex);
                    tvSubmitName.setText(name);
                }
                tvSubmitPhone.setText(phone);
                tvWeizhi.setText(weizhi);
                showDialog();
                getPsmoney();
            }
        } else if (requestCode == BEIZHU_REQUEST_CODE) {
            if (resultCode == 9) {
                tvOrdersTasteNotes.setText(data.getStringExtra("note"));
            }
        } else if (requestCode == 10) {
            if (resultCode == 11) {
                pay = data.getIntExtra("type", 4);
                tvOrdersMethodOfPayment.setText(data.getStringExtra("payname"));
//                if (pay == 2 && canOrder) {     //如果是支付宝并且订单符合条件，生成orderInfo
//                    orderInfo = getOrderInfo(getOutTradeNo(), "EatAway客户端下单", price1 + Double.parseDouble(format1) + "");
//                    HttpUtils httpUtils = new HttpUtils(Contants.URL_ALIPAY_SIGN) {
//                        @Override
//                        public void onError(Call call, Exception e, int id) {
//                            Toast.makeText(SubmitOrdersActivity.this, R.string.please_check_your_network_connection, Toast.LENGTH_SHORT).show();
//                        }
//
//                        @Override
//                        public void onResponse(String response, int id) {
//                            sign = response;
//                            if (sign != null && !"".equals(sign)) {
//
//                            }
//                        }
//                    };
//                    httpUtils.addParam("partner", PARTNER);
//                    httpUtils.addParam("service", "mobile.securitypay.pay");
//                    httpUtils.addParam("orderinfo", orderInfo);
//                    httpUtils.clicent();
//                    String s;
//                    if (sex.equals("女士")||sex.equals("Miss")) {
//                        s = "1";
//                    } else {
//                        s = "0";
//                    }
//                    String trim = tvOrdersTime.getText().toString().trim();
//                    String s1 = trim.replaceAll(getString(R.string.the_delivery_time1), "");
//                    httpUtils.addParam("allprices", price1 + Double.parseDouble(format1) + "").addParams("shopid", shopid)
//                            .addParams("money", format1 + "").addParams("userid", MyApplication.getLogin().getUserId())
//                            .addParams("cometime", s1).addParams("name", name).addParams("sex", s)
//                            .addParams("phone", phone).addParams("address", weizhi).addParams("beizhu", tvOrdersTasteNotes.getText().toString()).addParams("juli", peisong+"")
//                            .addParams("token", MyApplication.getLogin().getToken());
//                    for (int i = 0; i < goods.size(); i++) {
//                        httpUtils.addParam("goods[" + i + "][0]", goods.get(i).getGoodsid());
//                        httpUtils.addParam("goods[" + i + "][1]", goods.get(i).getNumber() + "");
//                    }
//                    httpUtils.clicent();
//                }else {
//                    orderInfo = "";
//                }
            }
        } else if (requestCode == 4) {
            if (resultCode == Activity.RESULT_OK) {
                Log.i("dropui", "onResult: " + "------");
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                // use the result to update your UI and send the payment method nonce to your server
                if (TextUtils.isEmpty(nonce)) {
                    nonce = result.getPaymentMethodNonce().getNonce();
                }
//                result.
                showDialog();
                postOrder(BANK, Contants.URL_ADDODER);
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            // the user canceled
            Toast.makeText(this, R.string.qu_xiao_ding_dan, Toast.LENGTH_SHORT).show();
        } else {
            // handle errors here, an exception may be available in
            Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
            Toast.makeText(this, R.string.xia_dan_shi_bai, Toast.LENGTH_SHORT).show();
        }
    }


    class MyBean {

        /**
         * status : 1
         * msg : {"lmoney":"2","coordinate":"114.5375765,38.0274239","maxprice":"12","maxlong":"0.8","maxmoney":"1","lkmoney":"2","long":"100","juli":782.27,"time":""}
         */

        private int status;
        private MsgBean msg;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public MsgBean getMsg() {
            return msg;
        }

        public void setMsg(MsgBean msg) {
            this.msg = msg;
        }

        public class MsgBean {
            /**
             * lmoney : 2
             * coordinate : 114.5375765,38.0274239
             * maxprice : 12
             * maxlong : 0.8
             * maxmoney : 1
             * lkmoney : 2
             * long : 100
             * juli : 782.27
             * time :
             */

            private String lmoney;
            private String coordinate;
            private String maxprice;
            private String maxlong;
            private String maxmoney;
            private String lkmoney;
            @SerializedName("long")
            private String longX;
            private String juli;
            private String time;

            public String getLmoney() {
                return lmoney;
            }

            public void setLmoney(String lmoney) {
                this.lmoney = lmoney;
            }

            public String getCoordinate() {
                return coordinate;
            }

            public void setCoordinate(String coordinate) {
                this.coordinate = coordinate;
            }

            public String getMaxprice() {
                return maxprice;
            }

            public void setMaxprice(String maxprice) {
                this.maxprice = maxprice;
            }

            public String getMaxlong() {
                return maxlong;
            }

            public void setMaxlong(String maxlong) {
                this.maxlong = maxlong;
            }

            public String getMaxmoney() {
                return maxmoney;
            }

            public void setMaxmoney(String maxmoney) {
                this.maxmoney = maxmoney;
            }

            public String getLkmoney() {
                return lkmoney;
            }

            public void setLkmoney(String lkmoney) {
                this.lkmoney = lkmoney;
            }

            public String getLongX() {
                return longX;
            }

            public void setLongX(String longX) {
                this.longX = longX;
            }

            public String getJuli() {
                return juli;
            }

            public void setJuli(String juli) {
                this.juli = juli;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }
        }
    }

    private void postOrder(int type, String url) {
        Log.i("xiadan", "url" + url);
        HttpUtils httpUtils = new HttpUtils(url) {
            @Override
            public void onError(Call call, Exception e, int id) {
                hidDialog();
                Log.i("xiadan", e.getMessage());
                Toast.makeText(SubmitOrdersActivity.this, R.string.please_check_your_network_connection, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(String response, int id) {
                Log.i("zhifu", "onResponse: " + response);
                hidDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    if (status == 0) {
                        Toast.makeText(SubmitOrdersActivity.this, R.string.xia_dan_shi_bai, Toast.LENGTH_SHORT).show();
                    } else if (status == 1) {
                        Toast.makeText(SubmitOrdersActivity.this, R.string.xia_dan_cheng_gong, Toast.LENGTH_SHORT).show();
                        setResult(ORDER_RESULT);
                        finish();
                    } else if (status == 9) {
                        Toast.makeText(SubmitOrdersActivity.this, R.string.login_agin, Toast.LENGTH_SHORT).show();
                        MyApplication.saveLogin(null);
                        startActivity(new Intent(SubmitOrdersActivity.this, LoginLoginActivity.class)
                                .putExtra("jump", false));
                    } else if (status == 4) {
                        Toast.makeText(SubmitOrdersActivity.this, R.string.shang_dian_wei_ying_ye, Toast.LENGTH_SHORT).show();
                    } else if (status == 5) {
                        Toast.makeText(SubmitOrdersActivity.this, R.string.chao_chu_zui_da_pei_song_ju_li, Toast.LENGTH_SHORT).show();
                    } else if (status == 11) {
                        //优惠码已停用
                        Toast.makeText(SubmitOrdersActivity.this, R.string.you_hui_ma_yi_ting_yong, Toast.LENGTH_SHORT).show();
                    } else if (status == 12) {
                        //优惠活动未开始
                        Toast.makeText(SubmitOrdersActivity.this, R.string.you_hui_huo_dong_wei_kai_shi, Toast.LENGTH_SHORT).show();
                    } else if (status == 13) {
                        //优惠码已过期
                        Toast.makeText(SubmitOrdersActivity.this, R.string.you_hui_ma_yi_guo_qi, Toast.LENGTH_SHORT).show();
                    } else if (status == 14) {
                        //修改优惠码总使用次数失败
                        Toast.makeText(SubmitOrdersActivity.this, R.string.xiu_gai_you_hui_ma_zong_shi_yong_ci_shi_shi_bai, Toast.LENGTH_SHORT).show();
                    } else if (status == 15) {
                        //优惠码使用次数超限
                        Toast.makeText(SubmitOrdersActivity.this, R.string.gai_you_hui_ma_yi_chao_chu_shi_yong_shang_xian, Toast.LENGTH_SHORT).show();
                    } else if (status == 16) {
                        //优惠码个人使用次数修改失败
                        Toast.makeText(SubmitOrdersActivity.this, R.string.you_hui_ma_ge_ren_shi_yong_ci_shu_xiu_gai_shi_bai, Toast.LENGTH_SHORT).show();
                    } else if (status == 17) {
                        //优惠码个人使用信息添加失败
                        Toast.makeText(SubmitOrdersActivity.this, R.string.you_hui_ma_ge_ren_shi_yong_ci_shu_tian_jia_shi_bai, Toast.LENGTH_SHORT).show();
                    } else if (status == 18) {
                        //优惠码错误（被删除未找到）
                        Toast.makeText(SubmitOrdersActivity.this, R.string.gai_you_hui_ma_yi_bei_shan_chu, Toast.LENGTH_SHORT).show();
                    } else if (status == 19) {
                        //商品价格有变动，请重新下单
                        Toast.makeText(SubmitOrdersActivity.this, R.string.shang_pin_jia_ge_bian_dong, Toast.LENGTH_SHORT).show();
                    } else if (status == 20) {
                        //订单添加失败
                        Toast.makeText(SubmitOrdersActivity.this, R.string.ding_dan_tian_jia_shi_bai, Toast.LENGTH_SHORT).show();
                    } else if (status == 21) {
                        //订单商品信息添加失败
                        Toast.makeText(SubmitOrdersActivity.this, R.string.ding_dan_shang_pin_xin_xi_tian_jia_shi_bai, Toast.LENGTH_SHORT).show();
                    } else if (status == 22) {
                        //商品信息不能为空
                        Toast.makeText(SubmitOrdersActivity.this, R.string.shang_pin_xin_xi_bu_neng_wei_kong, Toast.LENGTH_SHORT).show();
                    } else if (status == 23) {
                        //收入明细添加失败
                        Toast.makeText(SubmitOrdersActivity.this, R.string.shou_ru_ming_xi_tian_jia_shi_bai, Toast.LENGTH_SHORT).show();
                    } else if (status == 24) {
                        //极光推送请求异常
                        Toast.makeText(SubmitOrdersActivity.this, R.string.ding_dan_tong_zhi_qing_qiu_yi_chang, Toast.LENGTH_SHORT).show();
                    } else if (status == 25) {
                        //极光推送回复异常
                        Toast.makeText(SubmitOrdersActivity.this, R.string.ding_dan_tong_zhi_hui_fu_yi_chang, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        String s;
        if (sex.equals("女士") || sex.equals("Miss")) {
            s = "1";
        } else {
            s = "0";
        }
        String trim = tvOrdersTime.getText().toString().trim();
        String s1 = trim.replaceAll(getString(R.string.the_delivery_time1) + ":", "");
        httpUtils.addParam("allprices", calmoney + "").addParams("shopid", shopid)
                .addParams("money", psmoney + "").addParams("userid", MyApplication.getLogin().getUserId())
                .addParams("cometime", time).addParams("name", name).addParams("sex", s)
                .addParams("phone", phone).addParams("address", weizhi).addParams("beizhu", tvOrdersTasteNotes.getText().toString()).addParams("juli", peisong + "")
                .addParams("token", MyApplication.getLogin().getToken());
        for (int i = 0; i < goods.size(); i++) {
            httpUtils.addParam("goods[" + i + "][0]", goods.get(i).getGoodsid());
            httpUtils.addParam("goods[" + i + "][1]", goods.get(i).getNumber() + "");
            if (goods.get(i).getSelectGui() != null) {
                httpUtils.addParam("goods[" + i + "][2][0]", goods.get(i).getSelectGui().getGid());
                httpUtils.addParam("goods[" + i + "][2][1]", goods.get(i).getSelectGui().getGuige());
                httpUtils.addParam("goods[" + i + "][2][2]", goods.get(i).getSelectGui().getPrimoney());
            }
            if (goods.get(i).getSelectPei() != null) {
                for (int j = 0; j < goods.get(i).getSelectPei().size(); j++) {
                    httpUtils.addParam("goods[" + i + "][3]" + "[" + j + "][0]", goods.get(i).getSelectPei().get(j).getPcid());
                    httpUtils.addParam("goods[" + i + "][3]" + "[" + j + "][1]", goods.get(i).getSelectPei().get(j).getPcname());
                    httpUtils.addParam("goods[" + i + "][3]" + "[" + j + "][2]", goods.get(i).getSelectPei().get(j).getPcprice());
                }
            }
        }
        if (yid != null && !TextUtils.equals(yid, "")) {
            Log.i("xiadan", "yid: " + yid + " "+ymoney + " "+ycode);
            httpUtils.addParam("yid", yid);
            httpUtils.addParam("ymoney", ymoney + "");
            httpUtils.addParam("ycodema", ycode);
        }
        if (type == BANK) {
            //银行卡
            httpUtils.addParam("nonce", nonce);
            httpUtils.addParam("pay", "3");
        }
        if (yujitime != null) {
            httpUtils.addParam("yujitime", yujitime);
        }
        httpUtils.addParam("type", "1");
        httpUtils.addParam("youhuiall", NumberFormatUtil.round((ymoney + discount), 2) + "");
        httpUtils.clicent();
    }

    private void getPsmoney() {
        HttpUtils httpUtils = new HttpUtils(Contants.URL_ADDRESS_JI) {
            @Override
            public void onError(Call call, Exception e, int id) {
                hidDialog();
                Toast.makeText(SubmitOrdersActivity.this, R.string.please_check_your_network_connection, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(String response, int id) {
                Log.i("=======", response);
                hidDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        isGotPsmoney = true;
                        MyBean bean = new Gson().fromJson(response, MyBean.class);
                        if (TextUtils.isEmpty(bean.getMsg().getJuli())) {
                            peisong = 0;
                        } else {
                            peisong = Double.parseDouble(bean.getMsg().getJuli());
                        }
                        if (TextUtils.isEmpty(bean.getMsg().getMaxprice())) {
                            freeMoney = 0;
                        } else {
                            freeMoney = Double.parseDouble(bean.getMsg().getMaxprice());
                        }
                        if (TextUtils.isEmpty(bean.getMsg().getMaxlong())) {
                            feeForKm = 0;
                        } else {
                            feeForKm = Double.parseDouble(bean.getMsg().getMaxlong());
                        }
                        if (TextUtils.isEmpty(bean.getMsg().getMaxmoney())) {
                            feeForMoney = 0;
                        } else {
                            feeForMoney = Double.parseDouble(bean.getMsg().getMaxmoney());
                        }
                        if (TextUtils.isEmpty(bean.getMsg().getLkmoney())) {
                            feeForLk = 0;
                        } else {
                            feeForLk = Double.parseDouble(bean.getMsg().getLkmoney());
                        }
                        if (TextUtils.isEmpty(bean.getMsg().getLongX())) {
                            maxX = 0;
                        } else {
                            maxX = Double.parseDouble(bean.getMsg().getLongX());
                        }
                        yujitime = bean.getMsg().getTime();
                        canOrder = true;
                        if (peisong / 1000 > maxX) {
                            //超出配送距离
                            tvOrdersDelivery.setText(R.string.chao_chu_pei_song);
                            canOrder = false;
                        } else {
                            if (freeMoney > 0 && price1 >= freeMoney) {
                                psmoney = 0;
                            } else if (feeForKm >= 0 && peisong / 1000 <= feeForKm) {
                                psmoney = feeForMoney;
                            } else if (feeForKm >= 0 && peisong / 1000 > feeForKm) {
                                psmoney = (peisong / 1000 - feeForKm) * feeForLk + feeForMoney;
                            } else if (feeForKm < 0) {
                                psmoney = peisong / 1000 * feeForLk;
                            }
                            psmoney = NumberFormatUtil.round(psmoney, 2);
                            calmoney = NumberFormatUtil.round(price1 + psmoney, 2);
                            tvOrdersDelivery.setText("+$" + psmoney);
                            tvOrdersMoney.setText("$" + calmoney);
                            shoppingPrise.setText("$" + calmoney);
                        }
                        if (peisong > 1000) {
                            tvOrdersDress.setText(getString(R.string.order_distance) + " " + NumberFormatUtil.round(peisong / 1000, 2) + "km");
                        } else {
                            tvOrdersDress.setText(getString(R.string.order_distance) + " " + peisong + "m");
                        }
                        if (!TextUtils.isEmpty(bean.getMsg().getTime())) {
                            tvRunTime.setVisibility(View.VISIBLE);
                            if (MyApplication.isEnglish) {
                                tvRunTime.setText(getString(R.string.yu_ji_dao_lu_shi_jian) + " " + bean.getMsg().getTime());
                            }else {
                                tvRunTime.setText(getString(R.string.yu_ji_dao_lu_shi_jian) + bean.getMsg().getTime());
                            }
                        }else {
                            tvRunTime.setVisibility(View.GONE);
                        }
                    }else if (status == 9) {
                        Toast.makeText(SubmitOrdersActivity.this, R.string.login_agin, Toast.LENGTH_SHORT).show();
                        MyApplication.saveLogin(null);
                        startActivity(new Intent(SubmitOrdersActivity.this, LoginLoginActivity.class)
                                .putExtra("jump", false));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        if (!TextUtils.isEmpty(coordinate)) {
            Log.i("xiadan", "getPsmoney: " + coordinate);
            httpUtils.addParam("coordinate", coordinate).addParams("shopid", shopid);
            httpUtils.clicent();
        } else {
            Toast.makeText(this, R.string.wu_xiao_de_di_zhi, Toast.LENGTH_SHORT).show();
        }
//        httpUtils.addParam("coor", "-34.923087,138.575255");
//        httpUtils.addParam("shopcoor", "-34.955107,138.590151");
    }

    /**
     * 根据原价格进行优惠码的优惠计算
     * 注：该函数中刷新的是参数price1
     */
    private void getDiscountmoney(Intent data) {
        //优惠码
        yid = data.getStringExtra("yid");
        discountState = data.getIntExtra("state", 0);
        discountNum = data.getDoubleExtra("discount", 0);
        ycode = data.getStringExtra("ycode");
        if (discountState == 1) {
            ymoney = NumberFormatUtil.round(price1 * (discountNum / 100), 2);
            price1 = NumberFormatUtil.round(price1 - ymoney, 2);
            tvOrdersGetDiscount.setText((int) discountNum + "% off");
            tvOrdersDiscount.setText("-$" + NumberFormatUtil.round((ymoney + discount),2));
            if (isGotPsmoney) {
                //如果之前计算过配送费，重新计算
                showDialog();
                getPsmoney();
            } else {
                //如果没有计算过，直接显示算好的总价
                tvOrdersMoney.setText("$" + price1);
                shoppingPrise.setText("$" + price1);
            }
        } else if (discountState == 2) {
            if (price1 - discountNum > 0) {
                ymoney = discountNum;
                price1 = NumberFormatUtil.round(price1 - discountNum, 2);
                tvOrdersDiscount.setText("-$" + (discountNum + discount));
            } else {
                ymoney = price1;
                tvOrdersDiscount.setText("-$" + NumberFormatUtil.round(price1, 2));
                price1 = 0;
            }
            tvOrdersGetDiscount.setText("-$" + NumberFormatUtil.round((discountNum + discount), 2));
            if (isGotPsmoney) {
                //如果之前计算过配送费，重新计算
                showDialog();
                getPsmoney();
            } else {
                //如果没有计算过，直接显示算好的总价
                tvOrdersMoney.setText("$" + price1);
                shoppingPrise.setText("$" + price1);
            }
            showDialog();
            getPsmoney();
        } else {
            discountNum = 0;
        }
    }
}
