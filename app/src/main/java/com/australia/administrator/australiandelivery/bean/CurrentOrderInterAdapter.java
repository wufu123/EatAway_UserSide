package com.australia.administrator.australiandelivery.bean;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.australia.administrator.australiandelivery.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2017/7/29.
 */

public class CurrentOrderInterAdapter extends RecyclerView.Adapter<CurrentOrderInterAdapter.ViewHolder> {
    private Context context;
    private List<CurrentOrderBean.MsgBean.GoodsBean> list;

    public CurrentOrderInterAdapter(Context context, List<CurrentOrderBean.MsgBean.GoodsBean> bean) {
        this.context = context;
        this.list = bean;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_order_details_middle_order_second, null);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.i("mylist", "onBindViewHolder: ");
        if (!TextUtils.isEmpty(list.get(position).getGuigename())) {
            holder.itemOrderDetailsMiddleOrderFoodname.setText(list.get(position).getGoodsname() + "(" +
                    list.get(position).getGuigename() +  ")");
        }else {
            holder.itemOrderDetailsMiddleOrderFoodname.setText(list.get(position).getGoodsname());
        }
        holder.itemOrderDetailsMiddleOrderFoodnum.setText("×" + list.get(position).getNum());
        holder.itemOrderDetailsMiddleOrderFoodfee.setText("$" + list.get(position).getZprice());
        String[] split = list.get(position).getPcname().split(",");
        holder.llPeicai.removeAllViews();
        for (int i = 0; i < split.length; i++) {
            if (!TextUtils.equals(split[i], "")) {
                TextView tvPeicai = new TextView(context);
                tvPeicai.setText("+" + split[i]);
                holder.llPeicai.addView(tvPeicai);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (list != null) {
            Log.i("mylist", "getItemCount: " + list.size());
            return list.size();
        }
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.v_item_order_details_middle_order_line)
        View vItemOrderDetailsMiddleOrderLine;
        @Bind(R.id.item_order_details_middle_order_foodname)
        TextView itemOrderDetailsMiddleOrderFoodname;
        @Bind(R.id.ll_peicai)
        LinearLayout llPeicai;
        @Bind(R.id.item_order_details_middle_order_foodnum)
        TextView itemOrderDetailsMiddleOrderFoodnum;
        @Bind(R.id.item_order_details_middle_order_foodfee)
        TextView itemOrderDetailsMiddleOrderFoodfee;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
