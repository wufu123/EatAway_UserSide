package com.australia.administrator.australiandelivery.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.australia.administrator.australiandelivery.MyApplication;
import com.australia.administrator.australiandelivery.R;
import com.australia.administrator.australiandelivery.bean.OrderBean;
import com.australia.administrator.australiandelivery.utils.NumberFormatUtil;

import org.w3c.dom.Text;

import butterknife.Bind;


/**
 * Created by Administrator on 2017/7/10.
 */

public class OrderDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private OrderListAdapter adapter;
    private OrderBean bean;

    public OrderDetailsAdapter(Context context) {
        this.context = context;
    }

    public OrderDetailsAdapter(Context context, OrderBean bean) {
        this.context = context;
        this.bean = bean;
    }

    public OrderBean getBean() {
        return bean;
    }

    public void setBean(OrderBean bean) {
        this.bean = bean;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == 0) {
            view = LayoutInflater.from(context).inflate(R.layout.item_order_details_top, parent, false);
            TopHolder holder = new TopHolder(view);
            holder.tvOrderDetailsTopType = (TextView) view.findViewById(R.id.tv_order_details_top_type);
            holder.tvOrderDetailsTopTime = (TextView) view.findViewById(R.id.tv_order_details_top_time);
            return holder;
        } else if (viewType == 1) {
            view = LayoutInflater.from(context).inflate(R.layout.item_order_details_middle, parent, false);
            MiddleHolder holder = new MiddleHolder(view);
            holder.rlPs = (RelativeLayout) view.findViewById(R.id.rl_order_details_ps);
            holder.vPs = view.findViewById(R.id.v_order_detail_ps);
            holder.tvItemOrderDetailsMiddleDiscount = (TextView) view.findViewById(R.id.tv_item_order_details_middle_discount);
            holder.rvItemOrderDetailsMiddleOrder = (RecyclerView) view.findViewById(R.id.rv_item_order_details_middle_order);
            holder.tvItemOrderDetailsMiddleDeliveryFee = (TextView) view.findViewById(R.id.tv_item_order_details_middle_delivery_fee);
            holder.tvItemOrderDetailsMiddleDistance = (TextView) view.findViewById(R.id.tv_item_order_details_middle_distance);
            holder.tvItemOrderDetailsMiddleTotal = (TextView) view.findViewById(R.id.tv_item_order_details_middle_total);
            holder.ivOrderDetailsMiddleType = (ImageView) view.findViewById(R.id.iv_order_details_middle_type);
            return holder;
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.item_order_details_bottom, parent, false);
            BottomHolder holder = new BottomHolder(view);
            holder.v_address = view.findViewById(R.id.v_address);
            holder.ll_address = (LinearLayout) view.findViewById(R.id.ll_address);
            holder.llDiscount = (LinearLayout) view.findViewById(R.id.ll_discount);
            holder.tvDiscount = (TextView) view.findViewById(R.id.tv_item_order_details_bottom_discount_number);
            holder.vDiscount = view.findViewById(R.id.v_discount);
            holder.tv_delivery_time = (TextView) view.findViewById(R.id.tv_delivery_time);
            holder.tvItemOrderDetailsBottomDeliveryTime = (TextView) view.findViewById(R.id.tv_item_order_details_bottom_delivery_time);
            holder.tvItemOrderDetailsBottomOrderNumber = (TextView) view.findViewById(R.id.tv_item_order_details_bottom_order_number);
            holder.tvItemOrderDetailsBottomDeliveryAddress = (TextView) view.findViewById(R.id.tv_item_order_details_bottom_delivery_address);
            holder.tvItemOrderDetailsBottomRemarks = (TextView) view.findViewById(R.id.tv_item_order_details_bottom_remarks);
            return holder;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position == 0) {
            initTop((TopHolder) holder, position);
        } else if (position == 1) {
            initMiddle((MiddleHolder) holder, position);
        } else if (position == 2) {
            initBottom((BottomHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /**
     * 辅助函数
     */
    private void initTop(TopHolder holder, int position) {
        holder.tvOrderDetailsTopTime.setText(bean.getMsg().getAddtime());
        if (bean.getMsg().getStatu().equals("1")) {
            holder.tvOrderDetailsTopType.setText(context.getResources().getString(R.string.tui_dan_zhong));
        } else if (bean.getMsg().getStatu().equals("3")) {
            holder.tvOrderDetailsTopType.setText(context.getResources().getString(R.string.yi_tui_dan));
        } else {
            if (bean.getMsg().getState().equals("1")) { //未接单
                holder.tvOrderDetailsTopType.setText(context.getResources().getString(R.string.order_waiting_seller));
            } else if (bean.getMsg().getState().equals("2")) {   //已接单
                holder.tvOrderDetailsTopType.setText(context.getResources().getString(R.string.order_seller_order));
            } else if (bean.getMsg().getState().equals("3")) {   //待确认
                holder.tvOrderDetailsTopType.setText(context.getResources().getString(R.string.order_seller_delivered));
            } else if (bean.getMsg().getState().equals("4")) {   //待评价
                holder.tvOrderDetailsTopType.setText(context.getResources().getString(R.string.order_finish));
            } else if (bean.getMsg().getState().equals("5")) {   //已评价
                holder.tvOrderDetailsTopType.setText(context.getResources().getString(R.string.order_finish));
            }
        }
    }

    private void initMiddle(MiddleHolder holder, int position) {
        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        holder.rvItemOrderDetailsMiddleOrder.setLayoutManager(manager);
        adapter = new OrderListAdapter(context, bean.getMsg().getGoods());
        holder.rvItemOrderDetailsMiddleOrder.setAdapter(adapter);
        Log.i("order_type", "initMiddle: " + bean.getMsg().getType());
        if ("2".equals(bean.getMsg().getType())) {
            holder.rlPs.setVisibility(View.GONE);
            holder.vPs.setVisibility(View.GONE);
        }else {
            holder.rlPs.setVisibility(View.VISIBLE);
            holder.vPs.setVisibility(View.VISIBLE);
            holder.tvItemOrderDetailsMiddleDeliveryFee.setText("+$" + bean.getMsg().getMoney());
        }
        holder.tvItemOrderDetailsMiddleTotal.setText("$" + bean.getMsg().getAllprice());
        if (bean.getMsg().getJuli() > 1000) {
            holder.tvItemOrderDetailsMiddleDistance.setText("(" + context.getString(R.string.order_distance) + NumberFormatUtil.round(bean.getMsg().getJuli() / 1000, 2) + "km)");
        }else {
            holder.tvItemOrderDetailsMiddleDistance.setText("(" + context.getResources().getString(R.string.order_distance) + bean.getMsg().getJuli() + "m)");

        }
        double discount = TextUtils.isEmpty(bean.getMsg().getYouhuiall())?0: NumberFormatUtil.round(Double.parseDouble(bean.getMsg().getYouhuiall()), 2);
        holder.tvItemOrderDetailsMiddleDiscount.setText("-$" + discount);
        if (bean.getMsg().getStatu().equals("1")) {
            holder.ivOrderDetailsMiddleType.setVisibility(View.GONE);
        } else if (bean.getMsg().getStatu().equals("3")) {
            holder.ivOrderDetailsMiddleType.setVisibility(View.GONE);
        } else {
            if (MyApplication.isEnglish) {
                holder.ivOrderDetailsMiddleType.setVisibility(View.GONE);
            } else {
                holder.ivOrderDetailsMiddleType.setVisibility(View.VISIBLE);
                if (bean.getMsg().getState().equals("1")) { //未接单
                    holder.ivOrderDetailsMiddleType.setBackgroundResource(R.drawable.img_order_type_wait);
                } else if (bean.getMsg().getState().equals("2")) {   //已接单
                    holder.ivOrderDetailsMiddleType.setBackgroundResource(R.drawable.img_order_type_receive);
                } else if (bean.getMsg().getState().equals("3")) {   //待确认
                    holder.ivOrderDetailsMiddleType.setBackgroundResource(R.drawable.img_order_type_service);
                } else if (bean.getMsg().getState().equals("4")) {   //待评价
                    holder.ivOrderDetailsMiddleType.setBackgroundResource(R.drawable.img_order_type_ok);
                } else if (bean.getMsg().getState().equals("5")) {   //已评价
                    holder.ivOrderDetailsMiddleType.setBackgroundResource(R.drawable.img_order_type_ok);
                }
            }
        }
    }

    private void initBottom(BottomHolder holder, int position) {
        if ("2".equals(bean.getMsg().getType())) {
            holder.tv_delivery_time.setText(context.getString(R.string.zi_qu_shi_jian));
        }else {
            holder.tv_delivery_time.setText(context.getString(R.string.delivery_time));
        }
        holder.tvItemOrderDetailsBottomDeliveryTime.setText(bean.getMsg().getCometime());
        holder.tvItemOrderDetailsBottomOrderNumber.setText(bean.getMsg().getOrderid());
        if (TextUtils.isEmpty(bean.getMsg().getAllprice())) {
            holder.ll_address.setVisibility(View.GONE);
            holder.v_address.setVisibility(View.GONE);
        }else {
            holder.ll_address.setVisibility(View.VISIBLE);
            holder.v_address.setVisibility(View.VISIBLE);
            holder.tvItemOrderDetailsBottomDeliveryAddress.setText(bean.getMsg().getAddress());
        }
        if (!TextUtils.isEmpty(bean.getMsg().getBeizhu())) {
            holder.tvItemOrderDetailsBottomRemarks.setText(bean.getMsg().getBeizhu());
        }
        if (TextUtils.isEmpty(bean.getMsg().getYcodema())) {
            holder.llDiscount.setVisibility(View.GONE);
            holder.vDiscount.setVisibility(View.GONE);
        }else {
            holder.llDiscount.setVisibility(View.VISIBLE);
            holder.vDiscount.setVisibility(View.VISIBLE);
            holder.tvDiscount.setText(bean.getMsg().getYcodema());
        }
    }


    /**
     * holders
     */
    class TopHolder extends RecyclerView.ViewHolder {
        public TopHolder(View itemView) {
            super(itemView);
        }

        TextView tvOrderDetailsTopType;
        TextView tvOrderDetailsTopTime;
    }

    class MiddleHolder extends RecyclerView.ViewHolder {
        public MiddleHolder(View itemView) {
            super(itemView);
        }

        RecyclerView rvItemOrderDetailsMiddleOrder;
        TextView tvItemOrderDetailsMiddleDeliveryFee;
        TextView tvItemOrderDetailsMiddleTotal;
        ImageView ivOrderDetailsMiddleType;
        TextView tvItemOrderDetailsMiddleDistance;
        TextView tvItemOrderDetailsMiddleDiscount;
        RelativeLayout rlPs;
        View vPs;
    }

    class BottomHolder extends RecyclerView.ViewHolder {
        public BottomHolder(View itemView) {
            super(itemView);
        }
        TextView tv_delivery_time;
        TextView tvItemOrderDetailsBottomDeliveryTime;
        TextView tvItemOrderDetailsBottomOrderNumber;
        TextView tvItemOrderDetailsBottomDeliveryAddress;
        TextView tvItemOrderDetailsBottomRemarks;
        TextView tvDiscount;
        LinearLayout llDiscount,ll_address;
        View vDiscount,v_address;
    }
}
